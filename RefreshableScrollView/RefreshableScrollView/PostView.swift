//
//  PostView.swift
//  RefreshableScrollView
//
//  Created by guozw2 on 2023/12/12.
//  gualtierofrigerio

import SwiftUI

struct PostView: View {
    var post: Post
    
    var body: some View {
        VStack {
            HStack {
                Text("\(post.userId)")
                Text(post.title)
            }
            Text(post.body).foregroundColor(.gray)
        }
        .onAppear {
            print("onAppear post = \(post.id) \(post.userId)")
        }
    }
}
let template = Post(userId: 0, id: 0, title: "", body: "")
struct PostView_Previews: PreviewProvider {
    static var previews: some View {
        PostView(post: template)
    }
}
