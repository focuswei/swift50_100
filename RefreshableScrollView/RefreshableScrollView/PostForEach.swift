//
//  PostForEach.swift
//  RefreshableScrollView
//
//  Created by guozw2 on 2023/12/12.
//  gualtierofrigerio

import SwiftUI

struct Post: Codable, Identifiable {
    var userId: Int
    var id: Int
    var title: String
    var body: String
    
    static let template = Post(userId: 0, id: 0, title: "", body: "")
}

struct PostForEach: View {
    var posts: [Post]
    var lazy: Bool = false
    
    var body: some View {
        
        // 展示如何把通过 @ViewBuilder 把视图传递到另一个视图逻辑中，添加修饰符。
        // 作用是把坐标逻辑跟 View 的其他数据分离, 封装到另一处。
        PostRefreshableScrollView(action: refreshList) {
            if isLoading {
                VStack {
                    ProgressView()
                    Text("Loading...")
                }
            }
            
            if lazy {
                LazyVStack {
                    ForEach(posts) { post in
                        PostView(post: post)
                    }
                }
            } else {
                VStack {
                    ForEach(posts) { post in
                        PostView(post: post)
                    }
                }
            }
        }
    }
    
    @State private var isLoading = false
    
    private func refreshList() {
        isLoading = true
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0, execute: {
            isLoading = false
        })
    }
}

#Preview {
    PostForEach(posts: [Post.template])
}
