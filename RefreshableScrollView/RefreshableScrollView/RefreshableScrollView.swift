//
//  RefreshableScrollView.swift
//  RefreshableScrollView
//
//  Created by guozw2 on 2023/12/11.
//  有性能问题，Use Swift introspect instead + the underlying UIScrollView. Much better performance.

import SwiftUI

/// 不推荐
struct RefreshableScrollView<Content: View>: View {
    
    @State private var previousScrollOffset: CGFloat = 0
    @State private var scrollOffset: CGFloat = 0
    @State private var frozen: Bool = false
    @State private var rotation: Angle = .degrees(0)
    
    var threshold: CGFloat = 80
    @Binding var refreshing: Bool
    let content: Content
    
    init(height: CGFloat = 80, refreshing: Binding<Bool>, @ViewBuilder content: () -> Content) {
        self.threshold = height
        self._refreshing = refreshing
        self.content = content()
    }
    var body: some View {
        return VStack {
            ScrollView {
                ZStack(alignment: .top) {
                    // 不可见的图用来定位坐标Y
                    MovingView()
                    
                    VStack { self.content }.alignmentGuide(.top, computeValue: { d in
                        (self.refreshing && self.frozen) ? -self.threshold : 0.0
                    })
                    
                    SymbolView(height: self.threshold, loading: self.refreshing, frozen: self.frozen, rotation: self.rotation)
                }
            }
            .background(FixView())
            .onPreferenceChange(RefreshableKeyTypes.self, perform: { value in
                self.refreshLogic(values: value)
            })
        }
    }
    
    func refreshLogic(values: [PrefData]){
        // Calculate scroll offset
        let movingBounds = values.first { $0.vType == .movingView }?.bounds ?? .zero
        let fixedBounds = values.first { $0.vType == .fixedView }?.bounds ?? .zero
        self.scrollOffset  = movingBounds.minY - fixedBounds.minY
        self.rotation = self.symbolRotation(self.scrollOffset)

        // Crossing the threshold on the way down, we start the refresh process
        if !self.refreshing && (self.scrollOffset > self.threshold && self.previousScrollOffset <= self.threshold) {
            self.refreshing = true
        }
        if self.refreshing {
            // Crossing the threshold on the way up, we add a space at the top of the scrollview
            if self.previousScrollOffset > self.threshold && self.scrollOffset <= self.threshold {
                self.frozen = true
            }
        } else {
            // remove the sapce at the top of the scroll view
            self.frozen = false
        }
        // Update last scroll offset
        self.previousScrollOffset = self.scrollOffset
    }
    
    func symbolRotation(_ scrollOffset: CGFloat) -> Angle {
        
        // We will begin rotation, only after we have passed
        // 60% of the way of reaching the threshold.
        if scrollOffset < self.threshold * 0.60 {
            return .degrees(0)
        } else {
            // Calculate rotation, based on the amount of scroll offset
            let h = Double(self.threshold)
            let d = Double(scrollOffset)
            let v = max(min(d - (h * 0.6), h * 0.4), 0)
            return .degrees(180 * v / (h * 0.4))
        }
    }
}


struct MovingView: View {
    var body: some View {
        GeometryReader { geometry in
            Color.clear.preference(key: RefreshableKeyTypes.self, value: [PrefData(vType: .movingView, bounds: geometry.frame(in: .global))])
        }
        .frame(height: 0)
    }
}

struct FixView: View {
    var body: some View {
        GeometryReader(content: { geometry in
            Color.clear.preference(key: RefreshableKeyTypes.self, value: [PrefData(vType: .fixedView, bounds: geometry.frame(in: .global))])
        })
    }
}

struct PrefData: Equatable {
    enum MyScrollType: Equatable {
        case scrollViewTop
        case movingView
        case scrollViewContainer
        case fixedView
    }
    
    let vType: MyScrollType
    let bounds: CGRect
}

struct RefreshableKeyTypes: PreferenceKey {
    static var defaultValue: [PrefData] = []
    
    static func reduce(value: inout [PrefData], nextValue: () -> [PrefData]) {
        value.append(contentsOf: nextValue())
    }
    
    typealias Value = [PrefData]
}



