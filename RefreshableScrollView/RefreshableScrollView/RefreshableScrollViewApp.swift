//
//  RefreshableScrollViewApp.swift
//  RefreshableScrollView
//
//  Created by guozw2 on 2023/12/8.
//

import SwiftUI

@main
struct RefreshableScrollViewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
