//
//  PostContentView.swift
//  RefreshableScrollView
//
//  Created by guozw2 on 2023/12/14.
//

import SwiftUI

struct PostContentView: View {
    let model: ContentViewModel
    
    var body: some View {
        if #available(iOS 15, *) {
            PostList(posts: model.allPosts, networkPosts: [])
        } else {
            var type: String = ""
            switch type {
            case "lazy":
                PostForEach(posts: model.allPosts, lazy: true)
            case "Modifier":
                PostForEachModifier(posts: model.allPosts)
            case "pull-refresh":
                PostForEachPull(viewModel: model)
            default:
                PostForEach(posts: model.allPosts, lazy: false)
            }
        }
    }
    
    private func refreshAction() {
        
    }
}

#Preview {
    PostContentView(model: ContentViewModel())
}
