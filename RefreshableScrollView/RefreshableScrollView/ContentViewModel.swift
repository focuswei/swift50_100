//
//  ContentViewModel.swift
//  RefreshableScrollView
//
//  Created by guozw2 on 2023/12/14.
//

import Foundation


class ContentViewModel: ObservableObject {
    @Published var allPosts: [Post]
    var dataSource = [Post.template]
    
    init() {
        self.allPosts = dataSource
    }
}
