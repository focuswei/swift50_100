//
//  PostForEachPull.swift
//  RefreshableScrollView
//
//  Created by guozw2 on 2023/12/14.
//

import SwiftUI

@available(iOS 15, *)
struct PostForEachPull: View {
    var viewModel: ContentViewModel
    
    @State private var posts: [Post] = []
    var body: some View {
        ScrollViewPullRefresh {
            VStack {
                ForEach(posts) { post in
                    PostView(post: post)
                }
            }
        }
        .task {
            posts = await getPosts()
        }
        .refreshable {
            posts = await shufflePosts()
        }
    }
    
    private func getPosts() async -> [Post] {
        viewModel.allPosts
    }
    
    private func shufflePosts() async -> [Post] {
        do { try await Task.sleep(nanoseconds: 2_000)
        } catch { }
        return viewModel.allPosts
    }
}

//#Preview {
//    PostForEachPull()
//}
