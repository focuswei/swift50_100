//
//  PostForEachModifier.swift
//  RefreshableScrollView
//
//  Created by guozw2 on 2023/12/14.
//

import SwiftUI

struct PostForEachModifier: View {
    var posts: [Post]
    
    var body: some View {
        LazyVStack {
            if isLoading {
                ProgressView()
            }
            ForEach(posts) { post in
                PostView(post: post)
            }
        }
        .modifier(RefreshableScrollViewModifier(action: refreshAction))
    }
    
    @State private var isLoading = false
    
    private func refreshAction() {
        isLoading = true
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
            isLoading = false
        })
    }
}

#Preview {
    PostForEachModifier(posts: [])
}
