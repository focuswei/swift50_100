//
//  PostRefreshableScrollView.swift
//  RefreshableScrollView
//
//  Created by guozw2 on 2023/12/12.
//  gualtierofrigerio

import SwiftUI

struct PostRefreshableScrollView<Content: View>: View {
    // 传递闭包的方式
    init(action: @escaping () -> Void, @ViewBuilder content: @escaping () -> Content) {
        self.content = content
        self.refreshAction = action
    }
    
    private var content: () -> Content
    private var refreshAction: () -> Void
    private let threshold: CGFloat = 50.0
    
    var body: some View {
        GeometryReader { geo in
            ScrollView {
                content()
                    .anchorPreference(key: OffsetPreferenceKey.self, value: .top) { anchor in
                        geo[anchor].y
                    }
            }
            .onPreferenceChange(OffsetPreferenceKey.self) { offset in
                // 大于临界值，调用刷新闭包
                if offset > threshold {
                    refreshAction()
                }
            }
        }
    }
}



fileprivate struct OffsetPreferenceKey: PreferenceKey {
    static var defaultValue: CGFloat = 0
    
    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
        value = nextValue()
    }
}


struct RefreshableScrollViewModifier: ViewModifier {
    
    var action: () -> Void
    
    func body(content: Content) -> some View {
        PostRefreshableScrollView(action: action) {
            content
        }
    }
}
