//
//  PostList.swift
//  RefreshableScrollView
//
//  Created by guozw2 on 2023/12/14.
//

import SwiftUI

struct PostList: View {
    @State var posts: [Post]
    @State var networkPosts: [Post]
    let isNetwork: Bool = false
    
    var body: some View {
        VStack {
            Text("there are \(posts.count) posts")
            if #available(iOS 15.0, *) {
                List(posts) { post in
                    PostView(post: post)
                }
                .refreshable {
                    // 讲视图标记为可刷新, enables a standard pull-to-refresh gesture
                    if isNetwork {
                        do {
                            let url = URL(string: "https://www.hackingwithswift.com/samples/news-1.json")!
                            let (data, _) = try await URLSession.shared.data(from: url)
                            networkPosts = try JSONDecoder().decode([Post].self, from: data)
                        } catch {
                            // Something went wrong; clear the news
                            networkPosts = []
                        }
                    } else {
                        await refreshListAsync()
                    }
                }
            } else {
                List(posts) { post in
                    PostView(post: post)
                }
            }
        }
    }
    
    @State private var isRefreshing = false
    
    @available(iOS 15.0, *)
    private func refreshListAsync() async {
        if isRefreshing == false {
            isRefreshing = true
            self.posts = await shufflePosts(posts: self.posts)
            isRefreshing = false
        }
    }
    
    @available(iOS 15.0, *)
    private func shufflePosts(posts: [Post]) async -> [Post] {
        return posts.shuffled()
    }
}

#Preview {
    PostList(posts: [], networkPosts: [])
}
