//
//  SymbolView.swift
//  RefreshableScrollView
//
//  Created by guozw2 on 2023/12/11.
//

import SwiftUI

struct SymbolView: View {
    var height: CGFloat
    var loading: Bool
    var frozen: Bool
    var rotation: Angle
    
    
    var body: some View {
        Group {
            if self.loading { // If loading, show the activity control
                VStack {
                    Spacer()
                    ActivityRep()
                    Spacer()
                }.frame(height: height).fixedSize()
                 .offset(y: -height + (self.loading && self.frozen ? height : 0.0))
            } else {
                Image(systemName: "arrow.down") // If not loading, show the arrow
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: height * 0.25, height: height * 0.25).fixedSize()
                    .padding(height * 0.375)
                    .rotationEffect(rotation)
                    .offset(y: -height + (loading && frozen ? +height : 0.0))
            }
        }
    }
}

#Preview {
    SymbolView(height: 20, loading: true, frozen: false, rotation: Angle(degrees: 20))
}

struct ActivityRep: UIViewRepresentable {
    func updateUIView(_ uiView: UIActivityIndicatorView, context: Context) {
        uiView.startAnimating()
    }
    
    func makeUIView(context: Context) -> UIActivityIndicatorView {
        return UIActivityIndicatorView()
    }
    
    typealias UIViewType = UIActivityIndicatorView
   
    
    
}
