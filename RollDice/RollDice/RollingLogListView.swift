//
//  RollingLogListView.swift
//  RollDice
//
//  Created by guozw2 on 2023/8/24.
//

import SwiftUI

struct RollingLogListView: View {
    @Environment(\.presentationMode) var presentationsMode
    
    private var rollingLogManager = RollingLogManager.shared
    
    var body: some View {
        VStack {
            HStack(alignment: .center, spacing: 0) {
                Text("HISTORY")
                    .font(.title.bold())
                
                Spacer()
                
                Image(systemName: "xmark.circle.fill", variableValue: 10)
                    .font(.title)
                    .onTapGesture {
                        presentationsMode.wrappedValue.dismiss()
                    }
                    .accessibilityRemoveTraits(.isImage)
                    .accessibilityAddTraits(.isButton)
                    .accessibilityLabel("Close")
                    .accessibilityHint("Tap to close History panel")
            }
            
            ScrollView(.vertical) {
                LazyVStack(alignment: .leading, spacing: 20) {
                    ForEach(rollingLogManager.getLogs(), id: \.createdAt) { eachLog in
                        LogView(of: eachLog)
                            .padding(10)
                            .background(.gray.opacity(0.3))
                            .clipShape(RoundedRectangle(cornerRadius: 10))
                            .accessibilityElement()
                            .accessibilityLabel("History of rolling action at \(eachLog.createdAtDescription)")
                    }
                }
            }
        }
        .background(.black)
        .foregroundColor(.white)
    }
}

struct RollingLogListView_Previews: PreviewProvider {
    static var previews: some View {
        RollingLogListView()
    }
}
