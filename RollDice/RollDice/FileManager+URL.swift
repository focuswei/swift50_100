//
//  FileManager+URL.swift
//  RollDice
//
//  Created by guozw2 on 2023/8/22.
//

import Foundation

extension FileManager {
    func getDocumentDirectory() -> URL {
        let path = self.urls(for: .documentDirectory, in: .userDomainMask)
        return path[0]
    }
    
    func getFileURL(fileName: String) -> URL {
        return self.getDocumentDirectory().appendingPathComponent(fileName, conformingTo: .json)
    }
    
    func decodeJSON<T: Decodable>(_ fileName: String) throws -> T? {
        let url = self.getFileURL(fileName: fileName)
        
        guard let data = try? Data(contentsOf: url),
              let obj = try? JSONDecoder().decode(T.self, from: data) else {
            return nil }
        
        return obj
        
    }
    
    func encodeJSON<T: Encodable>(_ fileName: String, fileData: T, applyEncryption: Bool = false) -> URL? {
        let url = self.getFileURL(fileName: fileName)
        
        var options: Data.WritingOptions = [.atomic, .completeFileProtection]
        let encoder = JSONEncoder()
        let data = try? encoder.encode(fileData)
        do {
            try data?.write(to: url, options: options)
        } catch {
            return nil
        }
        return url
    }
    
    
}
