//
//  ContentView.swift
//  RollDice
//
//  Created by guozw2 on 2023/8/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
//        VStack
        DiceListView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
