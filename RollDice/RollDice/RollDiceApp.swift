//
//  RollDiceApp.swift
//  RollDice
//
//  Created by guozw2 on 2023/8/22.
//

import SwiftUI

@main
struct RollDiceApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
