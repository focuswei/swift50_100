//
//  DiceListView.swift
//  RollDice
//
//  Created by guozw2 on 2023/8/24.
//

import SwiftUI

struct DiceListView: View {
    @StateObject private var diceListVM = DiceListViewModel()
    var layouts: [GridItem] = [
        GridItem(.adaptive(minimum: 100))
    ]
    
    fileprivate func extracedFunc(_ dice: Dice) -> DiceView {
        return DiceView(dice: dice,
                        shadowColorIfPressingSwitcher: Color.white,
                        shadowColorWhenDiceIsRolling: Color.black,
                        width: 90,
                        height: 70,
                        switchForegroundColorEnabled: Color.white, switchForegroundColorDisabled: Color.gray)
    }
    
    var body: some View {
        ZStack {
            Color
                .black
                .ignoresSafeArea()
            
            ScrollView(.vertical, showsIndicators: true) {
                LazyVGrid(columns: layouts, spacing: 30) {
                    ForEach(diceListVM.dices) { dice in
                        extracedFunc(dice)
                    }
                }
                .padding(.horizontal, 5)
                .padding(.vertical, 20)
            }
        }
        .sheet(isPresented: $diceListVM.isShowingRollingLogView, content: {
            RollingLogListView()
        })
        .safeAreaInset(edge: .top) {
            if diceListVM.isShowingSettings {
                SettingsView()
            }
        }
        .safeAreaInset(edge: .bottom) {
            BottomPanelView(powerSwitchForgroundColorDisabled: Color.gray,
                            powerSwutcgerForgroundColorEnabled: Color.white)
        }
        .task {
            diceListVM.generateDices()
        }
        .environmentObject(diceListVM)
    }
}

struct DiceListView_Previews: PreviewProvider {
    static var previews: some View {
        DiceListView()
    }
}
