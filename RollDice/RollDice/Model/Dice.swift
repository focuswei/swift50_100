//
//  Dice.swift
//  RollDice
//
//  Created by guozw2 on 2023/9/7.
//

import Foundation
import SwiftUI

class Dice: Identifiable, ObservableObject {
    struct RollingTime {
        let timerInterval: Double
        let animationDuration: Double
    }
    var numberOfPossibilities: Double
    var orderNumber: Int = 0
    let id = UUID()
    
    @Published var visibleValue: Int = 1
    @Published var isShowingValue = false
    
    @Published var isPressingOnSwitcher = false
    @Published var runWhileRollingForSingleTapSwitcher = false
    @Published var finishedRolling = false
    
    // roll state
    var isSwitcherDisabled = false
    var isPressingSwitcher = false
    var makeVisibleValueSmaller = false
    
    var currentAnimationDurationOfShowingValue: Double = 0
    let rollingSlowest = 1.34
    let rollingFastest = 0.08
    let rollingStepChangeRate = 0.15
    let rollingTimerDelay = 0.045
    let fastRollingStep = 0.5
    
    private var longPressCounter: Double = 0
    private var longPressTimer: Timer?
    let longPressMinimumDuration: Double = 1
    let longPressTimerTimeInterval: Double = 0.5
    
    @Published var doingLongPressingOnSwitcher = false
    
    var groupId: UUID?
    
    private var rollingLogManager = RollingLogManager.shared
    
    static let sample = Dice(numberOfPossibilities: 4)
    
    @available(iOS 13.0, *)
    private var feedback = UIImpactFeedbackGenerator(style: .rigid)
    
    init(numberOfPossibilities: Double, orderNumber: Int = 1) {
        self.numberOfPossibilities = numberOfPossibilities
        self.orderNumber = orderNumber
    }
    
    private func moveToNextValue() {
        if isShowingValue {
            isShowingValue.toggle()
        } else {
            increaseValue()
            isShowingValue.toggle()
        }
    }
    
    func increaseValue() {
        if visibleValue < Int(numberOfPossibilities) {
            visibleValue += 1
        } else {
            // 置位
            visibleValue = 1
        }
    }
    
    // MARK: ** Long Press On Switcher **
    func startLongPressOnSwitcher() {
        // 通知视图用户按下开关
        isPressingOnSwitcher.toggle()
        
        // 重制计时flag
        longPressCounter = longPressMinimumDuration
        
        // 开启计时器
        longPressTimer = Timer.scheduledTimer(withTimeInterval: longPressTimerTimeInterval, repeats: true, block: { timer in
            self.longPressCounter += self.longPressTimerTimeInterval
        })
    }
    
    func stopLongPressOnSwitcher(groupId: UUID) {
        self.groupId = groupId
        
        // 通知视图，停止
        isPressingOnSwitcher.toggle()
        
        longPressTimer?.invalidate()
        
        guard let groudId = self.groupId else {
            fatalError("Dice's group id is missing")
        }
        
        runSingleTapOnDice(groupId: groupId, fastRollingInSeconds: longPressCounter, runAnimationForSingleTapOnSwitcher: false)
        
    }
    
    // MARK: ** SingleTap on Switcher **
    func runSingleTapOnDice(groupId: UUID, fastRollingInSeconds: Double? = nil, runAnimationForSingleTapOnSwitcher: Bool = true) {
        if !isSwitcherDisabled {
            self.groupId = groupId
            
            isPressingSwitcher.toggle()
            makeVisibleValueSmaller.toggle()
            
            isPressingSwitcher.toggle()
            isSwitcherDisabled = true
            
            if runAnimationForSingleTapOnSwitcher {
                isPressingOnSwitcher.toggle()
                
                Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { timer in
                    self.isPressingOnSwitcher.toggle()
                }
            }
            
            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { timer in
                self.roll(fastRollingInSeconds: fastRollingInSeconds) { _, double in
                    self.runWhileRollingForSingleTapSwitcher.toggle()
                } postAction: { _, _ in
                    self.isSwitcherDisabled = false
                    self.makeVisibleValueSmaller.toggle()
                    
                    self.finishedRolling.toggle()
                    self.rollingLogManager.logDice(of: self)
                }
            }
        }
    }
    
    func roll(fastRollingInSeconds: Double? = nil, actionInEveryLoop: ((Bool, Double) -> Void)? = nil, postAction: @escaping (UUID, Int) -> Void) {
        // create run loop
        let runLoops = generateLoops(fastRollingInSeconds: fastRollingInSeconds)
        
        var lastInterval: Double = 0
        for eachLoop in runLoops {
            lastInterval = eachLoop.timerInterval + eachLoop.animationDuration
            // creat Timer
            Timer.scheduledTimer(withTimeInterval: eachLoop.timerInterval, repeats: false) { timer in
                // trigger haptic
                self.feedback.impactOccurred()
                
                self.currentAnimationDurationOfShowingValue = eachLoop.animationDuration
                // 计算数值
                self.moveToNextValue()
                
                if let actionInEveryLoop = actionInEveryLoop {
                    // 并行
                    actionInEveryLoop(self.isShowingValue, eachLoop.animationDuration)
                }
            }
        }
        
        // re-enable Switcher
        Timer.scheduledTimer(withTimeInterval: lastInterval, repeats: false) { timer in
            postAction(self.id, self.visibleValue)
        }
    }
    
    func generateLoops(fastRollingInSeconds: Double? = nil) -> [RollingTime]{
        var rollingLoops = [RollingTime]()
        
        var loopCounter: Int = 0
        
        var timerInterval: Double = 0
        var animationDuration: Double = rollingSlowest
        
        while animationDuration >= rollingFastest {
            animationDuration -= loopCounter == 0 ? 0 : animationDuration * rollingStepChangeRate  // 1.34 * 0.15
            animationDuration = round(animationDuration * 100)/100.0
            
            let rollingLoop = RollingTime(timerInterval: timerInterval, animationDuration: animationDuration)
            rollingLoops.append(rollingLoop)
            
            timerInterval += animationDuration + rollingTimerDelay
            timerInterval = round(timerInterval * 100)/100.0
            
            loopCounter += 1
        }
        
        var fastRollingCount: Double = 0
        var fastRollingLimit = fastRollingStep
        
        if let second = fastRollingInSeconds {
            fastRollingLimit = second
        }
        
        while fastRollingCount <= fastRollingLimit {
            // 随机算法
            for _ in 0..<Int.random(in: 0...10) {
                let rollingLoop = RollingTime(timerInterval: timerInterval, animationDuration: animationDuration)
                rollingLoops.append(rollingLoop)
                
                timerInterval += animationDuration + rollingTimerDelay
                timerInterval = round(timerInterval * 100)/100.0
            }
            
            fastRollingCount += fastRollingStep
        }
        
        
        let reversedLoops = rollingLoops.reversed()
        for eachRolling in reversedLoops {
            let rollingLoop = RollingTime(timerInterval: timerInterval, animationDuration: eachRolling.animationDuration)
            rollingLoops.append(rollingLoop)
            
            timerInterval += eachRolling.animationDuration + rollingTimerDelay
            // 取整
            timerInterval = round(timerInterval * 100)/100
        }
        
        return rollingLoops
    }
    
    func makeIncrementer(forIncrement amount: Int) -> () -> Int {
        var runningTotal = 0
        func incrementer() -> Int {
            runningTotal += amount
            return runningTotal
        }
        return incrementer
    }
}



extension Dice: Equatable {
    static func == (lhs: Dice, rhs: Dice) -> Bool {
        lhs.id == rhs.id
    }
}


