//
//  Settings.swift
//  RollDice
//
//  Created by guozw2 on 2023/8/22.
//

import Foundation


struct Settings: Codable {
    var numberOfDices: Double = 1
    var numberOfPossibilities: Double = 4
}


