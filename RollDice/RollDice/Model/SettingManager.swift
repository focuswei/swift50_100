//
//  SettingManager.swift
//  RollDice
//
//  Created by guozw2 on 2023/8/24.
//

import Foundation

class SettingsManager {
    static let shared = SettingsManager()
    
    var settings: Settings
    
    let settingFile = "Settings"
    
    private init() {
        guard let settings = try? FileManager.default.decodeJSON(settingFile) as Settings? else {
            self.settings = Settings()
            return
        }
        
        self.settings = settings
    }
    
    
    func save() {
        let _ = FileManager.default.encodeJSON(settingFile, fileData: settings)
    }
}
