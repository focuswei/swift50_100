//
//  RollingLog.swift
//  RollDice
//
//  Created by guozw2 on 2023/8/24.
//

import Foundation

struct RollingLog: Codable, Comparable {
    var dices: Double
    var posibilities: Double
    var result: String
    var sumOfResult: Int
    var highestResult: Int
    var lowestResult: Int
    var createdAt = Date.now
    
    var createdAtDescription: String {
        return createdAt.formatted(date: .abbreviated, time: .shortened)
    }
    static func < (lhs: RollingLog, rhs: RollingLog) -> Bool {
        lhs.result < rhs.result
    }
}
