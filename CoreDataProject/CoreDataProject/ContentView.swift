//
//  ContentView.swift
//  CoreDataProject
//
//  Created by guozw2 on 2023/5/8.
//  day 58

import SwiftUI

struct ContentView: View {
    /**
     从xcdatamodel 中fetch 数据对象，有很多是可选项？
     在实体Movie 的Inspectores 看到Codegen=Class Definition 就知道构建项目何时生成 托管对象类
     解决方法一：可以在View Inspectors修改 Attribute，Default value，等等。
     方法二：选中.xcdatamodel 文件，然后选择Xcode选项卡"Editor"，选择Creat NSManagedObject Subclass
     */
    @FetchRequest(sortDescriptors: []) var movies: FetchedResults<Movie>
    
    @Environment(\.managedObjectContext) var moc
    
    // 相当于 var _lastNameFilter: = State<String>(wrappedValue: "A"); var score: Int { get { return _score.wrappedValue } set { _score.wrappedValue = newValue }}; var $score: Binding<Int> { get { return _score.projectedValue }}; var $score: Binding<Int> { get { return _score.projectedValue }}
    @State private var lastNameFilter = "A"
    @State private var firstNameFilter = "T"
    
    @FetchRequest(sortDescriptors: []) var countries: FetchedResults<Country>
    
    var body: some View {
        VStack {
            List {
                ForEach(countries, id: \.self) { country in
                    Section(country.wrappedFullName) {
                        ForEach(country.candyArray, id: \.self) { candy in
                            Text(candy.wrappedName)
                        }
                    }
                }
            }

            Button("Add") {
                let candy1 = Candy(context: moc)
                candy1.name = "Mars"
                candy1.origin = Country(context: moc)
                candy1.origin?.shortName = "UK"
                candy1.origin?.fullName = "United Kingdom"

                let candy2 = Candy(context: moc)
                candy2.name = "KitKat"
                candy2.origin = Country(context: moc)
                candy2.origin?.shortName = "UK"
                candy2.origin?.fullName = "United Kingdom"

                let candy3 = Candy(context: moc)
                candy3.name = "Twix"
                candy3.origin = Country(context: moc)
                candy3.origin?.shortName = "UK"
                candy3.origin?.fullName = "United Kingdom"

                let candy4 = Candy(context: moc)
                candy4.name = "Toblerone"
                candy4.origin = Country(context: moc)
                candy4.origin?.shortName = "CH"
                candy4.origin?.fullName = "Switzerland"

                try? moc.save()
            }
        }
            
                /**
                 day 58 part 3
                 
                 FilteredList(filter: "lastName", filterValue: lastNameFilter) { (singer: Singer) in
                 Text("\(singer.wrappedFirstName) \(singer.wrappedLastName)")
                 }
                 
                 Button("Add Examples") {
                 let taylor = Singer(context: moc)
                 taylor.firstName = "Taylor"
                 taylor.lastName = "Swift"
                 
                 let ed = Singer(context: moc)
                 ed.firstName = "Ed"
                 ed.lastName = "Sheeran"
                 
                 let adele = Singer(context: moc)
                 adele.firstName = "Adele"
                 adele.lastName = "Adkins"
                 
                 changeDataModel()
                 }
                 
                 Button("Show A") {
                 lastNameFilter = "A"
                 }
                 
                 Button("Show S") {
                 lastNameFilter = "S"
                 }
                 */
                
    }
    
    func changeDataModel() {
        if moc.hasChanges {
            // Apple g
            try? moc.save()
        }
    }
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}
