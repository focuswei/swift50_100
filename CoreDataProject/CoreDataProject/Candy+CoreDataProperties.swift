//
//  Candy+CoreDataProperties.swift
//  CoreDataProject
//
//  Created by guozw2 on 2023/5/9.
//
//

import Foundation
import CoreData


extension Candy {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Candy> {
        return NSFetchRequest<Candy>(entityName: "Candy")
    }

    @NSManaged public var name: String?
    @NSManaged public var origin: Country?

    public var wrappedName: String {
        name ?? "Unknown name"
    }

}

extension Candy : Identifiable {

}
