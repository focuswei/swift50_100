//
//  ConstraintView.swift
//  CoreDataProject
//
//  Created by guozw2 on 2023/5/9.
//  day 57

import SwiftUI

struct ConstraintView: View {
    struct Student: Hashable {
        let name: String
    }
    let students = [Student(name: "Harry Potter"), Student(name: "Hermione Granger")]
    @FetchRequest(sortDescriptors: []) var wizards: FetchedResults<Wizard>
    @Environment(\.managedObjectContext) var moc
    var body: some View {
        VStack {
            List(wizards, id:\.self) { w in
                Text(w.name ?? "Unknown")
            }
            
            Button("Add") {
                let wizard = Wizard(context: moc)
                wizard.name = "Harry Potter"
            }
            Button("Save") {
                do {
                    try moc.save()
                } catch {
                    // 修改Wizard的Inspectors 中的 Constraint="name",然后添加当有多个相同名字的wizard就会报错，The operation couldn’t be completed. (Cocoa error 133021.)。
                    print(error.localizedDescription)
                }
            }
        }
    }
}

//struct ConstraintView_Previews: PreviewProvider {
//    static var previews: some View {
//        ConstraintView()
//    }
//}
