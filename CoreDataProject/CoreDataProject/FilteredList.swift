//
//  FilteredList.swift
//  CoreDataProject
//
//  Created by guozw2 on 2023/5/9.
//
import CoreData
import SwiftUI

fileprivate enum MyPredicate: String, CaseIterable, Sendable {
    case beginsWith
    
    init?(rawValue: String) {
        switch rawValue {
        case "beginsWith":
            self = .beginsWith
        default:
            self = .beginsWith
        }
    }
}
extension MyPredicate {
    // 大写
    var predicateString: String {
        return self.rawValue.uppercased()
    }
}
struct FilteredList<T: NSManagedObject, Content: View>: View {
    // 动态筛选 xcdatamodel的结果，不是讲返回结果筛选，而且单独的筛选对象
    // 相当于private var _fetchRequest = FetchRequest<FetchedResults<Singer>>(); var fetchRequest: FetchedResults<Singer> { get { return _fetchRequest.wrappedValue }};var $fetchRequest: Binding<FetchRequest<Result>.Configuration> { get { return _fetchRequest.projectedValue }};
    @FetchRequest var fetchRequest: FetchedResults<T>
    
    
    
    @Binding private var predicateEnum: MyPredicate
    let content: (T) -> Content
    
    var body: some View {
        List {
            ForEach(fetchRequest, id: \.self) { singer in
                // 把singer 通过闭包传递
                self.content(singer)
//                Text("\(singer.wrappedFirstName) \(singer.wrappedLastName)")
            }
        }
    }
    
    init(filter: String, filterValue: String,sort: [NSSortDescriptor], @ViewBuilder content: @escaping (T) -> Content) {
        // 自定义生成struct FetchRequest<>本身,因为如果不能在 declare阶段输入 @Binding 字段,这种初始化方法适合动态改变参数但是不能用 @Binding 的情况。
        // 但是有一个弊端，就是每当 body:View 更新的时候，都会初始化 _fetchRequest, 仅仅是 CoreData 避免了重新进行数据库查询，很消耗CPU
//        _fetchRequest = FetchRequest<Singer>(sortDescriptors: [], predicate: NSPredicate(format: "lastName BEGINSWITH %@", filter))
        _fetchRequest = FetchRequest(sortDescriptors: sort, predicate: NSPredicate(format: "%K BEGINSWITH %@", filter, filterValue))
        // 在闭包的执行中决定 范型的类型
        self.content = content
        // 挑战，1.输入断言的字符串 2.修改断言的字符串为枚举 3.接收一个SortDescriptor的数组
        _predicateEnum = .constant(MyPredicate.init(rawValue: filter)!)
        if let predicate = MyPredicate.init(rawValue: filter) {
            _fetchRequest = FetchRequest(sortDescriptors: sort, predicate: NSPredicate(format: "%K %K %@", filter, predicate.predicateString, filterValue))
        }
    }
    
}

//struct FilteredList_Previews: PreviewProvider {
//    static var previews: some View {
//        FilteredList()
//    }
//}
