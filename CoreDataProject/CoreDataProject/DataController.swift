//
//  DataController.swift
//  CoreDataProject
//
//  Created by guozw2 on 2023/5/8.
//

import Foundation
import CoreData

class DataController: ObservableObject {
    let container = NSPersistentContainer(name: "CoreDataProject")
    
    init() {
        container.loadPersistentStores { _, error in
            if let error = error {
                // NSUnderlyingException=Constraint unique violation: UNIQUE constraint failed: ZWIZARD.ZNAME, reason=constraint violation during attempted migration, NSExceptionOmitCallstacks=true} 解决方法：删除App重启
                print("Core Data failed to load: \(error.localizedDescription)")
                return
            }
            /**
             CoreData合并策略，解决因冲突造成的error
             The operation couldn’t be completed. (Cocoa error 133021.)
             */
            self.container.viewContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        }
    }
}
