//
//  CoreDataProjectApp.swift
//  CoreDataProject
//
//  Created by guozw2 on 2023/5/8.
//

import SwiftUI

@main
struct CoreDataProjectApp: App {
    @State private var dataController = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, dataController.container.viewContext)
        }
    }
}
