//
//  PredicateView.swift
//  CoreDataProject
//
//  Created by guozw2 on 2023/5/9.
//

import SwiftUI

struct PredicateView: View {
    // NSPredicate 除了== ''，我们还可以使用诸如<和 之类的比较>来过滤我们的对象：NSPredicate(format: "universe IN %@", ["Aliens", "Firefly", "Star Trek"])
    @FetchRequest(sortDescriptors: [], predicate: NSPredicate(format: "name like 'Executor'")) var ships: FetchedResults<Ship>
    @Environment(\.managedObjectContext) var moc
    
    var body: some View {
        VStack {
            List {
                ForEach(ships, id: \.self) { s in
                    Text(s.name ?? "Unknown name")
                }
            }
            
            Button("Add Examples") {
                let ship1 = Ship(context: moc)
                ship1.name = "Enterprise"
                ship1.universe = "Star Trek"
                
                let ship2 = Ship(context: moc)
                ship2.name = "Defiant"
                ship2.universe = "Star Trek"
                
                let ship3 = Ship(context: moc)
                ship3.name = "Millennium Falcon"
                ship3.universe = "Star Wars"
                
                let ship4 = Ship(context: moc)
                ship4.name = "Executor"
                ship4.universe = "Star Wars"
                
                try? moc.save()
            }
        }
    }
}

//struct PredicateView_Previews: PreviewProvider {
//    static var previews: some View {
//        PredicateView()
//    }
//}
