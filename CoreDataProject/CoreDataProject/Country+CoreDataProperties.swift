//
//  Country+CoreDataProperties.swift
//  CoreDataProject
//
//  Created by guozw2 on 2023/5/9.
//
//

import Foundation
import CoreData


extension Country {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Country> {
        return NSFetchRequest<Country>(entityName: "Country")
    }

    @NSManaged public var countryId: Int16
    @NSManaged public var shortName: String?
    @NSManaged public var fullName: String?
    @NSManaged public var candy: NSSet?
    
    // NSSet 不能和 swiftUI 一起使用，因此需要设置一个Set，给ForEach{} 使用。
    public var candyArray: [Candy] {
        let set = (candy as? Set<Candy>) ?? []
        return set.sorted { a, b in
            return a.wrappedName < b.wrappedName
        }
    }
    
    public var wrappedShortName: String {
        shortName ?? "Unknown Country"
    }

    public var wrappedFullName: String {
        fullName ?? "Unknown Country"
    }
}

// MARK: Generated accessors for candy
extension Country {

    @objc(addCandyObject:)
    @NSManaged public func addToCandy(_ value: Candy)

    @objc(removeCandyObject:)
    @NSManaged public func removeFromCandy(_ value: Candy)

    @objc(addCandy:)
    @NSManaged public func addToCandy(_ values: NSSet)

    @objc(removeCandy:)
    @NSManaged public func removeFromCandy(_ values: NSSet)

}

extension Country : Identifiable {

}
