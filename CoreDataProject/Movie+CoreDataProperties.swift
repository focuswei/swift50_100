//
//  Movie+CoreDataProperties.swift
//  CoreDataProject
//
//  Created by guozw2 on 2023/5/8.
//
//

import Foundation
import CoreData


extension Movie {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Movie> {
        return NSFetchRequest<Movie>(entityName: "Movie")
    }
    // 如果在这里修改，但是CoreData 是 lazy 关键字修饰的
    @NSManaged public var title: String?
    @NSManaged public var year: Int16
    @NSManaged public var director: String?
    public var wrappedTitle: String {
        title ?? "Unknown Title"
    }

}

extension Movie : Identifiable {

}
