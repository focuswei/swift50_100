//
//  TapView.swift
//  Flashzilla
//
//  Created by guozw2 on 2023/6/7.
//

import SwiftUI

struct TapView: View {
    @State private var currentAmount = Angle.zero
    @State private var finalAmount = Angle.zero
    
    var body: some View {
            VStack {
                Text("Hello, World!")
                    .onTapGesture {
                        print("Text tapped")
                    }
            }
            .highPriorityGesture(
                
                TapGesture().onEnded { _ in
                    print("VStack tapped")
                }
            )
        }
}

struct TapView_Previews: PreviewProvider {
    static var previews: some View {
        TapView()
    }
}
