//
//  TimerView.swift
//  Flashzilla
//
//  Created by guozw2 on 2023/6/7.
//

import SwiftUI

struct TimerView: View {
    // publisher 每一秒触发一次，在主线程，在公共循环运行，容差0.5秒
    let timer = Timer.publish(every: 1, tolerance: 0.5, on: .main, in: .common).autoconnect()
    
    @State private var counter = 0
    
    var body: some View {
        Text("Hello, World!")
            .onReceive(timer) { time in
                if counter == 5 {
                    timer.upstream.connect().cancel()
                } else {
                    print("The time is now \(time)")
                }
                
                counter += 1
            }
    }
}

struct TimerView_Previews: PreviewProvider {
    static var previews: some View {
        TimerView()
    }
}
