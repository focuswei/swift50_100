//
//  Card.swift
//  Flashzilla
//
//  Created by guozw2 on 2023/6/8.
//

import Foundation

struct Card: Identifiable, Codable, Equatable {
    var id = UUID()
    let prompt: String
    let answer: String
    
    static let example = Card(prompt: "Who played the 13th Doctor in Doctor Who?", answer: "Jodie Whittaker")
    
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.id == rhs.id
    }
}
