//
//  CardViewModel.swift
//  Flashzilla
//
//  Created by guozw2 on 2023/6/25.
//

import Foundation


@MainActor class CardViewModel: ObservableObject {
    
    let jsonFileName = "Cards"
    
    private(set) var cardList = [Card]()
    
    init() {
        getList()
    }
    
    func getList() {
        let decoder = JSONDecoder()
        do {
            // mappedIfSafe : 如果可能且安全的话，指示文件应映射到虚拟内存的，提高读取速度
            let data = try Data(contentsOf: FileManager.default.getFileURL(fileName: jsonFileName), options: .mappedIfSafe)
            let array = try decoder.decode([Card].self, from: data)
            cardList = array
        } catch {
            
        }
    }
    
    func saveList(cards: [Card]) {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(cards)
            try data.write(to: FileManager.default.getFileURL(fileName: jsonFileName))
        } catch  {
            
        }
    }
    
    func add(_ card: Card, at index: Int = 0) {
        if index > cardList.count {
            cardList.append(card)
        } else {
            cardList.insert(card, at: index)
        }
        saveList(cards: cardList)
    }
    
    func delete(_ card: Card) {
        if let index = cardList.firstIndex(where: { $0 == card }) {
            cardList.remove(at: index)
            saveList(cards: cardList)
        }
    }
    
    func delete(at index: Int) {
        cardList.remove(at: index)
        saveList(cards: cardList)
    }
}
