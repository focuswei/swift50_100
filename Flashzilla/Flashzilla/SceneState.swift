//
//  SceneState.swift
//  Flashzilla
//
//  Created by guozw2 on 2023/6/7.
//

import SwiftUI

struct SceneState: View {
    @Environment(\.scenePhase) var scenePhase
    
    var body: some View {
       
        
        Text("Hello world!!")
            .padding()
            .onChange(of: scenePhase) { newValue in
                if newValue == .active {
                    print("Active")
                } else if newValue == .inactive {
                    print("inactive")
                } else if newValue == .background {
                    print("background")
                }
            }
    }
}

struct SceneState_Previews: PreviewProvider {
    static var previews: some View {
        SceneState()
    }
}
