//
//  HapticsView.swift
//  Flashzilla
//
//  Created by guozw2 on 2023/6/7.
//

import SwiftUI
import CoreHaptics
struct HapticsView: View {
    // 只有 iPhone才有
    @State private var engine: CHHapticEngine?
    
    var body: some View {
        VStack {
            Form {
                Text("Success")
                    .onAppear(perform: prepareHaptics)
                    .onTapGesture(perform: complexSucces)
                Text("Error")
                    .onTapGesture(perform: simpleError)
                Text("Warning")
                    .onTapGesture(perform: simpleWarning)
            }
        }
    }
    
    /// 依赖触觉的用户，例如盲人用户 触觉反馈
    func simpleSuccess() {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
    }
    
    func simpleError() {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.error)
    }
    
    func simpleWarning() {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.warning)
    }
    
    func prepareHaptics() {
        guard CHHapticEngine.capabilitiesForHardware().supportsHaptics else { return }

        do {
            engine = try CHHapticEngine()
            try engine?.start()
        } catch {
            print("There was an error creating the engine: \(error.localizedDescription)")
        }
    }
    
    func complexSucces() {
        guard CHHapticEngine.capabilitiesForHardware().supportsHaptics else {
            return
        }
        var events = [CHHapticEvent]()
        
        let intensity = CHHapticEventParameter(parameterID: .hapticIntensity, value: 1)
        let sharpness = CHHapticEventParameter(parameterID: .hapticSharpness, value: 1)
        let event = CHHapticEvent(eventType: .hapticTransient, parameters: [intensity, sharpness], relativeTime: 0)
        events.append(event)

        
        // convert those events into a pattern and play it immediately
        do {
            let pattern = try CHHapticPattern(events: events, parameters: [])
            let player = try engine?.makePlayer(with: pattern)
            try player?.start(atTime: 0)
        } catch {
            
        }
    }
}

struct HapticsView_Previews: PreviewProvider {
    static var previews: some View {
        HapticsView()
    }
}
