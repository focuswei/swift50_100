//
//  CardView.swift
//  Flashzilla
//
//  Created by guozw2 on 2023/6/8.
//

import SwiftUI

struct CardView: View {
    
    enum AnswerType {
        case correct
        case wrong
    }
    @State private var isShowingAnswer = false
    @State private var offset = CGSize.zero
    // 触觉反馈需要1-2秒时间准备时间，并不是马上反馈。
    @State private var feedback = UINotificationFeedbackGenerator()
    
    //增加辅助功能，查看手机设置-> 红绿色盲
    @Environment(\.accessibilityDifferentiateWithoutColor) var differentiateWithoutColor
    
    @Environment(\.accessibilityVoiceOverEnabled) var voiceOverEnabled
    let card: Card
    var removal: ((AnswerType) -> Void)? = nil
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 25, style: .continuous)
                .fill(differentiateWithoutColor ? .white
                      : .white.opacity(1 - Double(abs(offset.width / 50)))
                )
                .background(
                    RoundedRectangle(cornerRadius: 25, style: .circular)
                        .fill(offset.width > 0 ? (differentiateWithoutColor ? .pink
                                                  :.green)
                              : (offset == .zero ? .white
                                 : (differentiateWithoutColor ? .green:.pink)))
                )
                .shadow(radius: 10)
            
            VStack {
                if voiceOverEnabled {
                    Text(isShowingAnswer ? card.answer : card.prompt)
                        .font(.largeTitle)
                        .foregroundColor(.black)
                } else {
                    Text(card.prompt)
                        .font(.largeTitle)
                        .foregroundColor(.black)
                    
                    Text(card.answer)
                        .font(.title)
                        .foregroundColor(isShowingAnswer ? .gray
                                           :.white)
                }
                
            }
            .padding(20)
            .multilineTextAlignment(.center)
            
        }
        .accessibilityAddTraits(.isButton)
        .frame(width: 450, height: 250)
        .onTapGesture {
            isShowingAnswer.toggle()
        }
        .task {
            let string = try? await requestAgent()
            print("Success: \(string)")
        }
        .rotationEffect(.degrees(Double(offset.width / 5))) // 先旋转
        .offset(x: offset.width * 5, y: 0)  // 然后平移
        .opacity(2 - Double(abs(offset.width / 50))) // 超过屏幕一般就隐藏
        .gesture(
            // 手势导致VoiceOver 1）不能识别卡片是按钮 2）答案被揭晓却没有语音提示 3）盲人用户无法通过左滑右滑
            DragGesture()
                .onChanged{ gesture in
                    // 0.7 增加阻力
                    offset = CGSizeMake(gesture.translation.width * 0.7, gesture.translation.height * 0.7) // 从拖动手势开始到拖动的当前事件结束的总转换
                    feedback.prepare()
                }
                .onEnded { _ in
                    // abs 不管左滑或者右滑，都触发
                    if abs(offset.width) > 100 {
                        var answerType = AnswerType.correct
                        if offset.width > 0 {
                            feedback.notificationOccurred(.success)
                        } else {
                            answerType = .wrong
                            feedback.notificationOccurred(.error)
                        }
                        removal?(answerType)
                    } else {
                        offset = .zero
                    }
                })
        .animation(.spring(), value: offset)
        
    }
    
    func requestAgent() async throws -> String {
        let url = URL(string: "http://202.72.14.48:40303/ctPosttestIphone/AgentInfo.ashx")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = "token=9faf2e08-af68-47e0-9211-06c0c665bc41&type=agentdetail".data(using: .utf8)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//        request.setValue("9faf2e08-af68-470-9211-06c0c665bc41", forHTTPHeaderField: "token")
//        request.setValue("agentdetail", forHTTPHeaderField: "type")
        let (data, response) = try await URLSession.shared.data(for: request)
        return String(data: data, encoding: .utf8) ?? "Bad Request"
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(card: Card.example)
    }
}
