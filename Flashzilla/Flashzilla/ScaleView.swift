//
//  ScaleView.swift
//  Flashzilla
//
//  Created by guozw2 on 2023/6/6.
//

import SwiftUI

struct ScaleView: View {
    @State private var currentAmount = 0.0
        @State private var finalAmount = 1.0

        var body: some View {
            Text("Hello, World!")
                .scaleEffect(finalAmount + currentAmount)
                .gesture(
                    MagnificationGesture()
                        .onChanged { amount in
                            currentAmount = amount - 1
                        }
                        .onEnded { amount in
                            finalAmount += currentAmount
                            currentAmount = 0
                        }
                )
        }
}

struct scaleView_Previews: PreviewProvider {
    static var previews: some View {
        ScaleView()
    }
}
