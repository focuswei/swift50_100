//
//  FlashzillaApp.swift
//  Flashzilla
//
//  Created by guozw2 on 2023/6/6.
//

import SwiftUI

@main
struct FlashzillaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
