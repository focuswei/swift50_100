//
//  ContentView.swift
//  Flashzilla
//
//  Created by guozw2 on 2023/6/6.
//

import SwiftUI

struct ContentView: View {
    // 色盲选项
    @Environment(\.accessibilityDifferentiateWithoutColor) var differentiateWithoutColor
    // 存储App当前是否活跃
    @Environment(\.scenePhase) var scenePhase
    @Environment(\.accessibilityVoiceOverEnabled) var voiceOverEnabled
    
    // TODO: 换成 vm.cardList
    @State private var cards = [Card]()
    // 增加计算属性100，给用户一些选择压力，10秒内完成10个卡
    @State private var timeRemaining = 100
    @State private var isActive = true
    @State private var showingEditScreen = false
    // 计时器 publisher
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    // ViewModel
    @StateObject var vm = CardViewModel()
    
    var body: some View {
        ZStack {
            VStack {
                GeometryReader { geo in
                    Image(decorative:"background") // decorative 让背景不被读取
                        .resizable()
                        .scaledToFill() // 这个影响到别的Button拉伸
                        .ignoresSafeArea()
                }
            }
            
            
            VStack {
                Text("Time: \(timeRemaining)")
                    .font(.largeTitle)
                    .fixedSize()
                    .foregroundColor(.white)
                    .padding(.horizontal, 20)
                    .padding(.vertical, 10)
                    .background(.black.opacity(0.5))
                    .clipShape(Capsule())
                
                ZStack {
                    
                    ForEach(cards) { card in
                        CardView(card: card, removal: { answerType in
                            withAnimation {
                                removeCard(card: card, answerType: answerType)
                            }
                        })
                        .stacked(at: cards.firstIndex(of: card), in: cards.count)
                        .allowsHitTesting(hitIndex: cards.firstIndex(of: card), cardCount: cards.count)  // 防止测试时点击到当前卡片后面的卡
                            .accessibilityHidden(hitIndex: cards.firstIndex(of: card), cardCount: cards.count) // 防止VoiceOver提前读取到下面的信息
                        
                    }
                }
                .allowsHitTesting(timeRemaining > 0) // 禁止交互测试
                
                if cards.isEmpty {
                    Button("Start Again", action: resetCards)
                        .padding(.vertical, 10)
                        .background(.white)
                        .foregroundColor(.black)
                        .clipShape(Capsule())
                }
            }
            
            VStack {
                HStack {
                    Spacer()
                    
                    Button {
                        showingEditScreen = true
                    } label: {
                        Image(systemName: "plus.circle")
                            .padding()
                            .background(.black.opacity(0.7))
                            .clipShape(Circle())
                    }
                }
                
                Spacer()
            }
            .foregroundColor(.white)
            .font(.largeTitle)
            .padding()
            
            if differentiateWithoutColor || voiceOverEnabled {
                // 告诉色盲用户，哪边是正确含义或者错误。
                VStack {
                    Spacer()
                    
                    HStack {
                        Button {
                            // 告诉盲人这是按钮
                            withAnimation {
                                removeCard(at: cards.count - 1)
                            }
                        } label: {
                            Image(systemName: "xmark.circle")
                                .padding()
                                .background(.black.opacity(0.6))
                                .clipShape(Circle())
                        }
                        .accessibilityLabel("Wrong")
                        .accessibilityHint("Mark your answer as being incorrent")
                        
                        Spacer()
                        Button {
                            withAnimation {
                                removeCard(at: cards.count - 1)
                            }
                        } label: {
                            Image(systemName: "checkmark.circle")
                                .padding()
                                .background(.black.opacity(0.7))
                                .clipShape(Circle())
                        }
                        .accessibilityLabel("Correct")
                        .accessibilityHint("Mark your answer as being incorrent")
                        
                    }
                    .foregroundColor(.white)
                    .font(.largeTitle)
                    .padding()
                    
                }
            }
        }
        .sheet(isPresented: $showingEditScreen, onDismiss: resetCards, content: EditCards.init) // FIXME:  当想读取sheet，以EditCards初始化程序返回视图，前提是初始化init() 不需要任何参数，如果需要参数则改用闭包传递 () -> View
        .onReceive(timer) { time in
            guard isActive else { return }
            if timeRemaining > 0  {
                timeRemaining -= 1
            }
//            else if timeRemaining == 0 {
//                timeRemaining = 10
//            }
        }
        .onChange(of: scenePhase) { newPhase in
            if newPhase == .active {
                if cards.isEmpty == false {
                    isActive = true
                }
            } else {
                isActive = false
            }
        }
        .onAppear(perform: resetCards)
        
        /**
         VStack {
             Text("Hello")
             Spacer().frame(height: 100)
             Text("World")
         }
         .contentShape(Rectangle()) // 控制交互区域
         .onTapGesture {
             print("VStack tapped")
         }
         */
    }
    
    func loadDataFromUserDefault() {
        if let data = UserDefaults.standard.data(forKey: "Cards") {
            if let decoded = try? JSONDecoder().decode([Card].self, from: data) {
                cards = decoded
            }
        }
    }
    
    func loadDataFromJson() {
        cards = vm.cardList
    }
    func resetCards() {
        timeRemaining = 100
        isActive = true
        loadDataFromUserDefault()
    }
    
    func removeCard(at index: Int) {
        guard index >= 0 else { return }
        let removeItem = cards.remove(at: index)
        vm.delete(removeItem)
        if cards.isEmpty {
            isActive = false
        }
    }
    
    func removeCard(card: Card, answerType: CardView.AnswerType = .correct) {
        if let index = cards.firstIndex(where: { $0 == card }) {
            removeCard(at: index)
            if answerType == .correct {
                
            } else {
                var tempCard = card
                tempCard.id = UUID()
                cards.insert(tempCard, at: 0)
                // vm add
                vm.add(card)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

extension View {
    // 自定义修饰符，将View 偏移 .offset 的计算隐藏实现
    func stacked(at position: Int?, in total: Int) -> some View {
        guard let p = position else { return self.offset() }
        let offset = Double(total - p)
        return self.offset(x: 0, y: offset * 10)
    }
    
    func allowsHitTesting(hitIndex: Int?, cardCount: Int) -> some View {
        guard let index = hitIndex else { return self.allowsHitTesting(true) }
        return self.allowsHitTesting(index == cardCount - 1)
    }
    
    func accessibilityHidden(hitIndex: Int?, cardCount: Int) -> some View {
        guard let index = hitIndex else { return self.accessibilityHidden(true) }
        return self.accessibilityHidden(index == cardCount - 1)
    }
}
