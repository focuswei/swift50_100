//
//  FileManager+Tool.swift
//  Flashzilla
//
//  Created by guozw2 on 2023/6/21.
//

import Foundation


extension FileManager {
    static var documentDirectory: URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getFileURL(fileName: String) -> URL {
        return FileManager.documentDirectory.appending(component: fileName)
    }
    
    func save(filedata: Data) {
        do {
            try filedata.write(to: FileManager.documentDirectory)
        } catch {
            
        }
    }
    
    func loadData(url: URL) -> Data? {
        if let data = try? Data(contentsOf: url) {
            return data
        }
        return nil
    }
}

