//
//  CombineGesture.swift
//  Flashzilla
//
//  Created by guozw2 on 2023/6/7.
//

import SwiftUI

struct CombineGesture: View {
    // how far the circle has been dragged
    @State private var offset = CGSize.zero
    
    /// whether it is currently being dragged or not
    @State private var isDragging = false
    var body: some View {
        let dragGesture = DragGesture()
            .onChanged { value in
                offset = value.translation
            }
            .onEnded { value in
                withAnimation {
                    offset = .zero
                    isDragging = false
                }
            }
        
        let pressGesture = LongPressGesture()
            .onEnded { value in
                withAnimation {
                    isDragging = true
                }
            }
        
        let combine = pressGesture.sequenced(before: dragGesture)
        
        Circle()
            .fill(.red)
            .frame(width: 64, height: 64)
            .scaleEffect(isDragging ? 1.5 : 1)
            .offset(offset)
            .gesture(combine)
    }
}

struct combineGesture_Previews: PreviewProvider {
    static var previews: some View {
        CombineGesture()
    }
}
