//
//  AccessibilityView.swift
//  Flashzilla
//
//  Created by guozw2 on 2023/6/7.
//

import SwiftUI

struct AccessibilityView: View {
    // 对色盲友好 Differentiate Without Color.
    @Environment(\.accessibilityDifferentiateWithoutColor) var differentiateWithoutColor
    
    // reduce Motion 减弱动态效果
    @Environment(\.accessibilityReduceMotion) var reduceMotion
    // reduce Transparency 降低透明度
    @Environment(\.accessibilityReduceTransparency) var reduceTransparency
    @State private var scale = 1.0
    
    var body: some View {
        
        Text("Hello, World!")
                    .padding()
                    .background(reduceTransparency ? .black : .black.opacity(0.5))
                    .foregroundColor(.white)
                    .clipShape(Capsule())
        
//        HStack {
//                    if differentiateWithoutColor {
//                        Image(systemName: "checkmark.circle")
//                    }
//
//                    Text("Success")
//                }
//                .padding()
//                .background(differentiateWithoutColor ? .black : .green)
//                .foregroundColor(.white)
//                .clipShape(Capsule())
        
//                Text("Hello, World!")
//                    .scaleEffect(scale)
//                    .onTapGesture {
//                        withOptionalAnimation {
//                            scale *= 1.5
//                        }
//                    }

    }
    
    func withOptionalAnimation<Result>(_ animation: Animation? = .default, _ body: () throws -> Result) rethrows -> Result {
        if UIAccessibility.isReduceMotionEnabled {
            return try body()
        } else {
            return try withAnimation(animation, body)
        }
    }
}

struct AccessibilityView_Previews: PreviewProvider {
    static var previews: some View {
        AccessibilityView()
    }
}
