//
//  User.swift
//  Instafilter
//
//  Created by guozw2 on 2023/5/10.
//

import Foundation

struct User: Codable, Hashable {
    
    var name: String = ""
    var age: Int = 0
    var company: String = ""
    var id: String = ""
    var registered: Date = Date()
    var friends: [Friend] = []
    var tags: [String] = []
    
    enum CodingKeys: CodingKey {
        case name
        case age
        case company
        case id
        case registered
        case friends
        case tags
    }
    
    init() { }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        age = try container.decode(Int.self, forKey: .age)
        company = try container.decode(String.self, forKey: .company)
        id = try container.decode(String.self, forKey: .id)
        registered = try container.decode(Date.self, forKey: .registered)
        friends = try container.decode([Friend].self, forKey: .friends)
        tags = try container.decode([String].self, forKey: .tags)
    }
    
}


struct Friend: Codable, Hashable {
    var name: String
    var id: String
}
