//
//  UserChanllengeApp.swift
//  UserChanllenge
//
//  Created by guozw2 on 2023/5/11.
//  day 61挑战完成,请求网络数据，保存到CoreData,读取。

import SwiftUI

@main
struct UserChanllengeApp: App {
    @State private var controller = DataController()
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, controller.container.viewContext)
        }
    }
}
