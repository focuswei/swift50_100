//
//  DataController.swift
//  UserChanllenge
//
//  Created by guozw2 on 2023/5/11.
//

import Foundation
import CoreData

class DataController: ObservableObject {
    
    // EnvironmentValues 包含 NSManagedObjectContext 属于SwiftUI 与 UIKit 的桥接
    let container = NSPersistentContainer(name: "Users")
    
    init() {
        container.loadPersistentStores { _, error in
            if let error = error {
                return
            }
            
            self.container.viewContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        }
    }
}
