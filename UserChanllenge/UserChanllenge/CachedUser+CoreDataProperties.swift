//
//  CachedUser+CoreDataProperties.swift
//  UserChanllenge
//
//  Created by guozw2 on 2023/5/11.
//
//

import Foundation
import CoreData


extension CachedUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CachedUser> {
        return NSFetchRequest<CachedUser>(entityName: "CachedUser")
    }

    @NSManaged public var id: String?
    @NSManaged public var age: Int16
    @NSManaged public var company: String?
    @NSManaged public var registered: Date?
    @NSManaged public var name: String?
    @NSManaged public var tagsString: String?
    @NSManaged public var friends: NSSet?

    public var wrappedId: String {
        return id ?? ""
    }
    
    public var wrappedName: String {
        return name ?? ""
    }
    
    public var wrappedTags: [String] {
        return tagsString?.components(separatedBy: ",") ?? []
    }
    
    public var wrappedFriends: [CachedFriend] {
        let friends = (friends as? Set<CachedFriend>) ?? []
        return friends.sorted { a, b in
            return a.wrappedId > b.wrappedId
        }
    }
}

// MARK: Generated accessors for friends
extension CachedUser {

    @objc(addFriendsObject:)
    @NSManaged public func addToFriends(_ value: CachedFriend)

    @objc(removeFriendsObject:)
    @NSManaged public func removeFromFriends(_ value: CachedFriend)

    @objc(addFriends:)
    @NSManaged public func addToFriends(_ values: NSSet)

    @objc(removeFriends:)
    @NSManaged public func removeFromFriends(_ values: NSSet)

}

extension CachedUser : Identifiable {

}
