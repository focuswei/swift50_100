//
//  CachedFriend+CoreDataProperties.swift
//  UserChanllenge
//
//  Created by guozw2 on 2023/5/11.
//
//

import Foundation
import CoreData


extension CachedFriend {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CachedFriend> {
        return NSFetchRequest<CachedFriend>(entityName: "CachedFriend")
    }

    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var user: CachedUser?

    public var wrappedId: String {
        return id ?? ""
    }
    
    public var wrappedName: String {
        return name ?? ""
    }
}

extension CachedFriend : Identifiable {

}
