//
//  UsersChallenge.swift
//  Instafilter
//
//  Created by guozw2 on 2023/5/11.
//

import SwiftUI
import CoreData

struct UsersChallenge: View {
    @Environment(\.managedObjectContext) private var moc
    
    @Binding var dataIsChange: Bool
    @State private var users: [User] = []
    @State private var showNetworkFailure = false
    @State private var confirmationMessage = ""
    private var dateFormatter = ISO8601DateFormatter()
    
    var body: some View {
        VStack {
            List {
                // Generic struct 'ForEach' requires that 'User' conform to 'Hashable'
                ForEach(self.users, id: \.self) { user in
                    Section {
                        Text("userName: \(user.name)")
                        Text("company: \(user.company)")
                        Text("registerTime: \(dateFormatter.string(from: user.registered))")
//                        Section {
//                            Text("My Friend Here:").foregroundColor(.red)
//                            ForEach(user.friends, id: \.self) { friend in
//                                Text(friend.name)
//                                Text(friend.id)
//                                    .frame(alignment: .center)
//                                    .foregroundColor(.secondary)
//                            }
//                        }.padding(.leading, 20)
                    }
                }
            }
            
            Button("Fetch Request") {
                Task {
                    await placeUser()
                }
            }
            Button("Save CoreData") {
                Task.init(priority: .background, operation: {
                    for user in users {
                        let cacheU = CachedUser(context: moc)
                        cacheU.id = user.id
                        cacheU.name = user.name
                        cacheU.registered = user.registered
                        cacheU.company = user.company
                        cacheU.tagsString = user.tags.joined(separator: ",")
                        var friends = [CachedFriend]()
                        for f in user.friends {
                            let cacheFriend = CachedFriend(context: moc)
                            cacheFriend.id = f.id
                            cacheFriend.name = f.name
                            friends.append(cacheFriend)
                        }
                        cacheU.friends = NSSet(array: friends)
                    }
                    await coreDataChangeRequest()
                })
            }
            
            .alert("Oops", isPresented: $showNetworkFailure) {
                Button("OK") { }
            } message: {
                Text(confirmationMessage)
            }
        }
        
    }
    
    init(dataIsChange: Binding<Bool>) {
        _dataIsChange = dataIsChange
    }
    
    private func coreDataChangeRequest() async {
        // The operation couldn’t be completed. (Foundation._GenericObjCError error 0.) 因此需要MainActor.run {}
        await MainActor.run(body: {
            if moc.hasChanges {
                do {
                    try moc.save()
                    dataIsChange = true
                } catch {
                    dataIsChange = false
                    print("QQQ \(error.localizedDescription)")
                }
                
            }
        })
    }
    
    private func placeUser() async -> Void {
        
        let session = URLSession.init(configuration: URLSessionConfiguration.ephemeral)
        
        guard let url = URL(string: "https://www.hackingwithswift.com/samples/friendface.json") else { return }
        var urlRequest = URLRequest(url: url)
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        do {
            let (data, _) = try await session.data(for: urlRequest, delegate: nil)
            let jsonDecoder = JSONDecoder.init()
            jsonDecoder.dateDecodingStrategy = .iso8601
            let users = try jsonDecoder.decode([User].self, from: data)
            self.users = users
        } catch  {
            print(error)
            confirmationMessage = error.localizedDescription
        }
    }
    
}
//
//struct UsersChallenge_Previews: PreviewProvider {
//    static var previews: some View {
//        UsersChallenge()
//    }
//}
