//
//  ContentView.swift
//  UserChanllenge
//
//  Created by guozw2 on 2023/5/11.
//

import SwiftUI

struct ContentView: View {
    @FetchRequest(sortDescriptors: []) private var cacheUsers: FetchedResults<CachedUser>
    @Environment(\.managedObjectContext) var moc
    @State var dataIsChange = false
    var body: some View {
        VStack {
            UsersChallenge(dataIsChange: $dataIsChange)
                .onChange(of: dataIsChange) { newValue in
                    
                }
               
            List {
                ForEach(cacheUsers, id: \.self) { user in
                    Text("Tags: \(user.wrappedTags.joined(separator: "|"))")
                    ForEach(user.wrappedFriends, id: \.self) { cacheFriend in
                        Text("friendName: \(cacheFriend.wrappedName)")
                        Text(cacheFriend.wrappedId)
                            .frame(alignment: .center)
                            .foregroundColor(.secondary)
                    }
                }.padding(.leading, 20)
            }
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        ContentView()
    }
}
