//
//  DataController.swift
//  Bookworm
//
//  Created by guozw2 on 2023/5/6.
//

import Foundation
import CoreData


/// 为了和@StateObject 包装器一起使用
class DataController: ObservableObject {
    let container = NSPersistentContainer(name: "Bookworm")
    
    init() {
        container.loadPersistentStores { desc, error in
            if error == nil {
                desc
            }
        }
    }
}
