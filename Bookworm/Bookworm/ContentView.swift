//
//  ContentView.swift
//  Bookworm
//
//  Created by guozw2 on 2023/5/5.
//

import SwiftUI

struct ContentView: View {
    // 如果用@State 就是单向数据流, rememberMe 没有和PushButton.isOn 同步
    @State private var rememberMe = false
    
    // 非安全，临时沙盒
    @AppStorage("notes") private var notes = ""
    
    // 读取xcdatamodel要求：实时，筛选 因此使用 @FetchRequest 属性包装器，编译器自动创建 Student.swift
    //@FetchRequest(sortDescriptors: []) var students: FetchedResults<Student>
    
    // SortDescriptor 对title 属性排序，默认是ascending,可以指定第二个排序字段以防止相同的title，增加排序的字段对性能影响不大的前提是没有太多相同的数据，比如有很多title和author相同的就会比较影响。
    @FetchRequest(sortDescriptors: [SortDescriptor(\.author),
                                    SortDescriptor(\.title)]) var books: FetchedResults<Book>
    // 跟踪是否显示 AddBookView
    @State private var showingAddScreen = false
    
    // 更新 xcdatamodel 的上下文
    @Environment(\.managedObjectContext) var moc
    
    var body: some View {
        
        NavigationView {
            // xcdatamodel 具有自动生成 Identifier
            List {
                ForEach(books) { book in
                    NavigationLink {
                        DetailView(book: book)
                    } label: {
                        HStack {
                            EmojiRatingView(rating: book.rating)
                                .font(.largeTitle)
                            
                            VStack(alignment: .leading) {
                                Text(book.title ?? "Unknown Title")
                                    .font(.headline)
                                    .foregroundColor(book.rating > 1 ? Color.black:Color.red)
                                Text(book.author ?? "Unknown Author")
                                    .foregroundColor(.secondary)
                            }
                        }
                    }
                }.onDelete(perform:deleteBooks(at:)) // onDelete 只能作用在ForEach
            }
            .navigationTitle("Bookworm")
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    EditButton()
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button {
                        showingAddScreen.toggle()
                    } label: {
                        Label("Add Book", systemImage: "plus")
                    }
                }
            }
            .sheet(isPresented: $showingAddScreen) {
                AddBookView()
            }
        }
        Text("Count: \(books.count)")
        
        
        
        //        VStack {
        //            NavigationView {
        //                // 为了不超出可点击范围，放在Form, NavigationView
        //                TextEditor(text: $notes)
        //                    .navigationTitle("notes")
        //                    .padding()
        //            }
        
        //            List(students) { student in
        //                // student.name 的可选值 与普通的 Optional value 是不同的
        //                Text("\(student.name ?? "Unknown")")
        //            }
        
        //            Button("add") {
        //                let firstNames = ["Ginny", "Harry", "Hermione", "Luna", "Ron"]
        //                let lastNames = ["Granger", "Lovegood", "Potter", "Weasley"]
        //
        //                let chosenFirstName = firstNames.randomElement() ?? ""
        //                let chosenLastName = lastNames.randomElement() ?? ""
        //
        //                let student = Student(context: moc)
        //                student.id = UUID()
        //                student.name = "\(chosenFirstName) \(chosenLastName)"
        //                // 用上下文对象保存
        //                try? moc.save()
        //            }
        //
        // 这时候传递的是rememberMe,被PushButton捆绑在isOn
        //            PushButton(title: "Remember Me", isOn: $rememberMe)
        //            Text(rememberMe ? "On": "Off")
        // }
    }
    
    func deleteBooks(at offsets: IndexSet) -> Void {
        // loop sources
        for offset in offsets {
            // find
            let book = books[offset]
            
            // delete
            moc.delete(book)
        }
        
        // save the context
        try? moc.save()
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
