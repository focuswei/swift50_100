//
//  DetailView.swift
//  Bookworm
//
//  Created by guozw2 on 2023/5/6.
//

import SwiftUI
import CoreData

struct DetailView: View {
    // 为了让详情能够操作 xcdatamodel，引入环境变量
    @Environment(\.managedObjectContext) var moc
    // 删除后的UI更新是出栈
    // FIXME: 对于iPad 不适合
    @Environment(\.dismiss) var dismiss
    // 控制显示 Alert 的变量
    @State private var showingDeleteAlert = false
    
    let book: Book
    var body: some View {
        ScrollView {
            // 滑动视图
            ZStack(alignment: .bottomTrailing) {
                Image(book.genre ?? "Mystery")
                    .resizable()
                    .scaledToFit()
                
                Text(book.genre?.uppercased() ?? "FANTASY")
                    .font(.caption)
                    .fontWeight(.black)
                    .padding(8)
                    .foregroundColor(.white)
                    .background(.black.opacity(0.75))
                    .clipShape(Capsule())
                    .offset(x: -5, y: -5)
            }
            
            Text(book.title ?? "Unknown book")
                .font(.title)
                .foregroundColor(.secondary)
            
            Text(book.review ?? "No review")
                .padding()
            
            RatingView(rating: .constant(Int(book.rating)))
                .font(.largeTitle)
            
        }
        .navigationTitle(book.title ?? "Unknown book")
        .navigationBarTitleDisplayMode(.inline)
        .alert("Delete book", isPresented: $showingDeleteAlert) {
            Button("Delete", role: .destructive, action: deleteBook)
            Button("Cancel", role: .cancel, action: {})
        } message: {
            Text("Are you sure?")
        }
        .toolbar {
            Button {
                showingDeleteAlert = true
            } label: {
                Label("Delete this book", systemImage: "trash")
            }
        }
    }
    
    func deleteBook() {
        moc.delete(book)
        
        
        // try? moc.save()
        // FIXME: 删除后，ContentView 的List的NavigationLink没有更新
        dismiss()
    }
}

//struct DetailView_Previews: PreviewProvider {
//    // 导入CoreData，用示例数据
//    static let moc = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
//    static var previews: some View {
//        let book = Book(context: moc)
//        book.title = "Test book"
//        book.author = "Test author"
//        book.genre = "Fantasy"
//        book.rating = 4
//        book.review = "This way a great view; I really enjoyed it."
//        return  NavigationView {
//            DetailView(book: book)
//        }
//    }
//}
