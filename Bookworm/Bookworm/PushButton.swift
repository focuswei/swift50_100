//
//  PushButton.swift
//  Bookworm
//
//  Created by guozw2 on 2023/5/6.
//

import SwiftUI

struct PushButton: View {
    let title: String
    // 修改@State 为@Binding
    @Binding var isOn: Bool
    // 'PushButton' initializer is inaccessible due to 'private' protection level
    var onColors: [Color] = [.red, .yellow]
    var offColors: [Color] = [Color(white: 0.6), Color(white: 0.4)]
    
    var body: some View {
        Button(title) {
            isOn.toggle()
        }
        .padding()
        .background(LinearGradient(gradient: Gradient.init(colors: isOn ? onColors:offColors), startPoint: .top, endPoint: .bottom))
        .foregroundColor(.white)
        .clipShape(Capsule())
        .shadow(radius: isOn ? 0 : 5)
    }
}

struct PushButton_Previews: PreviewProvider {
    @State static var isCheck = false
    static var previews: some View {
        PushButton(title: "test", isOn: $isCheck) 
    }
}
