//
//  AddBookView.swift
//  Bookworm
//
//  Created by guozw2 on 2023/5/6.
//

import SwiftUI

struct AddBookView: View {
    @Environment(\.managedObjectContext) var moc
    // iOS 15+ 用于关闭当前视图
    @Environment(\.dismiss) var dismiss
    @State private var title = ""
    @State private var author = ""
    @State private var rating = 3
    // 怎么设置默认值是 genres 的第一个呢，把数组考虑成枚举
    @State private var genre: Genres = .Fantasy
    @State private var review = ""
    @State private var date = Date()
    
    enum Genres: String, CaseIterable, Identifiable {
        case Fantasy
        case Horror
        case Kids
        case Mystery
        case Poetry
        case Romance
        case Thriller
        var id: Self { self }
    }
    
    let genres = Genres.allCases
    var body: some View {
        NavigationView {
            Form {
                Section {
                    // $ 符号用于 Binding<T>值绑定
                    TextField("Name of book", text: $title)
                    // onChange 来监听输入的内容
                    TextField("Author 's name", text: $author)
                    
                    Picker("Genre", selection: $genre) {
                        // 批量创建 唯一标识和视图实例
                        ForEach(genres, id: \.self) { g in
                            Text(g.rawValue)
                        }
                    }
                }
                
                Section {
                    TextEditor(text: $review)
                    
                    RatingView(rating: $rating)
                    //                    Picker("Rating", selection: $rating) {
                    //                        ForEach(0..<6) {
                    //                            Text(String($0))
                    //                        }
                    //                    }
                } header: {
                    Text("Write a review")
                }
                
                Section {
                    DatePicker(selection: $date) {
                        Label("choose date", systemImage: "calendar.badge.clock")
                    }
                }
                
                Section {
                    Button("Save") {
                        // 挑战：通过强制默认，验证表单和未知类型的默认图片三种方式。（完成）
                        saveData()
                    }.disabled(validateInputMessage())
                }
            }
            .navigationTitle("Add Book")
        }
    }
    
    private func validateInputMessage() -> Bool {
        var isValidate = title.isEmpty != false || author.isEmpty != false || review.isEmpty != false
        return isValidate
    }
    
    private func saveData() {
        let newBook = Book(context: moc)
        newBook.id = UUID()
        newBook.title = title
        newBook.author = author
        newBook.review = review
        newBook.rating = Int16(rating)
        newBook.genre = genre.rawValue
        try? moc.save()
        
        dismiss()
    }
}

//struct AddBookView_Previews: PreviewProvider {
//    static var previews: some View {
//        AddBookView()
//    }
//}
