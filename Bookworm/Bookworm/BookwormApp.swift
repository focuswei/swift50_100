//
//  BookwormApp.swift
//  Bookworm
//
//  Created by guozw2 on 2023/5/5.
//

import SwiftUI

@main
struct BookwormApp: App {
    // 假设这个App只用一个xcdatamodel
    @StateObject private var dataController = DataController()
    
    var body: some Scene {
        WindowGroup {
            // 通过修饰符.environment 放入指定路径的环境变量, 而使用 @Environment 包装器就是读取值
            // viewContext 是为了让我们能够在内存中读取 xcdatamodel
            ContentView()
                .environment(\.managedObjectContext, dataController.container.viewContext)
        }
    }
}
