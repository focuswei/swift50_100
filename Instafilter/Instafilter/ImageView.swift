//
//  ImageView.swift
//  Instafilter
//
//  Created by guozw2 on 2023/5/12.
//

import SwiftUI
import CoreImage
import CoreImage.CIFilterBuiltins

struct ImageView: View {
    @State private var image: Image?
    // 创建上下文和过滤器
    private let context = CIContext()
    private let currentFilter = CIFilter.twirlDistortion()
    var body: some View {
        VStack {
            image?
                .resizable()
                .scaledToFit()
        }.onAppear(perform: loadImage)
    }
    
    func loadImage() {
        //        image = Image(uiImage: UIImage(named: "Example")!)
        guard let beginImage = CIImage(image: UIImage(named: "Example")!) else { return }
        // 输入
        currentFilter.inputImage = beginImage
        // 全棕色 sepiaTone
        // currentFilter.intensity = 1
        // 像素画 pixellate
        //        currentFilter.scale = 100
        // 水晶crystallize
        //        currentFilter.radius = 200
        // 旋转失真，神威效果
        //        currentFilter.radius = 1000
        //        currentFilter.center = CGPoint(x: beginImage.extent.size.width / 2, y: beginImage.extent.size.height / 2)
        // 查询key
        let inputKeys = currentFilter.inputKeys
        let amount = 5.0
        // 通过key-value匹配，支持查询
        if inputKeys.contains(kCIInputIntensityKey) {
            currentFilter.setValue(amount, forKey: kCIInputIntensityKey) }
        if inputKeys.contains(kCIInputRadiusKey) { currentFilter.setValue(amount * 200, forKey: kCIInputRadiusKey) }
        if inputKeys.contains(kCIInputScaleKey) { currentFilter.setValue(amount * 10, forKey: kCIInputScaleKey) }
        guard let outputImage = currentFilter.outputImage else { return }
        if let cgimage = context.createCGImage(outputImage, from: outputImage.extent) {
            let uiimage = UIImage(cgImage: cgimage)
            image = Image(uiImage: uiimage)
        }
    }
}

struct ImageView_Previews: PreviewProvider {
    static var previews: some View {
        ImageView()
    }
}
