//
//  ContentView.swift
//  Instafilter
//
//  Created by guozw2 on 2023/5/10.
//  如何突破 SwiftUI 的界限，将它连接到 Apple 的其他框架

import SwiftUI
import CoreImage
import CoreImage.CIFilterBuiltins

struct ContentView: View {
    @State private var image: Image?
    // select an image
    @State private var showImagePickerSimple = false
    // 使用了UIKit 的内容，就会自动引入UIHostingController
    @State private var inputImage: UIImage?
    @State private var filterValue = 0.5
    
    // 创建上下文成本很高
    private let context = CIContext()
    
    typealias filterProtocol = (CIFilter & CIFilterProtocol)
    // 可变的过滤器
    @State private var currentFilter: CIFilter = CIFilter.sepiaTone()
    
    // 自定义过滤器
    @State private var showingFilterSheet = false
    
    private var saver = ImageSaver()
    
    var body: some View {
        
        // 显示导航栏名字，并拥有present 功能
        NavigationView {
            VStack {
                ZStack {
                    Rectangle()
                        .fill(.secondary)
                    
                    Text("Tap to select a picture")
                        .foregroundColor(.white)
                        .font(.headline)
                    
                    image?
                        .resizable()
                        .scaledToFit()
                }.onTapGesture {
                    // @escaping () -> Void
                    showImagePickerSimple = true
                }
                
                HStack {
                    Text("Insensity")
                        .padding(5)
                    Slider(value: $filterValue, in: 0.0...1.0)
                        .onChange(of: filterValue) { _ in
                            // @escaping (V) -> Void) -> some View where V : Equatable
                            applyProcessing()
                        }
                }.padding(.vertical)
                
                HStack {
                    Button("Change Filter"){
                        showingFilterSheet.toggle()
                    }
                    .padding(.leading, 10)
                    
                    Spacer()
                    
                    //
                    Button("Save", action: saveButtonAction)
                        .padding(.trailing, 10)
                }
                
                
            }
            .padding([.horizontal, .bottom])
            .navigationTitle("Insensity")
            .onChange(of: inputImage) { newValue in
                loadImage()
            }
            .sheet(isPresented: $showImagePickerSimple) {
                // present,push,sheet,alert 尽量在 NavigationView的内容下操作， @escaping () -> Content
                ImagePicker_Simple(image: $inputImage)
            }
            .confirmationDialog("Select Filter", isPresented: $showingFilterSheet) {
                Button("sepiaTone") { setFilter(CIFilter.sepiaTone()) }
                Button("pixellate") { setFilter(CIFilter.pixellate()) }
                Button("colorMonochrome") { setFilter(CIFilter.colorMonochrome()) }
                Button("bloomFilter") { setFilter(CIFilter.boxBlur()) }
            }
        }
        /**
         VStack {
             // 虽然可以用三元运算符 image != nil ? image!:UIImage() 但是建议
 //            Image(uiImage: image != nil ? image!:UIImage()).resizable().scaledToFit()
             image?.resizable().scaledToFit()
             Button("Select Image") {
                 showImagePicker_Simple.toggle()
             }
         }
         .sheet(isPresented: $showImagePicker_Simple) {
             ImagePicker_Simple(image: $inputImage)
                 .onChange(of: inputImage) { newValue in
                     loadImage()
                 }
         }
         */
       
        
    }
    
    func setFilter(_ filter: CIFilter) {
        currentFilter = filter
        loadImage()
    }
    
    func applyProcessing() {

        let inputKeys = currentFilter.inputKeys
        let amount = Float(filterValue)
        // 通过key-value匹配，支持查询
        if inputKeys.contains(kCIInputIntensityKey) {
            currentFilter.setValue(amount, forKey: kCIInputIntensityKey)
        }
        if inputKeys.contains(kCIInputRadiusKey) {
            currentFilter.setValue(amount * 200, forKey: kCIInputRadiusKey)
        }
        if inputKeys.contains(kCIInputScaleKey) {
            currentFilter.setValue(amount * 100, forKey: kCIInputScaleKey)
        }
        
        guard let outputImage = currentFilter.outputImage else {
            return
        }
        
        if let cgimage = context.createCGImage(outputImage, from: outputImage.extent) {
            let uiImage = UIImage(cgImage: cgimage)
            image = Image(uiImage: uiImage)
        }
    }
    
    // 整理代码
    func saveButtonAction() {
        // 保存当前照片的修改状态
        guard let outputImage = currentFilter.outputImage else {
            return
        }
        
        if let cgimage = context.createCGImage(outputImage, from: outputImage.extent) {
            let uiImage = UIImage(cgImage: cgimage)
            let beginImage = CIImage(cgImage: cgimage)
            currentFilter.setValue(beginImage, forKey: kCIInputImageKey)
            saver.writeToPhotoAlbum(uiimage:  uiImage)
            saver.successCompletion = { uiimage in
                
            }
            saver.failureCompletion = { error in
                
            }
        }
    }
    
    func loadImage() -> Void {
        guard let uiimage = self.inputImage else {
            return
        }
        let beginImage = CIImage(image: uiimage)
        currentFilter.setValue(beginImage, forKey: kCIInputImageKey)
        applyProcessing()
    }
            

    
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}
