//
//  AddAnyView.swift
//  Instafilter
//
//  Created by guozw2 on 2023/5/10.
//

import SwiftUI

struct AddAnyView: View {
    // 方法一，使用AnyView初始化
//    var randomText: some View {
//        if Bool.random() {
//            return AnyView(Text("Hello World")).frame(width: 300)
//        }
//        return AnyView(Text("Hello Kitty"))
//    }
    
    // 方法二，使用ViewBuilder
//    var randomText: some View {
//        if Bool.random() {
//            Text("Hello World").frame(width: 300)
//        } else {
//            Text("Hello Kitty")
//        }
//    }
    
    // 方法三 Group
    var randomText: some View {
        Group {
            if Bool.random() {
                Text("Hello, World!")
                    .frame(width: 300)
            } else {
                Text("Hello, Kitty")
            }
        }
    }
    
    // 方法四，使用三元运算符
    var body: some View {
        randomText
    }
}

//struct AddAnyView_Previews: PreviewProvider {
//    static var previews: some View {
//        AddAnyView()
//    }
//}
