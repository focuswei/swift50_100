//
//  InstafilterApp.swift
//  Instafilter
//
//  Created by guozw2 on 2023/5/10.
//

import SwiftUI

@main
struct InstafilterApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
