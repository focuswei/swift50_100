//
//  ImageSaver.swift
//  Instafilter
//
//  Created by guozw2 on 2023/5/15.
//

import UIKit

class ImageSaver: NSObject {
    var failureCompletion: ((_ error: Error?) -> Void)?
    var successCompletion: ((_ uiimage: UIImage) -> Void)?
    func writeToPhotoAlbum(uiimage: UIImage) {
        UIImageWriteToSavedPhotosAlbum(uiimage, self, #selector(saveImageCompletion), nil)
    }
    
    // The method selector of the completionTarget object to call. This optional method should conform to the following signature:
    @objc func saveImageCompletion(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            failureCompletion?(error)
        } else {
            successCompletion?(image)
        }
    }
}
