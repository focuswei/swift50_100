//
//  ImagePicker_Simple.swift
//  Instafilter
//
//  Created by guozw2 on 2023/5/12.
//  衔接PhotosUI 和 SwiftUI


import PhotosUI
import SwiftUI

struct ImagePicker_Simple: UIViewControllerRepresentable {
    // 值依赖绑定，告诉想用的 View
    @Binding var image: UIImage?
    
    /// 必须继承NSObject，是UIKit event action传递的基础类。
    class Coordinator: NSObject, PHPickerViewControllerDelegate {
        // 自己定义一个parent，用来传递绑定值。
        var parent: ImagePicker_Simple

        init(_ parent: ImagePicker_Simple) {
            self.parent = parent
        }
        
        // MARK: ** PHPickerViewControllerDelegate **
        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            picker.dismiss(animated: true)
            
            // 建议写法
            guard let provider = results.first?.itemProvider else { return }
            if provider.canLoadObject(ofClass: UIImage.self) {
                provider.loadObject(ofClass: UIImage.self) { (reading, error) in
                    if error == nil,
                       let image = reading as? UIImage{
                        self.parent.image = image
                    } else {
                        print(error!.localizedDescription)
                    }
                }
            }
        }
    }
    // 告诉SwiftUI 需要包装什么样的ViewController, 这里包装的是PHPickerViewController
    // Error: Type ImagePicker_Simple does not conform to protocol UIViewControllerRepresentable,
    
    // 负责创建初始视图控制器
    func makeUIViewController(context: Context) -> some UIViewController {
        var config = PHPickerConfiguration()
        config.filter = PHPickerFilter.images
        
        // 即是出现View，没办法交互，这时候需要coordinators
        let picker = PHPickerViewController(configuration: config)
//        picker.delegate = self 不能这么写，需要嵌套class
        // 初始化自动关联coordinator
        picker.delegate = context.coordinator
        return picker
    }
    
    // 负责更新
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        let coordinator: Coordinator = context.coordinator
        
        
    }
    
    // swiftUI 自动调用
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    
    
}

