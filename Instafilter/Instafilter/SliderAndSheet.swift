//
//  SliderAndSheet.swift
//  Instafilter
//
//  Created by guozw2 on 2023/5/11.
//

import SwiftUI

struct SliderAndSheet: View {
    @State private var blurAmount = 0.0 {
        // wrappedValue 使用nonmutating set 来设置值也就是State<T>的结构体是不能自身内部修改wrappedValue的值，因此修改$blurAmount 是不会触发 set{}，但是外部修改，比如直接赋值是可以修改的。
        didSet {
            print("New value is \(blurAmount)")
        }
    }
    
    @State private var showingConfirmation: Bool = false
    @State private var backgroundColor = Color.white

    
    var body: some View {
        VStack {
            Text("Hello World")
                .blur(radius: blurAmount)
                .frame(width: 300, height: 300)
                .background(backgroundColor)
                .onTapGesture {
                    showingConfirmation = true
                }
                .confirmationDialog("Change background", isPresented: $showingConfirmation) {
                    // 相当于以前的 Sheet
                    Button("red") { backgroundColor = .red }
                    Button("green") { backgroundColor = .green }
                    Button("blue") { backgroundColor = .blue }
                    Button("Cancel") { }
                } message: {
                    Text("select a new color")
                }
            // 为了解决 didSet {} 不响应，特有的解决方法是修饰符.onChange()
            Slider(value: $blurAmount, in: 0...20)
                .onChange(of: blurAmount) { newValue in
                    print("New value is \(blurAmount)")
                }
            
            Button("Random") {
                blurAmount = Double.random(in: 0...20)
            }
        }
    }
}

//struct SliderAndSheet_Previews: PreviewProvider {
//    static var previews: some View {
//        SliderAndSheet()
//    }
//}
