//
//  ContentView.swift
//  snowSeeker
//
//  Created by guozw2 on 2023/9/18.
//  new skills such as split view layouts, optional alerts, layout with transparent groups, and even an improved way of formatting lists in text. It also gave you the chance to practice lots of c

import SwiftUI

struct ContentView: View {
    // 跟踪当前水平大小的环境变量
    @Environment(\.horizontalSizeClass) var sizeClass
    
    //
    let resorts: [Resort] = Bundle.main.decode("resorts.json")
    
    @State private var searchText = ""
    
    @State private var resortAlpha = false
    @State private var resortCountryCode = false
    @State private var descending = false
    
    @StateObject var favorites = Favorites()
    
    var filteredResorts: [Resort] {
        var list = resorts
        if !searchText.isEmpty {
            list = resorts.filter { $0.name.localizedCaseInsensitiveContains(searchText) }
        } else if resortAlpha {
            list = resorts.sorted { lhs, rhs in
                descending ? lhs.name > rhs.name : lhs.name < rhs.name
            }
        } else if resortCountryCode {
            list = resorts.sorted { lhs, rhs in
                descending ? lhs.country > rhs.country : lhs.country < rhs.country
            }
        }
        return list
    }
    
    var body: some View {
        NavigationView {
           // 最多并排三个 View
            List(filteredResorts) { resort in
                NavigationLink {
//                    Text(resort.name)
                    // So instead we’re going to group them into two views: one for resort information (price and size) and one for ski information (elevation and snow depth).
                    // If we have two views in a navigation view, NavigationLink presents destinations in the secondary view.
                    
                    ResortView(resort: resort)
                            .dynamicTypeSize(.xxLarge)
                            .environmentObject(favorites)/// 单区间，最大 xLarge
                        
                        
                } label: {
                    HStack {
                        Image(resort.country)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 45, height: 25, alignment: .center)
                            .clipShape(RoundedRectangle(cornerRadius: 5))
                            .overlay(
                                RoundedRectangle(cornerRadius: 5)
                                    .stroke(.black, lineWidth: 1)
                            )
                        
                        VStack(alignment: .leading) {
                            Text(resort.name)
                                .font(.headline)
                            Text("runs: \(resort.runs)")
                                .foregroundColor(.secondary)
                        }
                        
                        // 添加♥️
                        if favorites.contains(resort) {
                            Spacer()
                            Image(systemName: "heart.fill")
                                .accessibilityLabel("This is a favorite resort")
                                .foregroundColor(.red) // 搭配 SF Symbol 更好
                        }
                    }
                }
            }
            .navigationTitle("Resorts")
            .searchable(text: $searchText, prompt: "Search for a resort") //Second, we can bind that to our List in ContentView by adding this directly below the existing navigationTitle() modifier:
            .toolbar(content: {
                ToolbarItem(placement: .topBarTrailing) {
                    Button {
                        resorteByAlpha()
                    } label: {
                        HStack(spacing: 0) {
                            Image(systemName: "textformat.size.smaller")
                            Image(systemName: "arrow.up.arrow.down")
                                .font(.caption2)
                        }
                    }
                }
                ToolbarItem(placement: .topBarTrailing) {
                    Button {
                        resorteByCountryCode()
                    } label: {
                        HStack(spacing: 0) {
                            Image(systemName: "flag")
                            Image(systemName: "arrow.up.arrow.down")
                                .font(.caption2)
                        }
                    }
                }
            })
            
            WelcomeView()
        }
        .environmentObject(favorites) // 注入环境变量
    }
    
    func resorteByAlpha() {
        if !resortCountryCode && resortAlpha {
            descending.toggle()
        } else {
            descending = false
        }
        resortCountryCode = false
        resortAlpha = true
    }
    
    func resorteByCountryCode() {
        if resortCountryCode && !resortAlpha {
            descending.toggle()
        } else {
            descending = false
        }
        resortAlpha = false
        resortCountryCode = true
    }
    
    /**
     if sizeClass == .compact {
         // 简洁的布局方式
         VStack(content: UserView.init)
     } else {
         HStack {
             UserView()
         }
     }
     */
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
