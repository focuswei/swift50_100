//
//  Favorites.swift
//  snowSeeker
//
//  Created by guozw2 on 2023/10/23.
//

import SwiftUI

class Favorites: ObservableObject {
    
    // 用户喜欢的村 ID
    private var resorts: Set<String>
    
    //UserDefaults
    private static let saveKey = "Favorites"
    
    init() {
        let resorts = Favorites.load()
        self.resorts = resorts.isEmpty == false ? resorts:[]
    }
    
    
    func contains(_ resort: Resort) -> Bool {
        resorts.contains(resort.id)
    }
    
    func add(_ resort: Resort) {
        objectWillChange.send()
        resorts.insert(resort.id)
        save()
    }
    
    func remove(_ resort: Resort) {
        objectWillChange.send()
        resorts.remove(resort.id)
        save()
    }
    
    func save() {
        if resorts.isEmpty {
            UserDefaults.standard.removeObject(forKey: Favorites.saveKey)
        } else {
            let data = try? JSONEncoder().encode(resorts)
            UserDefaults.standard.set(data, forKey: Favorites.saveKey)
        }
    }
    
    class func load() -> Set<String> {
        guard let data = UserDefaults.standard.object(forKey: Favorites.saveKey) as? Data else { return [] }
        let resort = try? JSONDecoder().decode(Set<String>.self, from: data)
        return resort ?? []
    }
}
