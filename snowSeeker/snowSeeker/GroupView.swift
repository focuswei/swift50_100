//
//  GroupView.swift
//  snowSeeker
//
//  Created by guozw2 on 2023/9/20.
//

import SwiftUI

struct GroupView: View {
    @State private var selectedUser: User? = nil
    @State private var isShowingUser = false
    @State private var layoutVertically = false
    
    var body: some View {
        Group {
            if layoutVertically {
                VStack {
                    UserView()
                }
            } else {
                HStack {
                    UserView()
                }
            }
        }
        .onTapGesture {
            layoutVertically.toggle()
        }
    }
    
    
    // 简单的"OK"Alert
//        Text("Hello World")
//            .onTapGesture {
//                selectedUser = User()
//                isShowingUser = true
//            }
//            .alert("Welcome", isPresented: $isShowingUser, presenting: selectedUser) { _ in
//
//            }
    
    // iPad 和 iPhone 有着不同的布局习惯
//    var body: some View {
//        NavigationLink {
//            Text("NewSecondary")
//        } label: {
//            Text("Hello world!")
//        }
//        .navigationTitle("Primary")
//
//        Text("Second")
//    }
}

struct GroupView_Previews: PreviewProvider {
    static var previews: some View {
        GroupView()
    }
}
