//
//  Facility.swift
//  snowSeeker
//
//  Created by guozw2 on 2023/10/23.
//

import Foundation

struct Facility: Identifiable {
    let id = UUID()
    var name: String
    
    let icons = [
        "Accommodation": "house",
        "Beginneers" : "1.circle",
        "Cross-country" : "map",
        "Eco-friendly" : "leaf.arrow.circlepath",
        "Family" : "person.3"
    ]
    
    let descriptions = [
        "Accommodation": "This resort has popular on-site accommodation.",
        "Beginners": "This resort has lots of ski schools.",
        "Cross-country": "This resort has many cross-country ski routes.",
        "Eco-friendly": "This resort has won an award for environmental friendliness.",
        "Family": "This resort is popular with families."
    ]
}
