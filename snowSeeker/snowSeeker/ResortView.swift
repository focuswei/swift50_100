//
//  ResortView.swift
//  snowSeeker
//
//  Created by guozw2 on 2023/10/20.
//

import SwiftUI

struct ResortView: View {
    let resort: Resort
    // UserInterfaceSizeClass, a regular or compact size class
    @Environment(\.horizontalSizeClass) var sizeClass
    // 动态字体
    @Environment(\.dynamicTypeSize) var typeSize
    
    @EnvironmentObject var favorites: Favorites
    
    @State private var selectedFacility: Facility?
    // 状态更新，放在与绑定控件一起, private 防止页面外的控制
    @State private var showingFacility = false
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 0, content: {
                Image(decorative: resort.id)
                    .resizable()
                    .scaledToFit()
                    .overlay(alignment: Alignment.bottomTrailing) {
                        // 图片右下角添加文字来源。
                        Text(resort.imageCredit)
                            .font(.subheadline)
                            .foregroundStyle(Color.white)
                    }
                
                HStack {
                    // 有足够位置1x4，不够位置2x2
                    if sizeClass == .compact && typeSize == .large {
                        VStack(spacing: 10) {
                            ResortDetailsView(resort: resort)
                        }
                        VStack(spacing: 10) {
                            SkiDetailsView(resort: resort)
                        }
                    } else {
                        ResortDetailsView(resort: resort)
                        SkiDetailsView(resort: resort)
                    }
                }
                .padding(.vertical)
                .background(Color.primary.opacity(0.1))
                
                Group {
                    Text(resort.description)
                        .padding(.vertical)
                    
                    Text("Facilties")
                        .font(.headline)
                    
                    
                    // day98 加上icon
                    HStack {
                        List {
                            // 创建 List
                            ForEach(resort.facilityTypes) { facility in
                                Button {
                                    showingFacility.toggle()
                                    selectedFacility = facility
                                } label: {
                                    facility.icon
                                        .font(.title)
                                }
                            }
                            
                        }
                        
                    }
                    .padding(.vertical)
                    // 可以用 .or
//                    Text(resort.facilities, format: .list(type: .and))
//                        .padding(.vertical)
                }
                .padding(.horizontal)
            })
            .alert(selectedFacility?.name ?? "More information", isPresented: $showingFacility, presenting: selectedFacility, actions: {_ in }, message: { facility in
                Text(facility.description)
            })
            
            // 文章末尾添加“喜爱”按钮
            Button(favorites.contains(resort) ? "Remove from Favorites" : "Add to Favorites") {
                if favorites.contains(resort) {
                    favorites.remove(resort)
                } else {
                    favorites.add(resort)
                }
            }
            .buttonStyle(.borderedProminent)
            .padding()
        }
        .navigationTitle("\(resort.name), \(resort.country)")
        .navigationBarTitleDisplayMode(.inline)
    }
}

#Preview {
    ResortView(resort: Resort.example)
        .environmentObject(Favorites())
}

extension Facility {
    // 确保在 VoiceOver 上使用
    var icon: some View {
        if let iconName = icons[name] {
            return Image(systemName: iconName)
                .accessibilityLabel(name)
                .foregroundColor(.secondary)
        } else {
            fatalError("Unknown facility type: \(name)")
        }
    }
    
    var description: String {
        if let message = descriptions[name] {
            return message
        } else {
            fatalError("Unknown facility type: \(name)")
        }
    }
}
