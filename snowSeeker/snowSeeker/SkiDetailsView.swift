//
//  SkiDetailsView.swift
//  snowSeeker
//
//  Created by guozw2 on 2023/10/20.
//

import SwiftUI

struct SkiDetailsView: View {
    let resort: Resort
    
    var body: some View {
        // Group 布局中立，所以由父视图决定
        Group {
            VStack {
                Text("Elevation")
                    .font(.caption.bold())
                Text("\(resort.elevation)m")
                    .font(.title3)
            }
            
            VStack {
                Text("Snow")
                    .font(.caption.bold())
                Text("\(resort.snowDepth)cm")
                    .font(.title3)
            }
        }
        .frame(maxWidth: .infinity) // 传递给子视图，让其拉伸
    }
}

#Preview {
    SkiDetailsView(resort: Resort.example)
}
