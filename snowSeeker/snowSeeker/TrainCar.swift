//
//  TrainCar.swift
//  snowSeeker
//
//  Created by guozw2 on 2023/10/20.
//

import SwiftUI
enum TrainSymbol: String {
    case front = "train.side.front.car"
    case middle = "train.side.middle.car"
    case rear = "train.side.rear.car"
}

struct TrainCar: View {
    let position: TrainSymbol
    let showFrame: Bool
    
    init(_ position: TrainSymbol, showFrame: Bool = true) {
        self.position = position
        self.showFrame = showFrame
    }
    var body: some View {
        Image(systemName: position.rawValue)
                    .background(Color("customPink"))
    }
}

struct TrainTrack: View {
    var body: some View {
        Divider()
            .frame(maxWidth: 200)
    }
}

struct DefaultSpacing: View {
    var body: some View {
        Text("Default Spacing")
        HStack(spacing: 0) {
            TrainCar(.rear)
            // adding an invisible view that modifies your layout without displaying any content.
//            Spacer()
            // opacity modifier to create an invisible version of that view
            ZStack {
                Text("")
                    .font(.largeTitle)
                    .opacity(0)
                    .background(.brown)
                TrainCar(.middle)
            }
            TrainCar(.front)
        }
        .padding()
        .background(.teal)
        TrainTrack()
    }
}
#Preview {
    VStack {
        DefaultSpacing()
    }
}
