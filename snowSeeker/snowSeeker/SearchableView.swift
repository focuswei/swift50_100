//
//  SearchableView.swift
//  snowSeeker
//
//  Created by guozw2 on 2023/9/20.
//

import SwiftUI

struct SearchableView: View {
    
    @State private var searchText = ""
    let allNames = ["Subh", "Vina", "Melvin", "Stefanie"]
    
    var body: some View {
        // 使用 searchable 必须确保在NavigationView 里面
        // 可以用计算属性处理数据
        NavigationView {
            List(filteredNames, id: \.self) { name in
                Text(name)
            }
            Text("Searching for \(searchText)")
                .searchable(text: $searchText, prompt: "Looking for something") // placeholder
                .navigationTitle("Searching")
        }
    }
    
    // () Function produces
    // 去掉 = 是计算属性
    var filteredNames: [String] {
        if searchText.isEmpty {
            return allNames
        } else {
            return allNames.filter { keyword in
                // contains
                keyword.localizedCaseInsensitiveContains(searchText)
//                keyword.range(of: searchText, options: .caseInsensitive, locale: Locale.current) != nil
            }
        }
    }
}

struct SearchableView_Previews: PreviewProvider {
    static var previews: some View {
        SearchableView()
    }
}
