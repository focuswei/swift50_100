//
//  DismissWindow.swift
//  snowSeeker
//
//  Created by guozw2 on 2023/10/20.
//

import SwiftUI

@available(iOS 17.0, *)
struct DismissWindowButton: View {
    @Environment(\.dismissWindow) private var dismissWindow


    var body: some View {
        Button("Close Auxiliary Window") {
            // Dismisses the window that’s associated with the specified identifier.
            dismissWindow(id: "auxiliary")
        }
    }
}

private struct SheetContents: View {
    @Environment(\.dismiss) private var dismiss


    var body: some View {
        Button("Sheet Close") {
            dismiss()
        }
    }
}


private struct DetailView: View {
    @State private var isSheetPresented = false
    @Environment(\.dismiss) private var dismiss // Applies to DetailView.

    var body: some View {
        Button("Show Sheet") {
            isSheetPresented = true
        }
        .sheet(isPresented: $isSheetPresented) {
            SheetContents()
            Button("Done") {
                dismiss() // Fails to dismiss the sheet. 因为 dismiss 操作的是 DetailView 而不是 Sheet
            }
        }
    }
}

#Preview {
    if #available(iOS 18.0, *) {
        DismissWindowButton()
    } else {
        HStack {
            DetailView()
        }
    }
}
