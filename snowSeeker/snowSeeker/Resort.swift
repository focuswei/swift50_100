//
//  Resort.swift
//  snowSeeker
//
//  Created by guozw2 on 2023/9/20.
//

import Foundation

struct Resort: Codable, Identifiable {
    static let allResorts: [Resort] = Bundle.main.decode("resorts.json")
    static let example = allResorts[0]
    
    let id: String
    let name: String
    let country: String
    let description: String
    let imageCredit: String
    let price: Int
    let size: Int
    let snowDepth: Int
    let elevation: Int
    let runs: Int
    // 如果是常量，那么需要符合 Codable, 变量就不用
    var facilityTypes: [Facility] {
        // 高阶用法，init 
        facilities.map(Facility.init)
    }
    let facilities: [String]
}
