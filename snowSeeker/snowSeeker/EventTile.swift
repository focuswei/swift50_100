//
//  EventTile.swift
//  snowSeeker
//
//  Created by guozw2 on 2023/10/19.
//

import SwiftUI

struct Event {
    let title: String
    let date: Date
    let location: String
    let symbol: String
}

struct EventTile: View {
    let event: Event
    let stripeHeight = 15.0
    
    var body: some View {
        HStack(alignment: .firstTextBaseline, content: {
            Image(systemName: event.symbol)
                .font(.title)
            
            VStack(alignment: .leading) {
                Text(event.title)
                    .font(.title)
                Text(event.date, format: Date.FormatStyle().day(.defaultDigits)
                    .month(.wide))
                Text(event.location)
            }
        })
        .padding()
        .padding(.top, stripeHeight)
        .background {
            ZStack(alignment: .top, content: {
                Rectangle().opacity(0.3)
                Rectangle().frame(maxHeight: stripeHeight)
            })
            .foregroundColor(.teal)
        }
        .clipShape(RoundedRectangle(cornerRadius: stripeHeight, style: /*@START_MENU_TOKEN@*/.continuous/*@END_MENU_TOKEN@*/), style: /*@START_MENU_TOKEN@*/FillStyle()/*@END_MENU_TOKEN@*/)
    }
}

#Preview {
    EventTile(event: Event.init(title: "Buy Daisies", date: Date(), location: "Flower Shop", symbol: "gift"))
}
