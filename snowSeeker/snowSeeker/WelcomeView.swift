//
//  WelcomeView.swift
//  snowSeeker
//
//  Created by guozw2 on 2023/9/25.
//

import SwiftUI

struct WelcomeView: View {
    
    @ScaledMetric(relativeTo: .title) var textSize = 10.0
    
    var body: some View {
        VStack {
            Text("Welcome to SnowSeeker!")
                .font(.largeTitle)
//                .frame(width: textSize)
            
            Text("Please select a resort from the left-hand menu; swipe from the left edge to show it.")
                .foregroundColor(.secondary)
            
            Label("chaos", systemImage: "leaf")
                .font(.title)  // padding 与 title 一起变大
                .foregroundColor(.white)
                .padding(textSize)
                .background {
                    Capsule()
                        .fill(.purple.opacity(0.75))
                }
        }
    }
}

extension View {
    // 尽量不用，保留Apple 的特点
    @ViewBuilder func phoneOnlyStackNavigationView() -> some View {
        // phone, or TV.
        if UIDevice.current.userInterfaceIdiom == .phone {
            // Deprecated
            self.navigationViewStyle(.stack)
        } else {
            self
        }
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}
