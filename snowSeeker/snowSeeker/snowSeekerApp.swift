//
//  snowSeekerApp.swift
//  snowSeeker
//
//  Created by guozw2 on 2023/9/18.
//

import SwiftUI

@main
struct snowSeekerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
        #if os(macOS)
        Window("Auxiliary", id: "auxiliary") {
            AuxiliaryContentView()
        }
        #endif
    }
}
