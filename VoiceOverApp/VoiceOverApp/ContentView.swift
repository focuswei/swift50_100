//
//  ContentView.swift
//  VoiceOverApp
//
//  Created by guozw2 on 2023/5/22.
//

import SwiftUI

struct ContentView: View {
    let pictures = [
            "ales-krivec-15949",
            "galina-n-189483",
            "kevin-horstmann-141705",
            "nicolas-tissot-335096"
        ]
    
    let labels = [
        "Tulips",
        "Frozen tree buds",
        "Sunflowers",
        "Fireworks",
    ]
    
    @State private var selectPicture = Int.random(in: 0...3)
    
    var body: some View {
        VStack {
            // decorative 将图像标记为对 VoiceOver 不重要。
            Image(decorative: "ales-krivec-15949")
                .accessibilityHidden(true)
                .scaledToFit()// 从无障碍系统隐藏视图。隐藏特性
            
            VStack {
                Text("Your score is")
                Text("1000")
                    .font(.title)
            }
            .accessibilityElement(children: .ignore)
            .accessibilityLabel("Your score is 1000") // 将多个视图组合为一个。 阅读起来就是 "Your score is 1000"
            
            Image(pictures[selectPicture])
                .resizable()
                .scaledToFit()
                .onTapGesture {
                    selectPicture = Int.random(in: 0...3)
                }
                .accessibilityLabel(Text(labels[selectPicture])) // 阅读文本
                .accessibilityHint(Text(labels[selectPicture]))  // 延迟阅读
                .accessibilityAddTraits(.isButton) // 增加一个按钮特征
                .accessibilityRemoveTraits(.isImage) // 删除图像特征
        }
   
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
