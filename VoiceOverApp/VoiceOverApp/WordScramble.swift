//
//  WordScramble.swift
//  VoiceOverApp
//
//  Created by guozw2 on 2023/5/22.
//

import SwiftUI

struct WordScramble: View {
    @State private var newWord = ""
    @State private var useWords = [String]()
    var body: some View {
        List {
            Section {
                TextField("Enter your word", text: $newWord)
            }
            
            Section {
                ForEach(useWords, id:\.self) { word in
                    HStack {
                        Image(systemName: "\(word.count).circle")
                        Text(word)
                    }
                    .accessibilityElement(children: .ignore)
                    .accessibilityLabel(word)
                    .accessibilityHint("\(word.count) letters")
                }
            }
        }
        .navigationTitle("rootWord")
        .onSubmit {
            
        }
        .onAppear(perform: startGame)
        
    }
    
    func startGame() -> Void {
        
    }
}

struct WordScramble_Previews: PreviewProvider {
    static var previews: some View {
        WordScramble()
    }
}

/// 这是一个在任何类型的整数和之间添加的扩展Double，无论整数是在左边还是右
extension BinaryInteger {
    static func *(lhs: Self, rhs: Double) -> Double {
        return Double(lhs) * rhs
    }

    static func *(lhs: Double, rhs: Self) -> Double {
        return lhs * Double(rhs)
    }
}
