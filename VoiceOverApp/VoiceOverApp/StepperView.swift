//
//  StepperView.swift
//  VoiceOverApp
//
//  Created by guozw2 on 2023/5/22.
//

import SwiftUI

struct StepperView: View {
    @State private var value = 10
    
    var body: some View {
        VStack {
            Text("Value: \(value)")
            
            Button("Increment") {
                value += 1
            }
            
            Button("Decrement") {
                value -= 1
            }
            
        }
        .accessibilityElement()
        .accessibilityLabel("Value")
        .accessibilityValue("\(value)")
        .accessibilityAdjustableAction { direction in
            if direction == .decrement {
                value += 1
            } else if direction == .increment {
                value -= 1
            }
        }
    }
}

struct StepperView_Previews: PreviewProvider {
    static var previews: some View {
        StepperView()
    }
}
