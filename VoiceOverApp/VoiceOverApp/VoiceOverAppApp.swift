//
//  VoiceOverAppApp.swift
//  VoiceOverApp
//
//  Created by guozw2 on 2023/5/22.
//

import SwiftUI

@main
struct VoiceOverAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
