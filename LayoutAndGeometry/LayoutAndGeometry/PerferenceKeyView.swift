//
//  PerferenceKeyView.swift
//  LayoutAndGeometry
//
//  Created by guozw2 on 2023/11/23.
//

import SwiftUI

struct UsingCustomScrollView: View {
    @State private var pageIndex = 0
    @State private var offset: CGFloat = 0

    private let data = [
        "Information for card Zero",
        "Information for card One",
        "Information for card Two",
        "Information for card Three",
        "Information for card Four",
        "Information for card Five",
        "Information for card Six"
    ]

    var body: some View {
        let width: CGFloat = 300
        let height: CGFloat = 200
        VStack(spacing: 20) {
            ScrollViewWithOffset(.horizontal, showsIndicators: true) { offset in
                let offsetX = Int(round((offset.x / width)))
                pageIndex =  abs(offsetX)
                self.offset = offset.x
            } content: {
                LazyHStack {
                    ForEach(data.indices, id: \.self) { index in
                        Image(systemName: "\(index).circle")
                            .font(.largeTitle)
                            .frame(width: width, height: height)
                            .background(Color.red.gradient)
                            .cornerRadius(10)
                    }
                }
            }
            .frame(width: .infinity, height: height)

            Text("\(pageIndex)")
            Text("\(offset)")

            if pageIndex < data.count {
                Text(data[pageIndex])
            }
        }
        .padding(.horizontal)
    }
}

enum ScrollOffsetNamespace {
    static let namespace = "scrollView"
}

struct ScrollOffsetPreferenceKey: PreferenceKey {
    static var defaultValue: CGPoint = .zero
    static func reduce(value: inout CGPoint, nextValue: () -> CGPoint) {}
}

struct ScrollViewOffsetTracker: View {
    var body: some View {
        GeometryReader { geo in
            Color.clear
                .preference(
                    key: ScrollOffsetPreferenceKey.self,
                    value: geo.frame(in: .named(ScrollOffsetNamespace.namespace)).origin
                )
        }
        .frame(height: 0)
    }
}

private extension ScrollView {
    func withOffsetTracking(action: @escaping (_ offset: CGPoint) -> Void) -> some View {
        self.coordinateSpace(name: ScrollOffsetNamespace.namespace)
            .onPreferenceChange(ScrollOffsetPreferenceKey.self, perform: action)
    }
}

public struct ScrollViewWithOffset<Content: View>: View {

    public init(
        _ axes: Axis.Set = .vertical,
        showsIndicators: Bool = true,
        onScroll: ScrollAction? = nil,
        @ViewBuilder content: @escaping () -> Content
    ) {
        self.axes = axes
        self.showsIndicators = showsIndicators
        self.onScroll = onScroll ?? { _ in }
        self.content = content
    }

    private let axes: Axis.Set
    private let showsIndicators: Bool
    private let onScroll: ScrollAction
    private let content: () -> Content

    public typealias ScrollAction = (_ offset: CGPoint) -> Void

    public var body: some View {
        ScrollView(axes, showsIndicators: showsIndicators) {
            ZStack(alignment: .top) {
                ScrollViewOffsetTracker()
                content()
            }
        }
        .withOffsetTracking(action: onScroll)
    }
}
