//
//  fixedSizeView.swift
//  LayoutAndGeometry
//
//  Created by guozw2 on 2023/12/22.
//

import SwiftUI

struct fixedSizeView: View {
    @State var horizontalFlag = false
    var body: some View {
        Text("<stfid>0662e1a5-36cd-4bab-8133-5f64521dc1c9</stfid><staffno>47777</staffno><fullname>Lee Yuk Wah</fullname>")
            .border(Color(.blue))
            .fixedSize(horizontal: horizontalFlag, vertical: false)
            .frame(width: 300, height: 100)
            .border(Color(.green))
            .font(.title)
        
        Button {
            withAnimation {
                horizontalFlag.toggle()
            }
        } label: {
            Text("Animation Value -> \(String(horizontalFlag))")
        }

    }
}

struct LittleSquares: View {
    let total: Int
    let itemWidth: CGFloat = 30
    var maxWidth: CGFloat {
        get {
            CGFloat(itemWidth * CGFloat(total) + ((itemWidth - 1) > 0 ? itemWidth - 1 : 1) * 5)
        }
    }
  
    var body: some View {
        GeometryReader { proxy in
            HStack(spacing: 5) {
//            GridRow(alignment: .center) {
                ForEach(0..<self.maxSquares(proxy), id: \.self) { row in
                    RoundedRectangle(cornerRadius: 5).frame(width: self.itemWidth, height: self.itemWidth)
                        .foregroundColor(self.allFit(proxy) ? .green : .red)
                }
            }
        }
        .frame(idealWidth: (5 + self.itemWidth) * CGFloat(self.total), maxWidth: (5 + self.itemWidth) * CGFloat(self.total))
    }
    
    func maxSquares(_ proxy: GeometryProxy) -> Int {
        let count =  min(Int(proxy.size.width / (itemWidth + 5)), total)
        print("\(count)")
        return count
    }
    
    func allFit(_ proxy: GeometryProxy) -> Bool {
        return maxSquares(proxy) == total
    }
}

struct SquareView: View {
    @State var idealWidth = 150.0
    @State private var fixedWidthFlag = false

    var body: some View {
        GeometryReader { proxy in
            VStack {
                VStack {
                    LittleSquares(total: 7)
                        .border(Color.black)
                        .fixedSize(horizontal: self.fixedWidthFlag, vertical: false)
                }
                .frame(width: self.idealWidth)
                .border(Color.primary)
                
                Form {
                    Slider(value: $idealWidth, in: 1...300) {
                        Text("Width")
                    } minimumValueLabel: {
                        Text("1")
                    } maximumValueLabel: {
                        Text("300")
                    } onEditingChanged: { isEditing in
                        
                    }
                    
                    Toggle(isOn: $fixedWidthFlag, label: {
                        Text("FixedWidth")
                    })
                }
            }.padding(.top, 140)
        }
    }
}

#Preview {
//    fixedSizeView()
    SquareView()
}
