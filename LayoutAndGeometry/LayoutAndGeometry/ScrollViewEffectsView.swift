//
//  ScrollViewEffectsView.swift
//  LayoutAndGeometry
//
//  Created by guozw2 on 2023/8/14.
//  挑战完成，动态改变颜色和改变大小

import SwiftUI

struct ScrollViewEffectsView: View {
    
    let colors: [Color] = [.red, .green, .blue, .orange, .pink, .purple, .yellow]
    /**
     当我们使用 GeometryProxy.frame(in: ) 方法，SwiftUI 讲计算视图崽我们要求的坐标空间中的当前位置。然后随着试视图的移动，这些值会改变，SwiftUI 将自动确保 GeometryReader 会更新。
     DragGesture 将宽度和高度存储为 @State，因为它允许我们根据拖动调整其他属性以创建简洁的效果。我们看可以动态从视图环境中获取值，将其绝对或相对位置输入到各种修饰符中。更好的是，如果需要，您可以嵌套几何图形读取器。
     */
    
    let maxColorHue = 0.9
    var body: some View {
        GeometryReader { fullView in
            ScrollView {
                ForEach(0..<50) { index in
                    GeometryReader { geo in
                        Text("Row #\(index)")
                            .font(.title)
                            .frame(maxWidth: .infinity)
                            .background(Color(hue: min(geo.frame(in: .global).midY / fullView.size.height, maxColorHue), saturation: 1, brightness: 1))
//                            .background(colors[index % 7])
                            .rotation3DEffect(.degrees(geo.frame(in: .global).minY - fullView.size.height / 2) / 5, axis: (x: 0, y: 1, z: 0))
                            .opacity(geo.frame(in: .global).minY > 200 ? 1:0.5)
                            .scaleEffect(CGSize(width: max(0.5, geo.frame(in: .global).midY/fullView.size.height), height: max(fullView.size.height*0.5, geo.frame(in: .global).minY)/fullView.size.height))
                            
                    }
                    .frame(height: 40)
                }
            }
        }
    }
    
//    var body: some View {
//        ScrollView(.horizontal, showsIndicators: false) {
//            HStack(spacing: 0) {
//                ForEach(1..<20) { num in
//                    GeometryReader { geo in
//                        Text("Number \(num) + \(geo.frame(in: .global).debugDescription)")
//                            .font(.largeTitle)
//                            .padding()
//                            .background(.red)
//                            .rotation3DEffect(.degrees(-geo.frame(in: .global).minX) / 10, axis: (x: 0, y: 0.1, z: 0))  /// 根据坐标对每个滑动的cell 进行缩放
//                            .frame(width: 200, height: .infinity)
//                    }
//                    .frame(width: 200, height: .infinity)
//                }
//            }
//        }
//    }
}

struct ScrollViewEffectsView_Previews: PreviewProvider {
    static var previews: some View {
        ScrollViewEffectsView()
    }
}
