//
//  AlignmentView.swift
//  LayoutAndGeometry
//
//  Created by guozw2 on 2023/6/26.
//

import SwiftUI

struct AlignmentView: View {
    var body: some View {
        // 修饰符的优先级可以查看Inspector 里面，从底到上是Background,Frame,Padding,Font,Accessibility,Text
        // 询问是从闭包外向内，响应是从内向外
        Text("Hello, World!")
            .padding(20) // 先询问填充
            .background(.red)
        
        Text("frame 300*300, but Text is ")
            .padding()
            .frame(width: 300, height: 300, alignment: .topLeading)
            .background(.green)
        
        HStack(alignment: .firstTextBaseline) {
            Text("Live")
                .font(.caption)
            Text("long")
            Text("and")
                .font(.title)
            Text("prosper")
                .font(.largeTitle)
        }
        
        // 红色背景
//            VStack(alignment: .leading) {
//                        Text("Hello, world!")
//                    .alignmentGuide(.leading, computeValue: { d in
//                        // ViewDimensions 对象其中包含视图的宽度和高度
//                        d[.leading]})
//                        Text("This is a longer line of text")
//                    }
//                    .background(.red)
//                    .frame(width: 300, height: 100)
//                    .background(.blue)
        
        
        VStack(alignment: .leading) {
            // 硬编码对齐的位置
                ForEach(0..<10) { position in
                    Text("Number \(position)")
                        .alignmentGuide(.leading) { _ in CGFloat(position) * -10 }
                }
            }
            .background(.red)
            .frame(width: 400, height: 100)
            .background(.blue)
        
        // 为了完全控制对齐参考线，您需要创建自定义对齐参考线
    }
}

struct AlignmentView_Previews: PreviewProvider {
    static var previews: some View {
        AlignmentView()
    }
}
