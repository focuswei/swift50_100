//
//  ExtenModifier.swift
//  LayoutAndGeometry
//
//  Created by guozw2 on 2023/12/6.
//  extension 的作用

import SwiftUI

struct ExtenModifier: View {
    
    @State private var tapped = false
    
    @State private var littleRect: CGRect = .zero
    @State private var bigRect: CGRect = .zero
    
    var body: some View {
        VStack {
            Text("Hello World!").font(.largeTitle)
                .modifier(SelectOnTap(color: .purple, width: 4))
            
            Circle().frame(width: 50, height: 50)
                .modifier(SelectOnTap(color: .green))
            
            LinearGradient(gradient: Gradient(colors: [Color.green, Color.blue]), startPoint: UnitPoint(x: 0, y: 0), endPoint: UnitPoint(x: 1, y: 1))
                .frame(width: 50, height: 50)
                .modifier(SelectOnTap(color: .blue, width: 1))
        }
        
//        VStack {
//            Text("Little = \(littleRect.debugDescription)")
//            Text("Big = \(bigRect.debugDescription)")
//            
//            HStack {
//                LitlleView()
//            }
//            .coordinateSpace(name: "mySpace")
//            .retrieveBounds(viewId: 1, $littleRect)
//            .retrieveBounds(viewId: 2, $bigRect)
//        }
        
        
//        VStack(alignment: .center) {
//
//            Spacer()
//
//            Text("Hello World").conditionalModifier(tapped, PlainModifier(), CrazyModifier())
//
//
//            Spacer()
//
//            // This line alternates between a modifier, or none at all
//            Text("Hello World").conditionalModifier(tapped, PlainModifier())
//
//            Spacer()
//            Button("Tap Me!") { self.tapped.toggle() }.padding(.bottom, 40)
//        }
        
        
    }
}

#Preview {
    ExtenModifier()
}

struct LitlleView: View {
    var body: some View {
        Text("Little Text").font(.caption).padding(20).saveBounds(viewId: 1, coordinateSpace: .named("mySpace"))
    }
}

struct BigView: View {
    var body: some View {
        Text("Big Text").font(.largeTitle).padding(20).saveBounds(viewId: 2, coordinateSpace: .named("mySpace"))
    }
}

struct PlainModifier: ViewModifier {
    func body(content: Content) -> some View {
        return content
            .font(.largeTitle)
            .foregroundColor(.blue)
            .autocapitalization(.allCharacters)
    }
}

struct CrazyModifier: ViewModifier {
    var font: Font = .largeTitle
    func body(content: Content) -> some View {
        return content
            .font(.largeTitle)
            .foregroundColor(.red)
            .autocapitalization(.words)
            .font(Font.custom("Papyrus", size: 24))
            .padding()
            .overlay {
                RoundedRectangle(cornerRadius: 10).stroke(Color.purple, lineWidth: 3.0)
            }
    }
}

struct MyRoundedBorder<S>: ViewModifier where S : ShapeStyle {
    let shapeStyle: S
    var width: CGFloat = 1
    let cornerRadius: CGFloat
    
    init(shapeStyle: S, width: CGFloat, cornerRadius: CGFloat) {
        self.shapeStyle = shapeStyle
        self.width = width
        self.cornerRadius = cornerRadius
    }
    
    func body(content: Content) -> some View {
        return content.overlay(RoundedRectangle(cornerRadius: cornerRadius).strokeBorder(shapeStyle, lineWidth: width))
    }
}

struct SelectOnTap: ViewModifier {
    let color: Color
    var width: CGFloat = 3
    @State private var tapped: Bool = false
    
    func body(content: Content) -> some View {
        return content
            .padding(20)
            .overlay {
                RoundedRectangle(cornerRadius: 10).stroke(tapped ? color: Color.red, lineWidth: width)
            }
            .onTapGesture {
                withAnimation(.easeInOut(duration: 0.3)) {
                    self.tapped.toggle()
                }
            }
    }
}

// MARK: Extension

extension View {
    public func addBorder<S>(_ content: S, width: CGFloat = 1, cornerRadius: CGFloat) -> some View where S : ShapeStyle {
        return overlay(RoundedRectangle(cornerRadius: cornerRadius).strokeBorder(content, lineWidth: width))
    }
}

extension Image {
    init(_ name: String, defaultImage: String) {
        if let img = UIImage(named: name) {
            self.init(uiImage: img)
        } else {
            self.init(defaultImage)
        }
    }
    
    init(_ name: String, defaultSystemImage: String) {
        if let img = UIImage(named: name) {
            self.init(uiImage: img)
        } else {
            self.init(systemName: defaultSystemImage)
        }
    }
}

extension View {
    // If condition is met, apply modifier, otherwise, leave the view untouched
    public func conditionalModifier<T>(_ condition: Bool, _ modifier: T) -> some View where T: ViewModifier {
        Group {
            if condition {
                self.modifier(modifier)
            } else {
                self
            }
        }
    }

    // Apply trueModifier if condition is met, or falseModifier if not.
    public func conditionalModifier<M1, M2>(_ condition: Bool, _ trueModifier: M1, _ falseModifier: M2) -> some View where M1: ViewModifier, M2: ViewModifier {
        Group {
            if condition {
                self.modifier(trueModifier)
            } else {
                self.modifier(falseModifier)
            }
        }
    }
}


extension View {
    // 设置 viewId save bounds
    public func saveBounds(viewId: Int, coordinateSpace: CoordinateSpace = .global) -> some View {
        background( GeometryReader(content: { geometry in
            Color.clear.preference(key: SaveBoundsPrefKey.self, value: [SaveBoundsPrefData(viewId: viewId, bounds: geometry.frame(in: coordinateSpace))])
        }))
    }
    
    // 通过viewId 获取bounds，并且绑定到 @State
    public func retrieveBounds(viewId: Int, _ rect: Binding<CGRect>) -> some View {
        onPreferenceChange(SaveBoundsPrefKey.self, perform: { preferences in
            DispatchQueue.main.async {
                // The async is used to prevent a possible blocking loop,
                // due to the child and the ancestor modifying each other.
                let p = preferences.first(where: { $0.viewId == viewId })
                rect.wrappedValue = p?.bounds ?? .zero
            }
        })
    }
}

// MARK: Preference

struct SaveBoundsPrefData: Equatable {
    let viewId: Int
    let bounds: CGRect
}

struct SaveBoundsPrefKey: PreferenceKey {
    typealias Value = [SaveBoundsPrefData]
    
    static var defaultValue: [SaveBoundsPrefData] = []
    
    static func reduce(value: inout [SaveBoundsPrefData], nextValue: () -> [SaveBoundsPrefData]) {
        value.append(contentsOf: nextValue())
    }
    
    
    
}
