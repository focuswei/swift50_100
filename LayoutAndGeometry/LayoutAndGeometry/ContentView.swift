//
//  ContentView.swift
//  LayoutAndGeometry
//
//  Created by guozw2 on 2023/6/26.
//

import SwiftUI

struct ContentView: View {
    // 布局持中立态度
    var body: some View {
//        ScrollViewEffectsView()
//        UsingCustomScrollView()
        TextFieldTree()
//        VStack {
//            GeometryReader { geo in
//                Text("Hello World!")
//                    .frame(width: geo.size.width * 0.9, height: 40)
//                    .background(.red)
//            }
//            .background(.green)
//
//            Text("More Text")
//                .background(.blue)
//        }
    }
    
    func differenceBetweenMapAndCompactMap() {
        let numbers = ["1", "2", "fish", "3"]
        let evensMap = numbers.map(Int.init)
        let evensCompactMap = numbers.map(Int.init)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

