//
//  ViewDimensionsView.swift
//  LayoutAndGeometry
//
//  Created by guozw2 on 2023/12/18.
//

import SwiftUI

struct ViewDimensionsView: View {
    var body: some View {
        Text("研究ViewDimensions")
        
        Text("查看ViewDimensions[subscript]如何工作")
            .alignmentGuide(HorizontalAlignment.leading, computeValue: { d in
                return d[HorizontalAlignment.leading] + d.width / 3.0 - (d[explicit: VerticalAlignment.top] ?? 0)
            })
        
        /**
          不需要这样的写法
         d[HorizontalAlignment.leading] + d.width / 3.0 - d[explicit: VerticalAlignment.top]
         
         编译器能识别
         d[.leading] + d.width / 3.0 - d[explicit: .top]
         
         //.leading 通常为零，.center 为宽度/2.0，.trailing 为宽度。
         */
    }
}


struct ImplictitDemo: View {
    var body: some View {
        let leading = HorizontalAlignment.leading
        let center = HorizontalAlignment.center
        let trailing = HorizontalAlignment.trailing
        VStack(alignment: leading, content: {
            LabelView(title: "Athos", color: .green)
                .alignmentGuide(.leading, computeValue: { dimension in
                    30
                })
                //.alignmentGuide(HorizontalAlignment.center, computeValue: { dimension in 30})
                //.alignmentGuide(HorizontalAlignment.trailing, computeValue: { dimension in 90 }
            
            LabelView(title: "Prothos", color: .orange)
                .alignmentGuide(.leading, computeValue: { dimension in
                    90
                })
            //.alignmentGuide(HorizontalAlignment.center, computeValue: { dimension in 30})
            //.alignmentGuide(HorizontalAlignment.trailing, computeValue: { dimension in 30 }
            
            LabelView(title: "Aramis", color: .red)
            
        })
        .frame(width: 320, alignment: .trailing) // 改变容器里面的对齐线
    }
}

struct LabelView: View {
    let title: String
    let color: Color
    
    var body: some View {
        Text(title)
            .font(.title)
            .padding(10)
            .frame(width: 200, height: 40)
            .background(RoundedRectangle(cornerRadius: 8)
                .fill(LinearGradient(gradient: Gradient(colors: [color, .black]), startPoint: UnitPoint(x: 0, y: 0), endPoint: UnitPoint(x: 2, y: 1))))
    }
}

#Preview {
    Group {
        ViewDimensionsView()
        ImplictitDemo()
    }
}
