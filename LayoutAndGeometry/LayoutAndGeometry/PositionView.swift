//
//  PositionView.swift
//  LayoutAndGeometry
//
//  Created by guozw2 on 2023/6/28.
//

import SwiftUI

struct PositionView: View {
    var body: some View {
        // position 绝对位置 或者占用View 的特定一部分空间的新视图
        // offset 修改渲染内容的偏移量，Text 相对 View 默认是center,那么就是center偏移 x:100x,y:100，并不会改变视图的原始尺寸
        Text("Hello, world!")
            .offset(x: 100, y: 100)
            .background(.red)
            .frame(width: 200, height: 100)
            .background(.orange)
//            .position(x: 200, y: 100)
//            .background(.green)
        // 绝对位置 position, offset
        // 如果 background在最后就是会被 View 询问
        // View 询问 .background(.green), 其询问 background.position, 询问 .background(.orange), 询问 background(.red), 询问 Text.background, 询问 Text
        // subsequent 修饰符在offset 之后会使用原始尺寸
        /**
         Text 回答 "Hello, world!"的长度
         red.background
         */
    }
}

struct PositionView_Previews: PreviewProvider {
    static var previews: some View {
        PositionView()
    }
}
