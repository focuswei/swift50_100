//
//  ColorPreferenceKey.swift
//  LayoutAndGeometry
//
//  Created by guozw2 on 2023/11/24.
//

import SwiftUI

struct ColorPreferenceKey: PreferenceKey {
    
    typealias Value = Color?
    static var defaultValue: Value = nil
    
    static func reduce(value: inout Color?, nextValue: () -> Color?) {
        value = value ?? nextValue()
    }
}

struct ColorPreferenceKeyView: View {
    var body: some View {
        VStack {
            ColorBox(color: .red)
            ColorBox(color: .green)
            ColorBox(color: .blue)
        }
        .backgroundPreferenceValue(ColorPreferenceKey.self) { value in
            Rectangle()
                .foregroundColor(value ?? .white)
        }
    }
}


struct ColorBox: View {
    let color: Color
    var body: some View {
        Rectangle()
            .frame(width: 100, height: 100)
            .foregroundColor(color)
            .preference(key: ColorPreferenceKey.self, value: color)
    }
}


struct ColorPreferenceKeyView_Previews: PreviewProvider {
    static var previews: some View {
        ColorPreferenceKeyView()
    }
}
