//
//  LayoutAndGeometryApp.swift
//  LayoutAndGeometry
//
//  Created by guozw2 on 2023/6/26.
//

import SwiftUI

@main
struct LayoutAndGeometryApp: App {
    var body: some Scene {
        WindowGroup {
            SquareView()
        }
    }
}
 
