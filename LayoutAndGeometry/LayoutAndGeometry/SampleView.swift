//
//  SampleView.swift
//  LayoutAndGeometry
//
//  Created by guozw2 on 2023/11/27.
//

import SwiftUI

struct SampleView2: View {
    @State private var activeIdx: Int = 0
    
    var body: some View {
            
            VStack {
                Spacer()
                HStack {
                    MonthView2(activeMonth: $activeIdx, label: "September", idx: 0)
                    MonthView2(activeMonth: $activeIdx, label: "October", idx: 1)
                    MonthView2(activeMonth: $activeIdx, label: "November", idx: 2)
                    MonthView2(activeMonth: $activeIdx, label: "December", idx: 3)
                }
                
                
                Spacer()
                

                HStack {
                    MonthView2(activeMonth: $activeIdx, label: "May", idx: 4)
                    MonthView2(activeMonth: $activeIdx, label: "June", idx: 5)
                    MonthView2(activeMonth: $activeIdx, label: "July", idx: 6)
                    MonthView2(activeMonth: $activeIdx, label: "August", idx: 7)
                }
                
                Spacer()

                HStack {
                    MonthView2(activeMonth: $activeIdx, label: "September", idx: 8)
                    MonthView2(activeMonth: $activeIdx, label: "October", idx: 9)
                    MonthView2(activeMonth: $activeIdx, label: "November", idx: 10)
                    MonthView2 (activeMonth: $activeIdx, label: "December", idx: 11)
                }
                Spacer()
            }
            .backgroundPreferenceValue(MyPreferenceKey2.self) { transforme in
                // anchorPreference
                GeometryReader { geo in
                    self.createBorder2(geo, transforme)
                }
            }
        
    }
    
    func createBorder2(_ geometry: GeometryProxy, _ preferences2: [MyTextPreferenceData2]) -> some View {
        let p = preferences2.first(where: { $0.viewIdx == self.activeIdx })
        
        let topLeading = p?.topLeading
        let bottomTrailing = p?.bottomTrailing
        let tl = topLeading != nil ? geometry[topLeading!] : .zero
        let bT = bottomTrailing != nil ? geometry[bottomTrailing!] : .zero
        return RoundedRectangle(cornerRadius: 15)
                        .stroke(lineWidth: 3.0)
                        .foregroundColor(Color.green)
                        .frame(width: bT.x - tl.x, height:bT.y - tl.y)
//                        .fixedSize()
                        .offset(x: tl.x, y: tl.y)
                        .animation(.easeInOut(duration: 1.0))
    }
}

struct SampleView : View {
    @State private var activeIdx: Int = 0
    
//    @State private var rects: [CGRect] = Array<CGRect>(repeating: CGRect(), count: 12) // .onPreferenceChange
    
    var body: some View {
//        ZStack(alignment: .topLeading) {
//            RoundedRectangle(cornerRadius: 15).stroke(lineWidth: 3.0).foregroundColor(Color.green)
//                .frame(width: rects[activeIdx].size.width, height: rects[activeIdx].size.height)
//                .offset(x: rects[activeIdx].minX, y: rects[activeIdx].minY)
//                .animation(.easeInOut(duration: 1.0))
            
            VStack {
                Spacer()
               
                HStack {
                    MonthView(activeMonth: $activeIdx, label: "January", idx: 0)
                    MonthView(activeMonth: $activeIdx, label: "February", idx: 1)
                    MonthView(activeMonth: $activeIdx, label: "March", idx: 2)
                    MonthView(activeMonth: $activeIdx, label: "April", idx: 3)
                }
                
                Spacer()
                
                HStack {
                    MonthView(activeMonth: $activeIdx, label: "May", idx: 4)
                    MonthView(activeMonth: $activeIdx, label: "June", idx: 5)
                    MonthView(activeMonth: $activeIdx, label: "July", idx: 6)
                    MonthView(activeMonth: $activeIdx, label: "August", idx: 7)
                }
              
                
                Spacer()
                
                HStack {
                    MonthView(activeMonth: $activeIdx, label: "September", idx: 8)
                    MonthView(activeMonth: $activeIdx, label: "October", idx: 9)
                    MonthView(activeMonth: $activeIdx, label: "November", idx: 10)
                    MonthView(activeMonth: $activeIdx, label: "December", idx: 11)
                }
                Spacer()
            }
            .backgroundPreferenceValue(MyPreferenceKey.self) { transforme in
                // anchorPreference
                GeometryReader { geo in
                    self.createBorder(geo, transforme)
                }
            }
        
//            .onPreferenceChange(MyPreferenceKey.self, perform: { value in
//                for p in value {
//                    self.rects[p.viewIdx] = p.rect
//                }
//            })
//        }
//        .coordinateSpace(name: "myZStack")  /// 使用.onPreferenceChange()替换坐标空间
        
    }
    
    // 比用.coordinateSpace 效果好，不会出现一次突兀的动画
    func createBorder(_ geometry: GeometryProxy, _ preferences: [MyTextPreferenceData]) -> some View {
        let p = preferences.first(where: { $0.viewIdx == self.activeIdx })
        let bounds = p != nil ? geometry[p!.bounds] : .zero
        
        return RoundedRectangle(cornerRadius: 15)
                        .stroke(lineWidth: 3.0)
                        .foregroundColor(Color.green)
                        .frame(width: bounds.size.width, height: bounds.size.height)
                        .offset(x: bounds.minX, y: bounds.minY)
                        .animation(.easeInOut(duration: 1.0))
    }
    
    
}



#Preview {
    SampleView()
}


struct MonthView: View {
    @Binding var activeMonth: Int
    let label: String
    let idx: Int
    
    var body: some View {
        Text(label)
            .padding(10)
            .anchorPreference(key: MyPreferenceKey.self, value: .bounds, transform: { anchor in
                // 捕获当前坐标系里，转化为适合遵循PreferenceKey的key
                // 与MonthView所在的坐标空间相关的可转换geometry-value的函数
                [MyTextPreferenceData(viewIdx: self.idx, bounds: anchor)]
            })
            .onTapGesture { self.activeMonth = self.idx }
//            .background(MyPreferenceViewSetter(idx: self.idx))  // 使用 Preference Key
//            .background(MonthBorder(show: activeMonth == idx))   使用 @Binding
        
    }
}

struct MonthView2: View {
    @Binding var activeMonth: Int
    let label: String
    let idx: Int
    
    var body: some View {
        Text(label)
            .padding(10)
            .anchorPreference(key: MyPreferenceKey2.self, value: .bottomTrailing) { anchor in
                [MyTextPreferenceData2(viewIdx: self.idx, bottomTrailing: anchor)]
            }
            .transformAnchorPreference(key:MyPreferenceKey2.self, value: .topLeading, transform: { (value, anchor) in
                value[0].topLeading = anchor
            })
            .onTapGesture { self.activeMonth = self.idx }
        
    }
}

struct MonthBorder: View {
    let show: Bool
    
    var body: some View {
        RoundedRectangle(cornerRadius: 15)
            .stroke(lineWidth: 3.0).foregroundColor(show ? Color.red : Color.clear)
            .animation(.easeInOut(duration: 0.6))
    }
}

// onPreferenceChange 要求遵循 Equatable
struct MyTextPreferenceData: Equatable {
    let viewIdx: Int
//    let rect: CGRect
    let bounds: Anchor<CGRect>
}

struct MyTextPreferenceData2: Equatable {
    let viewIdx: Int
    var topLeading: Anchor<CGPoint>? = nil
    var bottomTrailing: Anchor<CGPoint>? = nil
}

struct MyPreferenceKey2: PreferenceKey {
    static var defaultValue: [MyTextPreferenceData2] = []
    
    static func reduce(value: inout [MyTextPreferenceData2], nextValue: () -> [MyTextPreferenceData2]) {
        value.append(contentsOf: nextValue())
    }
    
    typealias Value = [MyTextPreferenceData2]
    
    
}

struct MyPreferenceKey: PreferenceKey {
    static var defaultValue: [MyTextPreferenceData] = []
    
    static func reduce(value: inout [MyTextPreferenceData], nextValue: () -> [MyTextPreferenceData]) {
        value.append(contentsOf: nextValue())
    }
    
    typealias Value = [MyTextPreferenceData]
    
    
}


struct MyPreferenceViewSetter: View {
    let idx: Int
    var body: some View {
        GeometryReader { geo in
            // 隐形的框用于获取坐标
            Rectangle()
                .fill(Color.clear)
//                .preference(key: MyPreferenceKey.self, value: [MyTextPreferenceData(viewIdx: idx, rect: geo.frame(in: .named("myZStack")))]) /// 为什么需要命名空间
        }
    }
}
