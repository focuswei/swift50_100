//
//  CustomAlignmentView.swift
//  LayoutAndGeometry
//
//  Created by guozw2 on 2023/6/28.
//

import SwiftUI

struct CustomAlignmentView: View {
    // 虽然SwiftUI 在渲染的时候会数字进行四舍五入，但存储的时候会保存浮点型
    var body: some View {
        HStack(alignment: .midAccountAndName) {
            // 解决跨Stack对齐问题，需要自定义布局指南
            VStack {
                Text("@twostraws")
                    .alignmentGuide(.midAccountAndName, computeValue: {d in d[VerticalAlignment.center]})
                Image("paul-hudson")
                    .resizable()
                    .frame(width: 64, height: 64)
            }
            
            VStack {
                Text("Full name:")
                Text("PAUL HUDSON")
                    .alignmentGuide(.midAccountAndName, computeValue: { d in d[VerticalAlignment.center] })
                    .font(.largeTitle)
            }
        }
    }
}

struct CustomAlignmentView_Previews: PreviewProvider {
    static var previews: some View {
        CustomAlignmentView()
    }
}


extension VerticalAlignment {
    
    struct MidAccountAndName: AlignmentID {
        static func defaultValue(in d: ViewDimensions) -> CGFloat {
            d[.top]
        }
    }
    
    // 自定义对齐方式更易于使用
    static let midAccountAndName = VerticalAlignment(MidAccountAndName.self)
}
