//
//  CoordinateSpaceView.swift
//  LayoutAndGeometry
//
//  Created by guozw2 on 2023/6/28.
//

import SwiftUI

struct CoordinateSpaceView: View {
    @State var location = CGPoint.zero
    // 嵌套视图如何查找并操作其封闭视图的坐标空间：
    var body: some View {
        OuterView()
            .background(.red)
            .coordinateSpace(name: "Custom") // 为视图的坐标空间分配一个名称， 以便其他代码可以相对于命名空间的维度（如点和大小）进行操作，允许另一个函数查找和操作视图以及相对于该视图的尺寸。
        
        /**
         GeometryReader { geo in
             // 读取父视图建议的布局大小
             Text("Hello World!")
                 .frame(width: geo.size.width * 0.9)
                 .background(.orange)
         }
         
         // SwiftUI 的坐标空间只有两种: Global 全屏空间，Local 局部空间
         VStack {
             // VStack 被灵活调整一分为二
             GeometryReader { geo in
                 Text("Hello World!")
                     .frame(width: geo.size.width*0.9, height: 40)
                     .background(.red)
             }
             .background(.green)
           
             Text("More text")
 //                .position(x: 100, y: 100)
                 .background(.blue)
         }
         */
        
//        VStack {
//            Color.red.frame(width: 100, height: 100)
//                .overlay(circle)
//            Text("Location: \(Int(location.x)), \(Int(location.y))")
//        }
//        .coordinateSpace(name: "stack")  //
    }

    var circle: some View {
        Circle()
            .frame(width: 100, height: 100)
//            .onTapGesture(coordinateSpace: .named("stack"), perform: { info in
//                location = info
//            })
            .gesture(drag)
            .padding(5)
    }

    var drag: some Gesture {
        DragGesture(coordinateSpace: .named("stack"))
            .onChanged { info in location = info.location }
    }
    
    func tapAction() {
        
    }
}

struct CoordinateSpaceView_Previews: PreviewProvider {
    static var previews: some View {
        CoordinateSpaceView()
    }
}


struct OuterView: View {
    var body: some View {
        VStack {
            Text("Top")
            InnerView()
                .background(.green)
            // Global 查询的是全屏幕
            // Custom 查询的是Text的中心位置跟OuterView(想自定义的视图)的midX/Y的位置的距离
            // Local 查询的是Text的Center在GeometryReader（父视图）的midX/Y位置的距离
            Text("Bottom")
        }
    }
}

struct InnerView: View {
    var body: some View {
        HStack {
            Text("Left")
            GeometryReader { geo in
                Text("Center")
                    .background(.blue)
                    .onTapGesture {
                        print("Global center: \(geo.frame(in: .global).midX) x \(geo.frame(in: .global).midY)")
                        print("Custom center:  \(geo.frame(in: .named("Custom")).minX) x \(geo.frame(in: .named("Custom")).minY)")
                        print("Local center: \(geo.frame(in: .local).midX) x \(geo.frame(in: .local).midY)")
                    }
            }
            .background(.orange)
            Text("Right")
        }
    }
}

