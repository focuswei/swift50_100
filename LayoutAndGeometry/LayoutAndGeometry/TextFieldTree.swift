//
//  TextFieldTree.swift
//  LayoutAndGeometry
//
//  Created by guozw2 on 2023/12/1.
//  迷你地图显示了按比例缩小的表格。不同的颜色将代表标题视图、文本字段和文本字段容器
//  随着标题文本视图的增长，迷你地图也会做出反应。
//  当我们添加新视图（即 Twitter 字段）时，迷你地图也会发生变化。
//  当表单中的框架发生变化时，小地图也会更新。
//  文本字段颜色为红色表示没有输入，黄色表示少于 3 个字符，绿色表示 3 个或更多字符。

import SwiftUI

enum MyViewType: Equatable {
    case formContainer // main container
    case fieldContainer // contains a text label + text field
    case field(Int) // text field (with an associated value that indicates the character count in the field)
    case title // form title
    case miniMapArea // view placed behind the minimap elements
}

struct TextFieldTree: View {
    // 由于文本字段是包含 VStack 的子级，并且我们需要两个嵌套视图的边界，因此我们不能使用anchorPreference()两次。
    @State private var fieldValues = Array<String>(repeating: "", count: 5)
    @State private var length: Float = 360
    @State private var twitterFieldPresent = false
    
    var body: some View {
        VStack {
            Spacer()
            
            HStack(alignment: .center, content: {
                Color(white: 0.7)
                    .frame(width: 200)
                    .anchorPreference(key: TextFieldPreferenceKey.self, value: .bounds, transform: { anchor in
                        [MyPreferenceData(vtype: .miniMapArea, bounds: anchor)]
                    })
                    .padding(.horizontal, 30)
                
                VStack(alignment: .leading, content: {
                    
                    VStack {
                        Text("Hello \(fieldValues[0]) \(fieldValues[1]) \(fieldValues[2])")
                            .font(.title).fontWeight(.bold)
                            .anchorPreference(key: TextFieldPreferenceKey.self, value: .bounds, transform: { anchor in
                                [MyPreferenceData(vtype: .title, bounds: anchor)]
                            })
                        Divider()
                    }
                    
                    HStack(content: {
                        Toggle(isOn: $twitterFieldPresent) { Text("") }
                            
                            
                        Slider(value: $length, in: 360...540)
                            .layoutPriority(1)
                    })
                    .padding(.bottom, 5)
                    
                    // First row of text fields
                    HStack {
                        MyFormField(fieldValue: $fieldValues[0], label: "First Name")
                        
                        MyFormField(fieldValue: $fieldValues[1], label: "Middle Name")
                        
                        MyFormField(fieldValue: $fieldValues[2], label: "Last Name")
                    }.frame(width: 540)
                    
                    HStack {
                        MyFormField(fieldValue: $fieldValues[3], label: "Email")
                        
                        if twitterFieldPresent {
                            MyFormField(fieldValue: $fieldValues[4], label: "Twitter")
                        }
                            
                    }.frame(width: CGFloat(length))
                })
                .transformAnchorPreference(key: TextFieldPreferenceKey.self, value: .bounds, transform: { (value, anchor) in
                    value.append(MyPreferenceData(vtype: .formContainer, bounds: anchor))
                })
                
                Spacer()
            })
        }
        .overlayPreferenceValue(TextFieldPreferenceKey.self) { preferences in
            GeometryReader { geo in
                MiniMap(geometry: geo, preferences: preferences)
            }
        }
    }
    
}

#Preview {
    TextFieldTree()
}

struct MyPreferenceData: Identifiable {
    let id = UUID()
    let vtype: MyViewType
    let bounds: Anchor<CGRect>
    
    func getColor() -> Color {
        switch vtype {
        case .formContainer:
            return Color.init(uiColor: UIColor.lightText)
        case .fieldContainer:
            return Color.init(uiColor: UIColor.brown)
        case .field(let int):
            return int > 0 ? (int > 3 ? Color.green:Color.yellow):Color.red
        case .title:
            return Color.purple
        case .miniMapArea:
            return Color.gray
        }
    }
    
    
    /// true 就会显示在 小地图
    // Only fields, field containers and the title are shown in the minimap
    func show() -> Bool {
        switch vtype {
        case .field:
            return true
        case .title:
            return true
        case .fieldContainer:
            return true
        default:
            return false
        }
    }
}

struct TextFieldPreferenceKey: PreferenceKey {
    static var defaultValue: [MyPreferenceData] = []
    
    static func reduce(value: inout [MyPreferenceData], nextValue: () -> [MyPreferenceData]) {
        value.append(contentsOf: nextValue())
    }
    
    typealias Value = [MyPreferenceData]
    
    
}


struct MyFormField: View {
    @Binding var fieldValue: String
    let label: String
    var body: some View {
        VStack(alignment: .leading, content: {
            Text(label)
            TextField("", text: $fieldValue)
                .textFieldStyle(.roundedBorder)
                .anchorPreference(key: TextFieldPreferenceKey.self, value: .bounds, transform: { anchor in
                    return [MyPreferenceData(vtype: .field(self.fieldValue.count), bounds: anchor)]
                })
        })
        .padding(10)
        .background(RoundedRectangle(cornerRadius: 15).fill(Color(white: 0.9)))
        .transformAnchorPreference(key: TextFieldPreferenceKey.self, value: .bounds) { (value, anchor) in
            // 修改.anchorPreference 中 bounds 的值
            value.append(MyPreferenceData(vtype: .fieldContainer, bounds: anchor))
        }
    }
}


struct MiniMap: View {
    let geometry: GeometryProxy
    let preferences: [MyPreferenceData]
    var body: some View {
        
        guard let formContainerAnchor = preferences.first(where: { $0.vtype == .formContainer })?.bounds else { return AnyView(EmptyView()) }
        
        guard let miniMapAreaAnchor = preferences.first(where: { $0.vtype == .miniMapArea })?.bounds else {  return AnyView(EmptyView()) }
        
        // 屏幕剩余宽度比例
        let factor = geometry[formContainerAnchor].size.width / (geometry[miniMapAreaAnchor].size.width - 10.0)
        
        let containerPosition = CGPoint(x: geometry[formContainerAnchor].minX, y: geometry[formContainerAnchor].minY)
        
        let miniMapPosition = CGPoint(x: geometry[miniMapAreaAnchor].minX, y: geometry[miniMapAreaAnchor].minY)
        
        return AnyView(miniMapView(factor, containerPosition, miniMapPosition))
    }
    
    func miniMapView(_ factor: CGFloat, _ containerPosition: CGPoint, _ miniMapPosition: CGPoint) -> some View {
        ZStack(alignment: .topLeading) {
            // Create a small representation of each of the form's views.
            // Preferences are traversed in reverse order, otherwise the branch views
            // would be covered by their ancestors
            ForEach(preferences.reversed()) { pref in
                if pref.show() { // some type of views, we don't want to show
                    self.rectangleView(pref, factor, containerPosition, miniMapPosition)
                }
            }
        }.padding(5)
    }
        
        func rectangleView(_ pref: MyPreferenceData, _ factor: CGFloat, _ containerPosition: CGPoint, _ miniMapPosition: CGPoint) -> some View {
            Rectangle()
            .fill(pref.getColor())
            .frame(width: self.geometry[pref.bounds].size.width / factor,
                   height: self.geometry[pref.bounds].size.height / factor)
            .offset(x: (self.geometry[pref.bounds].minX - containerPosition.x) / factor + miniMapPosition.x,
                    y: (self.geometry[pref.bounds].minY - containerPosition.y) / factor + miniMapPosition.y)
        }
}
