//
//  ContentView.swift
//  CupcakeCorner
//
//  Created by guozw3 on 2022/3/30.
//

import SwiftUI

struct ContentView: View {
    /**
     @State  值类型或者枚举
     @StateObject  Reference types
     @EnvironmentObject 针对需要传递深层次的子 View中的对象（ObservableObject），可以用.environmentObject修饰器来将其注入环境
     使其任意子View都可以通过 @EnvironmentObject 来获取对应的对象。
     */
    
//    @StateObject var order: Order = Order() ///< 只会被创建一次，而不会因为 ContentView的body 更新而重复初始化
    @State var order: Order = Order()
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    Picker("Select your cake type", selection: $order.type) {
                        ForEach(Order.types.indices) {
                            Text(Order.types[$0])
                        }
                    }
                    
                    Stepper("Number of cakes: \(order.quantity)", value: $order.quantity, in: 3...20)
                }
                
                Section {
                    Toggle("Any special request?", isOn: $order.specialRequestEnabled)
                    
                    if order.specialRequestEnabled {
                        Toggle("Add extra frosting", isOn: $order.extraFrosting)
                        Toggle("Add extra sprinkles", isOn: $order.addSprinkles)
                    }
                }
                .accessibilityHidden(true)
                
                Section {
                    NavigationLink {
                        AddressView(order: order)
                    } label: {
                        Text("Delivery details")
                    }
                }
            }
            .navigationTitle("Cupcake Corner")
        }
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
