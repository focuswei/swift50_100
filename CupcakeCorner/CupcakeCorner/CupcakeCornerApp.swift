//
//  CupcakeCornerApp.swift
//  CupcakeCorner
//
//  Created by guozw3 on 2022/3/30.
//

import SwiftUI

@main
struct CupcakeCornerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
