//
//  AsyncLoad.swift
//  CupcakeCorner
//
//  Created by guozw3 on 2022/3/31.
//

import SwiftUI

struct AsyncLoad: View {
    @State private var results = [ResultItem]()
    
    var body: some View {
        // placeholder 的闭包可以设置占位图
        AsyncImage(url: URL(string: "https://hws.dev/img/logo.png")) { phase in
            if let image = phase.image {
                image
                    .resizable()
                    .scaledToFit()
            } else if phase.error != nil {
                Text("There was an error loading the image.")
            } else {
                ProgressView()
            }
        }
        .frame(width: 200, height: 200)
        
        
        List(results, id: \.trackId) { item in
            VStack(alignment: .leading, spacing: 0) {
                Text(item.trackName)
                    .font(.headline)
                Text(item.collectionName)
            }
        }
        .task {
            // task 修饰符可以调用异步函数， onAppear不可以，原因是后者是在View的生命周期中执行
            await loadData()
        }
    }
    
    /// 网络请求满足两个关键字： async 和 await
    func loadData() async {
        let session = URLSession.shared
        guard let url = URL.init(string: "https://itunes.apple.com/search?term=taylor+swift&entity=song") else { return }
        session.dataTask(with: URLRequest.init(url: url)) { data, response, error in
            if data != nil {
                if let decodedResponse = try? JSONDecoder().decode(Response.self, from: data!) {
                    results = decodedResponse.results
                }
            }
        }.resume()
    }
}

//struct AsyncLoad_Previews: PreviewProvider {
//    static var previews: some View {
//        AsyncLoad()
//    }
//}
