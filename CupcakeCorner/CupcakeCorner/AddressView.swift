//
//  AddressView.swift
//  CupcakeCorner
//
//  Created by guozw3 on 2022/3/31.
//

import SwiftUI

struct AddressView: View {
//    @ObservedObject var order: Order
    @State var order: Order

    var body: some View {
        Form {
            Section {
                TextField("Name", text: $order.name)
                TextField("Street Address", text: $order.streetAddress)
                TextField("City", text: $order.city)
                TextField("Zip", text: $order.zip)
            }
        }
        // 在屏幕底下
        Section {
            NavigationLink {
                CheckoutView(order: order)
            } label: {
                Text("Check out")
            }
        }.disabled(order.hasValidAddress == false)
        .navigationTitle("Delivery details")
        .navigationBarTitleDisplayMode(.automatic)
    }
}

struct CheckoutView: View {
//    @ObservedObject var order: Order
    @State var order: Order
    @State private var confirmationMessage = ""
    @State private var showingConfirmation = false
    @State private var showNetworkFailure = false // 通知网络失败

    var body: some View {
        ScrollView {
            VStack {
                AsyncImage(url: URL.init(string: "https://hws.dev/img/cupcakes@3x.jpg"), scale: 3) { image in
                    image.resizable().scaledToFit()
                } placeholder: {
                    ProgressView()
                }
                .frame(height: 233)
                
                Text("Your total is \(order.cost , format: .currency(code: "USD"))")
                    .font(.title)
                
                Button("Place Order", action: {
                    Task {
                        await placeOrder()
                    }
                })
                .padding()
            }
            .alert("Thank you!", isPresented: $showingConfirmation) {
                Button("OK") { }
            } message: {
                Text(confirmationMessage)
            }
            
        }
        .navigationTitle("Check out")
        .navigationBarTitleDisplayMode(.inline)
    }
    
    func placeOrder() async {
        // 从归档中读取Upload Data
        guard let encoded = try? JSONEncoder().encode(order) else {
            print("Failed to encode order")
            return
        }
        
        guard let url = URL(string: "https://reqres.in/api/cupcakes") else {
            fatalError()
        }
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        // 尝试检查错误返回
        request.httpMethod = "POST"
        
        do {
            let (data, _) = try await URLSession.shared.upload(for: request, from: encoded, delegate: nil)
            let order = try JSONDecoder.init().decode(Order.self, from: data)
            confirmationMessage = "Your order for \(order.quantity)x \(Order.types[order.type].lowercased()) cupcakes is on its way!"
            showingConfirmation = true
        } catch let error {
            print(error)
            showNetworkFailure = true
        }
    }
}

//struct AddressView_Previews: PreviewProvider {
//    static var previews: some View {
//        AddressView(order: Order.init())
//    }
//}
