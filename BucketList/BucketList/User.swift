//
//  User.swift
//  BucketList
//
//  Created by guozw2 on 2023/5/16.
//

import Foundation

struct User: Identifiable, Comparable {
    static func < (lhs: User, rhs: User) -> Bool {
        return lhs.firstName < rhs.firstName
    }
    
    let id = UUID()
    let firstName: String
    let lastName: String
    
    
}
