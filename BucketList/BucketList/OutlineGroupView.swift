//
//  OutlineGroupView.swift
//  BucketList
//
//  Created by guozw2 on 2023/12/13.
//

import SwiftUI

struct FileItem: Identifiable, Hashable, CustomStringConvertible {
    var id: Self { self }
    var paddingIndex: Int {
        mutating get {
            if hasParent {
                depth += 1
                return self.depth
            } else {
                return 1
            }
        }
    }
    private(set) var depth: Int = 1
    var hasParent: Bool = true
    var name: String
    var children: [FileItem]? = nil
    var description: String {
        switch children {
        case nil:
            return "📄 \(name)"
        case .some(let children):
            return children.isEmpty ? "📂 \(name)" : "📁 \(name)"
        }
    }
}

let data =
FileItem(hasParent: false, name: "users", children:
    [FileItem(name: "user1234", children:
      [FileItem(name: "Photos", children:
        [FileItem(name: "photo001.jpg"),
         FileItem(name: "photo002.jpg")]),
       FileItem(name: "Movies", children:
         [FileItem(name: "movie001.mp4")]),
          FileItem(name: "Documents", children: [])
      ]),
     FileItem(name: "newuser", children:
       [FileItem(name: "Documents", children: [])
       ])
    ])




struct OutlineGroupView: View {
    var body: some View {
        OutlineGroup(data, children: \.children) { item in
            Text("\(item.description)")
                .padding(.leading, item.hasParent ? 20:10)
        }
    }
}

#Preview {
    OutlineGroupView()
}
