//
//  EditView.swift
//  BucketList
//
//  Created by guozw2 on 2023/5/18.
//  大头针细节视图 功能是 允许修改title和description，查询维基百科的内容, day 70 part three，day 71 part four

import SwiftUI

struct EditView: View {
    @Environment(\.dismiss) var dismiss
    @StateObject private var vm: EditViewViewModel
    
    // 请求状态
//    @State private var loadingState = LoadingState.loading
    // 提供查询维基百科的信息方便参考，修改标注地图
//    @State private var pages = [Page]()
    
//    var onSave: ((Locale) -> Void)?
    // 接收一个要编辑的内容, 目的是一个真实值
//     var location: Locale
    // 错误教材：我们虽然可以用像 @Binding 来传入依赖值, 但是会产生一个问题，就是可选项值在 ContentView.sheet(item, dimiss:)这个修饰符中只传递真实值，我们修改成 可选项 Locale? 但是没办法修改 UI 的原因是 Locale 重载了 == 方法，UUID 没有改动就不更新数据了, 但是还是需要 Closure 传递修改，swiftUI吧不建议在within view updates
//    @Binding var bindingLocation : Locale?

    // private 属性不能注入 State，只能在 init{} 中初始化
//    @State private var name: String
//    @State private var description: String
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    // name.projectedValue -> warning: Accessing State's value outside of being installed on a View. This will result in a constant Binding of the initial value and will not update.
                    TextField("Place name", text: $vm.name)
                    TextField("Description", text: $vm.description)
                }
                Section("Nearby…") {
                    switch vm.loadingState {
                    case .success:
                        ForEach(vm.pages, id: \.pageid) { page in
                            Text(page.title)
                                .font(.headline)
                            + Text(": ") +
                            Text(page.description)
                                .italic()
                        }
                    case .loading:
                        Text("Loading…")
                    case .failed:
                        Text("Please try again later.")
                    }
                }
            }
            .navigationTitle("Place detail")
            .toolbar {
                Button("Save") {
                    /**
                     //  修改@Binding 的传值是不能用的 reason: Publishing changes from within view updates is not allowed, this will cause undefined behavior.
 //                    bindingLocation?.name = name
 //                    bindingLocation?.description = description
                     */
                    vm.save()
                    dismiss()
                }.task {
                    await vm.fetchNearbyPlaces()
                }
            }
        }
    }
    
    // 解决方法一：初始化, 修改后的属性通过 closure 传递
    init(location: Locale, doneClosure: @escaping ((Locale) -> Void)) {
        _vm = StateObject<EditView.EditViewViewModel>.init(wrappedValue: EditViewViewModel(location: location, doneClosure: doneClosure))
//        self.location = location
//        self.onSave = onSaveClosure
//        _name = State(initialValue: location.name)
//        _description = State(initialValue: location.description)
    }
    
    // 错误的教材, 虽然可以修改，但是swiftUI 不建议
//    init(location: Binding<Locale?>, onSaveClosure: @escaping (Locale) -> Void) {
//        _bindingLocation = location
//        self.onSave = onSaveClosure
//        _name = State(initialValue: location.wrappedValue?.name  ?? "")
//        _description = State(initialValue: location.wrappedValue?.description ?? "")
//
//    }
    
    
}

struct EditView_Previews: PreviewProvider {
    static var location: Locale = Locale(id: UUID.init(), name: "test", description: "annotation", latitude: 12.5, longitude: 44.2)
    static var previews: some View {
//        EditView(location: .constant(location)) { _ in }
        EditView(location: Locale.example, doneClosure: { _ in })
    }
}


extension EditView {
    /// 挑战，
    @MainActor class EditViewViewModel: ObservableObject {
//        var dismiss: EnvironmentValues
        @Published private(set) var pages = [Page]()
        
        @Published var loadingState = LoadingState.loading
        @Published var name: String
        @Published var description: String
        var location: Locale
        var doneComlete: ((Locale) -> Void)?
        init(location: Locale, doneClosure: @escaping ((Locale) -> Void)) {
            self.location = location

            name = location.name
            description = location.description
            doneComlete = doneClosure
//            dismiss =
        }
        
        func update(pages: [Page]) {
            self.pages = pages
        }
        
        func save() {
            var newLocation = location
            newLocation.id = UUID() // 因为重载 == 方法，触发 UI 更新
            newLocation.name = name
            newLocation.description = description
            // dismiss() Accessing Environment<DismissAction>'s value outside of being installed on a View. This will always read the default value and will not update.
            doneComlete?(newLocation)
        }
        
        func fetchNearbyPlaces() async {
            
            
            //        var urlComponent = URLComponents(string: "https://en.wikipedia.org/w/api.php")
            //        let ggscoord = URLQueryItem(name: "ggscoord", value: "\(location.coordinate.latitude)%7C\(location.coordinate.longitude)")
            //        let action = URLQueryItem(name: "action", value: "query")
            //        let prop = URLQueryItem(name: "prop", value: "coordinates%7Cpageimages%7Cpageterms")
            //        let colimit = URLQueryItem(name: "colimit", value: "50")
            //        let piprop = URLQueryItem(name: "piprop", value: "thumbnail")
            //        let pithumbsize = URLQueryItem(name: "pithumbsize", value: "500")
            //        let pilimit = URLQueryItem(name: "pilimit", value: "50")
            //        let wbptterms = URLQueryItem(name: "wbptterms", value: "description")
            //        let generator = URLQueryItem(name: "generator", value: "geosearch")
            //        let ggsradius = URLQueryItem(name: "ggsradius", value: "10000")
            //        let ggslimit = URLQueryItem(name: "ggslimit", value: "50")
            //        let format = URLQueryItem(name: "format", value: "json")
            //        urlComponent?.queryItems?.append(contentsOf: [ggscoord,action,prop,colimit,piprop, pithumbsize, pilimit,wbptterms,generator,ggsradius,ggslimit,format])
            //        guard let url = urlComponent?.url else { return }
            let urlString = "https://en.wikipedia.org/w/api.php?ggscoord=\(location.coordinate.latitude)%7C\(location.coordinate.longitude)&action=query&prop=coordinates%7Cpageimages%7Cpageterms&colimit=50&piprop=thumbnail&pithumbsize=500&pilimit=50&wbptterms=description&generator=geosearch&ggsradius=10000&ggslimit=50&format=json"
            
            guard let url = URL(string: urlString) else {
                print("Bad URL: \(urlString)")
                return
            }
            do {
                let (data, _) = try await URLSession.shared.data(from: url)
                
                // we got some data back!
                let items = try JSONDecoder().decode(Result.self, from: data)
                
                // success – convert the array values to our pages array
                let pages = items.query.pages.values.sorted()
                update(pages: pages)
                loadingState = .success
            } catch {
                // if we're still here it means the request failed somehow
                loadingState = .failed
            }
        }
     }
}
