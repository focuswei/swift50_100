//
//  ContentView.swift
//  BucketList
//
//  Created by guozw2 on 2023/5/16.
//

import SwiftUI
import MapKit

struct ContentView: View {
    
    @State var loadingState = LoadingState.loading
    @State var isShowingSheet = true
    // 身份验证成功后置位
    @State private var isUnlocked = false
    
    var body: some View {
        
        LocationView()
        
        
        /**
         // day 69 part two
         VStack {
             FaceIDView(isUnlocked: $isUnlocked)
             
             Button("request Biological") {
                 isUnlocked.toggle()
             }
         }
         */
        
        
        /**
         VStack {
         switch loadingState {
         case .loading:
             LoadingView()
         case .success:
             SuccessView()
         case .failed:
             FailedView()
         }
         .onTapGesture {
            isShowingSheet.toggle()
        }.confirmationDialog("select", isPresented: $isShowingSheet) {
            Button("Loading") { loadingState = .loading }
            Button("Success") { loadingState = .success }
            Button("Failure") { loadingState = .failed }
        }
         }
         */
    }
    
    
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}




/**
 let john = Contact(name: "John Appleseed", age: 24)
 cancellable = john.objectWillChange
     .sink { _ in
         print("\(john.age) will change")
 }
 print(john.haveBirthday())
 */
