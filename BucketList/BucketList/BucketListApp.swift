//
//  BucketListApp.swift
//  BucketList
//
//  Created by guozw2 on 2023/5/16.
//

import SwiftUI

@main
struct BucketListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
