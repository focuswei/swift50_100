//
//  Location.swift
//  BucketList
//
//  Created by guozw2 on 2023/5/17.
//

import Foundation
import CoreLocation

// 无论您创建什么新数据类型来存储位置，它都必须符合Identifiable协议，以便 SwiftUI 可以唯一地识别每个地图标记
struct Location: Identifiable {
    let id = UUID()
    let name: String
    let coordinate: CLLocationCoordinate2D
}


// Identifiable Map识别多个标记，Codable 加载和保存数据 Equatable 对比数据排序
struct Locale: Identifiable, Codable, Equatable {
    // Identifiable
    // associatedtype ID : Hashable , UUID 符合协议 Hashable
    var id: UUID
    var name: String
    var description: String
    let latitude: Double
    let longitude: Double
    
    // 计算属性放在 数据结构中
    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    static var example: Locale = Locale(id: UUID(), name: "Buckingham Palace", description: "Where Queen Elizabeth lives with her dorgis.", latitude: 51.501, longitude: -0.141)
    
    
    // 重载 这个方法，导致 swiftUI 更新不是根据 HashValue, 但是相当于简化，减少CPU 计算HashValue
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
    
    
}
