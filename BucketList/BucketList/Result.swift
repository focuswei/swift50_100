//
//  Result.swift
//  BucketList
//
//  Created by guozw2 on 2023/5/18.
//

import Foundation

/**
1.定义结构，把主要查询结果包含在一个 query key
2. query 内部是用 pages 作为字典，pageIndex 作为key，page作为值
3.每个 page 包含它的坐标，标题，术语。
 */

struct Result: Codable {
    var query: Query
}

struct Query: Codable {
    var pages: [Int: Page]
}

struct Page: Codable, Comparable {
    static func < (lhs: Page, rhs: Page) -> Bool {
        return lhs.title > rhs.title
    }
    
    var pageid: Int
    var title: String
    var terms: [String: [String]]?
    
    var description: String {
        terms?["description"]?.first ?? "No further information"
    }
}
