//
//  LocationView.swift
//  BucketList
//
//  Created by guozw2 on 2023/5/17.
//

import SwiftUI
import MapKit
import LocalAuthentication

struct LocationView: View {
    
    @StateObject private var viewModel = ViewModel()
    // 存储添加过的位置
    // @State private var locations = [Locale]()
    
    // 中心
//    @State private var mapRegion = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 50, longitude: 0), span: MKCoordinateSpan(latitudeDelta: 25, longitudeDelta: 25))
    
    // 提供位置信息的显示和编辑
    @State private var selectedPlace: Locale?
    @State private var didDismiss = false
    var body: some View {
        ZStack {
            if viewModel.isUnlocked {
                    // annotationItems 也可以传入 Binding<T>
                    Map(coordinateRegion: $viewModel.mapRegion, annotationItems: viewModel.locations) { location in
                        MapAnnotation(coordinate: location.coordinate) {
                            VStack {
                                Image(systemName: "star.circle")
                                    .resizable()
                                    .foregroundColor(.red)
                                    .frame(width: 44, height: 44)
                                    .background(.white)
                                    .clipShape(Circle())

                                // 自适应文本长度
                                Text(location.name)
                                    .fixedSize()
                            }
                            .onTapGesture {
                                selectedPlace = location
                            }
                        }
                    }
                    .ignoresSafeArea()
                        
                    Circle()
                        .fill(.blue)
                        .opacity(0.3)
                        .frame(width: 23, height: 23)
                    
                    VStack {
                        Spacer()
                        HStack {
                            Spacer()
                            Button {
                                // create a new location
                                viewModel.addLocation()
                            } label: {
                                Image(systemName: "plus")
                                // 修饰Button 与 修饰 Image的区别
                                .padding() //为了确保按钮在我们添加背景颜色之前变大
                                .foregroundColor(.white)
                                .background(.brown.opacity(0.7))
                                .font(.title)
                                .clipShape(Circle())
                                .padding(.trailing)  // 推向屏幕右侧
                            }
                            
                        }
                    }
                    
                    
                
            } else {
                // 提供解锁的方式
                Button {
                    viewModel.authenticate {
//                        viewModel.isUnlocked = true
                    }
                } label: {
                    Text("Unlock Places")
                        .fixedSize()
                }
                .padding()
                .background(.blue)
                .foregroundColor(Color.white)
                .clipShape(Circle())
            }
        }
        // 全屏幕模态跳转fullScreenCover
        .sheet(item: $selectedPlace, onDismiss: nil) { place in
            // 当需要显示包含来自自定义数据源内容的模态视图，输入的 Binding<Item?>，我们的详情View也要用可选项但是不建议修改
            EditView(location: place) { result in
                viewModel.update(oldLocation: place, newLocation: result)
//                    if let index = viewModel.locations.firstIndex(of: place) {
//                        // 这样写，依然不能够更新 locations 的UI，因为 Locale.id 是同一个值，所以地图认为是相同值，没有触发更新
//                        var tempArray = viewModel.locations
//                        tempArray[index] = result
//                        viewModel.updateLocations(array: tempArray)
//                    }
                
            }
           
        }
        .alert("Tips", isPresented: $viewModel.showErrorAlertFlag, actions: {
            Button("OK") { }
        }, message: {
            Text(viewModel.errorAlertMessage)
        })
    }
    
    
}

struct LocationView_Previews: PreviewProvider {
    static var previews: some View {
        LocationView()
    }
}


extension LocationView {
    /// ObservableObject
    /// MainActor 负责运行所有视图更新，使用此包装器的class代表，这个class的所有代码都由 MainActor 负责，因此这个操作可能卡UI
    @MainActor class ViewModel: ObservableObject {
        @Published var mapRegion = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 50, longitude: 0), span: MKCoordinateSpan(latitudeDelta: 25, longitudeDelta: 25))
        /// 读取数据没问题，但是写入在并发线程中容易出错，建议 private(set)
        @Published private(set) var locations: [Locale]
        @Published var selectPlace: Locale?
        
        // 解锁FaceID 的flag
        @Published var isUnlocked = false
        
        @Published var showErrorAlertFlag = false
        @Published var errorAlertMessage: String = "" {
            didSet {
                print("Q \(errorAlertMessage)")
            }
        }
        
        let savePath = FileManager.documentsDirectory.appendingPathComponent("SavedPlaces")
        
        // MARK: ** init **
        init() {
            do {
                let data = try Data(contentsOf: savePath)
                locations = try JSONDecoder().decode([Locale].self, from: data)
            } catch {
                locations = []
            }
        }
        
        // MARK: ** function **
        func addLocation() -> Void {
            let newLocation = Locale(id: UUID(), name: "New location", description: "", latitude: mapRegion.center.latitude - Double.random(in: -2.0...2.0), longitude: mapRegion.center.longitude - Double.random(in: -2.0...2.0))
            locations.append(newLocation)
            // locations 更新后 save
            dataEncode()
        }
        
        func updateLocations(array: [Locale]) {
            self.locations = array
        }
        
        func update(oldLocation: Locale, newLocation: Locale) {
            if let index = locations.firstIndex(of: oldLocation) {
                locations[index] = newLocation
            }
            dataEncode()
        }
        
        func dataEncode() {
            // flexible than UserDefault
            do {
                let data = try JSONEncoder.init().encode(locations)
                try data.write(to: savePath, options: [.atomic, .completeFileProtection])
            } catch {
                print("\(error.localizedDescription)")
            }
        }
        
        func authenticate(completion: @escaping () -> Void) {
            let context = LAContext()
            var error :NSError?
            if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                let reason = "Please authenticate yourself to unlock your places."
                
                context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, authenticationError in
                    
                    if success {
                        // 不允许从后台线程发布更改 Publishing changes from background threads is not allowed; make sure to publish values from the main thread (via operators like receive(on:)) on model updates.
                        Task { @MainActor in
                            
//                            await MainActor.run(body: {
                                self.isUnlocked = true
                                completion()
//                            })
                        }
                        
                    } else {
                        // error
                        Task { @MainActor in
                            self.showErrorAlertFlag = true
                            self.errorAlertMessage = authenticationError?.localizedDescription ?? ""
                        }
                    }
                }
                
            } else {
                self.showErrorAlertFlag = true
                self.errorAlertMessage = error?.localizedDescription ?? ""
            }
            
        }
    }
}
