//
//  FaceIDView.swift
//  BucketList
//
//  Created by guozw2 on 2023/5/17.
//

import SwiftUI
import LocalAuthentication

struct FaceIDView: View {
    @Binding var isUnlocked: Bool
    
    // 上下文，查询生物识别状态并执行身份验证检查，
    let context =  LAContext()
    // 设备允许的类型
    @State private var bioType: LABiometryType = LABiometryType.none
 
    var body: some View {
        VStack {
            if isUnlocked {
                switch bioType {
                case .faceID:
                    Text("Device Support FaceID")
                case .touchID:
                    Text("Device Support TouchID")
                case .none:
                    // iPod touch 没有生物识别
                    Text("Device has not biometry")
                default:
                    Text("Locked")
                }
            } else {
                Text("Locked")
            }
        }
        .onAppear(perform: authenticate)
        
    }
    
    func authenticate() -> Void {
        var error: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "We need to unlock your data"
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, error in
                if success {
                    
                } else {
                    
                }
            }
        } else {
            
        }
        self.bioType = context.biometryType
    }
}

struct FaceIDView_Previews: PreviewProvider {
    static var previews: some View {
        FaceIDView(isUnlocked: .constant(true))
    }
}
