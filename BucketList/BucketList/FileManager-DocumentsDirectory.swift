//
//  FileManager-DocumentsDirectory.swift
//  BucketList
//
//  Created by guozw2 on 2023/5/19.
//

import Foundation

extension FileManager {
    /// 目的在文档目录中查找文件，用 JSONEncoder 存储备用， JSONDecoder 转换。
    static var documentsDirectory: URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    
}
