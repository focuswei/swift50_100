//
//  saveStringView.swift
//  BucketList
//
//  Created by guozw2 on 2023/5/16.
//

import SwiftUI

struct saveStringView: View {
    
    let users = [
        User(firstName: "Arnold", lastName: "Rimmer"),
        User(firstName: "Kristine", lastName: "Kochanski"),
        User(firstName: "David", lastName: "Lister"),
    ].sorted()
    
    var body: some View {
        List(users) { user in
            Text("\(user.lastName), \(user.firstName)")
                .onTapGesture {
                    let str = "Test Message"
                    let urlPath = getDocumentsDirectory().appending(component: "message.txt")
                    
                    do {
                        try str.write(to: urlPath, atomically: true, encoding: .utf8)
                        let input = try String(contentsOf: urlPath)
                        print(input)
                    } catch {
                        
                    }
                }
        }
    }
    
    func getDocumentsDirectory() -> URL {
        // .documentationDirectory 没有读写权限
        // .documentDirectory 会进行iCloud备份
        // .userDomainMask 只有userDomain 有写入权限
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        // 为什么paths[0] 不报错， paths.first 报错，FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first ?? URL(fileURLWithPath: NSTemporaryDirectory()) 这种写法其实不好，违背了目的性。
        return paths[0]
    }
}

struct saveStringView_Previews: PreviewProvider {
    static var previews: some View {
        saveStringView()
    }
}
