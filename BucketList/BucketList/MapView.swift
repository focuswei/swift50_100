//
//  MapView.swift
//  BucketList
//
//  Created by guozw2 on 2023/5/17.
//

import SwiftUI
import MapKit

struct MapView: View {
    // 地图中心
    @State private var mapRegion = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 51.5, longitude: -0.12), span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
    // 地图注释，一直出现
    let locations = [
        Location(name: "Buckingham Palace", coordinate: CLLocationCoordinate2D(latitude: 51.501, longitude: -0.141)),
        Location(name: "Tower of London", coordinate: CLLocationCoordinate2D(latitude: 51.508, longitude: -0.076))
    ]
    
    var body: some View {
        NavigationView {
            Map(coordinateRegion: $mapRegion, annotationItems: locations) { location in
                // 简单大头针 MapMarker
                // MapMarker(coordinate: location.coordinate)
                MapAnnotation(coordinate: location.coordinate) {
                    // 自定义图像替换 大头针 MapAnnotation，可以添加点击手势，或者NavigationLink
                    NavigationLink {
                        Text(location.name).foregroundColor(.green)
                    } label: {
                        Circle()
                            .stroke(.red, lineWidth: 3)
                            .frame(width: 44, height: 44)
                    }
                    
                }
            }
            .navigationTitle("London Explorer")
        }
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
