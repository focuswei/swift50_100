//
//  LoadingState.swift
//  BucketList
//
//  Created by guozw2 on 2023/5/16.
//

import Foundation

enum LoadingState {
    case loading, success, failed
}
