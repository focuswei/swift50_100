//
//  EditView.swift
//  Hot Prospects
//
//  Created by guozw2 on 2023/5/30.
//

import SwiftUI

struct EditView: View {
    @EnvironmentObject var user: User
    var body: some View {
        // 找不到会崩溃
        TextField("Name", text: $user.name)
    }
}

struct DisplayView: View {
    @EnvironmentObject var user: User
    
    var body: some View {
        Text(user.name)
    }
}

struct EditView_Previews: PreviewProvider {
    @State static var sample: User = User()
    static var previews: some View {
        EditView().environmentObject(sample)
    }
}


@MainActor class DelayedUpdater: ObservableObject {
    var value = 0 {
        didSet {
            objectWillChange.send()
        }
    }
    
    init() {
        for i in 1...10 {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(i), execute: {
                self.value += 1
            })
        }
    }
}
