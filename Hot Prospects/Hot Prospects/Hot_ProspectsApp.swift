//
//  Hot_ProspectsApp.swift
//  Hot Prospects
//
//  Created by guozw2 on 2023/5/30.
//

import SwiftUI

@main
struct Hot_ProspectsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
