//
//  SwipeActionView.swift
//  Hot Prospects
//
//  Created by guozw2 on 2023/6/1.
//

import SwiftUI

struct SwipeActionView: View {
    var body: some View {
        List {
            Text("Taylor Swift")
                .swipeActions(edge: .leading, allowsFullSwipe: true) {
                    /// swipeAction 默认是 allowsFullSwipe=false，当滑动超过屏幕将会自动执行第一个Action
                    Button("Hi") {
                        print("Hi")
                    }
                    .tint(.blue) // 默认颜色是C7C7C7改变颜色为bule
                    
                    // destructive 会让Action 执行删除操作，并改变颜色为红色
                    Button(role: .cancel) {
                        print("GoodBye")
                    }    label: {
                        Text("GoodBye")
                    }
                    .clipShape(Circle())

                }
        }
        /// 添加滑动动作，ForEach 不能合成OnDelete，而是需要手动添加删除动作
    }
}

struct SwipeActionView_Previews: PreviewProvider {
    static var previews: some View {
        SwipeActionView()
    }
}
