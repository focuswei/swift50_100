//
//  Prospect.swift
//  Hot Prospects
//
//  Created by guozw2 on 2023/6/1.
//

import Foundation

class Prospect: Identifiable, Codable {
    
    // get propagated cleanly
    // get propagated cleanly
    var id = UUID()
    var name = "Anonymous"
    var emailAddress = ""
    var isContacted = false

//    var hashValue: Int {
//        return id.hashValue | name.hashValue | emailAddress.hashValue | isContacted.hashValue
//    }
//    static func == (lhs: Prospect, rhs: Prospect) -> Bool {
//        return lhs.id == rhs.id
//    }
}

@MainActor class Prospects: ObservableObject {
    @Published private(set) var people = [Prospect]()
    
    let saveKey = "SavedData"
    
    init() {
        if let data = UserDefaults.standard.data(forKey: saveKey) {
            if let decoded = try? JSONDecoder().decode([Prospect].self, from: data) {
                people = decoded
                return
            }
        }
        
        people = []
    }
    
    private func save() {
        if let encoded = try? JSONEncoder().encode(people) {
            UserDefaults.standard.set(encoded, forKey: saveKey)
        }
    }
    
    func toggle(_ prospect: Prospect) {
        objectWillChange.send()
        prospect.isContacted.toggle()
        save()
    }
    
    func add(_ prospect: Prospect) {
        people.append(prospect)
        save()
    }
}

