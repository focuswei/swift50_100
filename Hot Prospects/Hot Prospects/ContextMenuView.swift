//
//  ContextMenuView.swift
//  Hot Prospects
//
//  Created by guozw2 on 2023/6/1.
//

import SwiftUI

struct ContextMenuView: View {
    @State private var backgroundColor = Color.red
    
    var body: some View {
        VStack {
            Text("Hello, World!")
                .padding()
                .background(backgroundColor)
            
            /// Control-clicking 或者长按 出现上下文菜单
            Text("Change Color")
                .padding()
                .contextMenu {
                    Button {
                        backgroundColor = .red
                    } label: {
                        Label("Red", systemImage: "checkmark.circle.fill")
                            .foregroundColor(.red)
                    }
                    
                    Button("Green") {
                        backgroundColor = .green
                    }
                    
                    Button("Blue") {
                        backgroundColor = .blue
                    }
                }
        }
    }
}

struct contextMenuView_Previews: PreviewProvider {
    static var previews: some View {
        ContextMenuView()
    }
}
