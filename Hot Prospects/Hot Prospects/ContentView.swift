//
//  ContentView.swift
//  Hot Prospects
//
//  Created by guozw2 on 2023/5/30.
//

import SwiftUI
import SamplePackage

struct ContentView: View {
    
    let possibleNumbers = Array(1...60)
    var result: String {
        // 每次触发body.update 都会重新计算
        let selected = possibleNumbers.random(7).sorted()
        let strings = selected.map(String.init)
        return strings.joined(separator: ",")
    }
    /// 测试didset
     @StateObject var updater = DelayedUpdater()
    
    /// get propagated
    @StateObject var prospects = Prospects()
    
    var body: some View {
        // Text("Value is: \(updater.value)")
//        Text("Result is \(result)")
//        NotificationView()
        TabView {
            ProspectsView(filter: .none)
                .tabItem {
                    Label("EveryOne", systemImage: "person.3")
                }
            ProspectsView(filter: .contacted)
                .tabItem {
                    Label("Contacted", systemImage: "checkmark.circle")
                }
            ProspectsView(filter: .uncontacted)
                .tabItem {
                    Label("Uncontacted", systemImage: "questionmark.diamond")
                }
            MeView()
                .tabItem {
                    Label("Me", systemImage: "person.crop.square")
                }
        }
        .environmentObject(prospects)
        
    }
    
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

@MainActor class User: ObservableObject {
    @Published var name = "Taylor Swift"
}
