//
//  ImageInterpolation.swift
//  Hot Prospects
//
//  Created by guozw2 on 2023/5/31.
//

import SwiftUI

struct ImageInterpolation: View {
    var body: some View {
        Image("example")
            .resizable()
            .scaledToFit()
            .frame(maxHeight: .infinity)
            .background(.black)
            .ignoresSafeArea()
    }
}

struct ImageInterpolation_Previews: PreviewProvider {
    static var previews: some View {
        ImageInterpolation()
    }
}
