//
//  MeView.swift
//  Hot Prospects
//
//  Created by guozw2 on 2023/6/1.
//

import SwiftUI
import CoreImage.CIFilterBuiltins
struct MeView: View {
    @State private var name = "Anonymous"
    @State private var email = "you@yoursite.com"
    @State private var qrUIImage: UIImage = UIImage()
    private let context = CIContext()
    let filter = CIFilter.qrCodeGenerator()
    var body: some View {
        NavigationView {
            Form {
                TextField("name", text: $name)
                    .textContentType(.name)
                    .font(.title)
                    .autocorrectionDisabled()
                    .textInputAutocapitalization(.never)
                    .onChange(of: name) { newValue in
                        generatorQRCode()
                    }
                TextField("email", text: $email)
                    .textContentType(.emailAddress)
                    .font(.subheadline)
                    .autocorrectionDisabled()
                    .onChange(of: email) { newValue in
                        generatorQRCode()
                    }
                Image(uiImage: qrUIImage)
                    .interpolation(.none) // 禁用图像插值
                    .resizable()
                    .scaledToFit()
                    .frame(width: 200, height: 200)
                    .contextMenu {
                        Button("Save qrCode") {
                            UIImageWriteToSavedPhotosAlbum(qrUIImage, nil, nil, nil)
                        }
                    }
            }
            .navigationTitle("Your code")
        }
        .onAppear(perform: generatorQRCode)
    }
    
    func generatorQRCode() {
        guard let stringData = (name + "\n" + email).data(using: .utf8) else { return }
        filter.message = stringData
        if let outputImage = filter.outputImage {
            if let cgimage = context.createCGImage(outputImage, from: outputImage.extent) {
                qrUIImage = UIImage(cgImage: cgimage, scale: 3.0, orientation: .up)
            }
        }
    }
}

struct MeView_Previews: PreviewProvider {
    static var previews: some View {
        MeView()
    }
}
