//
//  ProspectsView.swift
//  Hot Prospects
//
//  Created by guozw2 on 2023/6/1.
//

import SwiftUI
import UserNotifications

struct ProspectsView: View {
    // 如果环境对象不存在会崩溃，这是类似于隐式解包可选项的工作方式
    @EnvironmentObject var prospects: Prospects
    
    /// 打开扫描二维码flag
    @State private var isShowingScanner = false
    enum FilterType {
        case none, contacted, uncontacted
    }
    var title: String {
        switch filter {
        case .none:
            return "Everyone"
        case .contacted:
            return "Contact"
        case .uncontacted:
            return ""
        }
    }
    var filteredProspects: [Prospect] {
        switch filter {
        case .none:
            return prospects.people
        case .contacted:
            return prospects.people.filter({ $0.isContacted })
        case .uncontacted:
            return prospects.people.filter({ !$0.isContacted })
        }
    }
    
    let filter: FilterType
    var body: some View {
        NavigationView {
            List {
                ForEach(filteredProspects) { prospect in
                    VStack(alignment: .leading) {
                        Text("name | \(prospect.name)")
                            .font(.headline)
                        Text("email | \(prospect.emailAddress)")
                            .foregroundColor(.secondary)
                    }
                    .swipeActions(edge: .trailing, allowsFullSwipe: false) {
                        Button {
                            addNotification(for: prospect)
                        } label: {
                            Label("Mark Contacted", systemImage: "bell")
                        }.tint(.orange)
                    }
                    .swipeActions(edge: .trailing, allowsFullSwipe: false) {
                        Button {
                            prospects.toggle(prospect)
                        } label: {
                            Label("Mark Contacted", systemImage: "person.crop.circle.fill.badge.checkmark")
                        }.tint(.green)
                    }
                    .swipeActions {
                        Button {
                            prospects.toggle(prospect)
                        } label: {
                            Label("Mark Uncontacted", systemImage: "person.crop.circle.badge.xmark")
                        }
                        .tint(.blue)
                    }
                }
                
            }
            .navigationTitle(title)
            .toolbar {
                Button {
                    isShowingScanner = true
                } label: {
                    Label("Scan", systemImage: "qrcode.viewfinder")
                }

            }
            .sheet(isPresented: $isShowingScanner) {
                CodeScannerView(codeTypes: [.qr], simulatedData: "Paul Hudson\npaul@hackingwithswift.com", completion: handleScan)
            }
        }
    }
    
    func addNotification(for prospect: Prospect) {
        let center = UNUserNotificationCenter.current()
        let addRequest = {
            let content = UNMutableNotificationContent()
            content.title = "Contact \(prospect.name)"
            content.subtitle = prospect.emailAddress
            content.sound = .defaultRingtone
            
            var dateComponent = DateComponents()
            dateComponent.hour = 9
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponent, repeats: false)
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
            return request
        }
        center.requestAuthorization { (success, nil) in
            if success {
                center.add(addRequest())
            }
        }
    }
    
    func handleScan(result: Result<ScanResult, ScanError>) {
       isShowingScanner = false
        switch result {
        case .success(let result):
            let details = result.string.components(separatedBy: "\n")
            guard details.count == 2 else {return}
            
            let person = Prospect()
            person.name = details[0]
            person.emailAddress = details[0]
            prospects.add(person)
        case .failure(let error):
            print(error.localizedDescription)
        }
    }
}

//struct ProspectsView_Previews: PreviewProvider {
//    static var previews: some View {
//        ProspectsView(filter: .none)
//    }
//}
