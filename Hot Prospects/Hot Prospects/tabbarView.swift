//
//  tabbarView.swift
//  Hot Prospects
//
//  Created by guozw2 on 2023/5/31.
//

import SwiftUI

struct tabbarView: View {
    enum TabType: Int {
        case one
        case two
    }
    @StateObject var user = User()
    @State private var status = TabType.one
    @State private var value = "One"
    
    var body: some View {
        // 如何和 NavigationView 一起，应作为 parent view
        TabView(selection: $status) {
            Text("1")
                .tabItem {
                    Label("One", systemImage: "star")
                }.onTapGesture {
                    value = "Two"
                    print("\(value)")
                }.tag(TabType.one)
            
            Button("change") {
                status = .one
            }
            .tabItem {
                Label("Two", systemImage: "book")
            }.tag(TabType.two)
        }
    }
}

struct tabbarView_Previews: PreviewProvider {
    static var previews: some View {
        tabbarView()
    }
}
