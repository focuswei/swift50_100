//
//  NotificationView.swift
//  Hot Prospects
//
//  Created by guozw2 on 2023/6/1.
//

import SwiftUI
import UserNotifications

struct NotificationView: View {
    var body: some View {
        VStack {
            Button("Request Permission") {
                // 申请权限
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert], completionHandler: { success, error in
                    if success {
                        print("All set")
                    } else {
                        print(error?.localizedDescription)
                    }
                })
            }
            
            Button("Schedule Notification") {
                // second
                createNotification()
            }
        }
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
    
    func createNotification() {
        var content = UNMutableNotificationContent()
        content.title = "Feed the cat"
        content.subtitle = "It looks hungry"
        content.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request)
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
