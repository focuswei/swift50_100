//
//  ResultView.swift
//  Hot Prospects
//
//  Created by guozw2 on 2023/5/31.
//

import SwiftUI

struct ResultView: View {
    
    @State var output = ""
    
    var body: some View {
        
        Text(output).task {
            await fetchReadingReponse()
        }
    }
    
    func fetchReadingReponse() async {
        do {
            if let url = URL(string: "https://hws.dev/readings.json") {
                let (data, _) = try await URLSession.shared.data(for: URLRequest.init(url: url))
                
                let readings = try JSONDecoder().decode([Double].self, from: data)
                output = "Found \(readings.count) readings"
            }
        } catch  {
            
        }
        
    }
    
    func fetchReadingsTask() async {
        do {
            let fetchTask = Task { () -> String in
                if let url = URL(string: "https://hws.dev/readings.json") {
                    let (data, _) = try await URLSession.shared.data(for: URLRequest.init(url: url))
                    
                    let readings = try JSONDecoder().decode([Double].self, from: data)
                    return "Found \(readings.count) readings"
                }
                return "Not Found"
            }
        } catch {
            
        }
    }
}

struct ResultView_Previews: PreviewProvider {
    static var previews: some View {
        ResultView()
    }
}
