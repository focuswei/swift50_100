//
//  LoginForm.swift
//  NonNegative
//
//  Created by guozw2 on 2023/5/26.
//

import SwiftUI

struct LoginForm: View {
    enum Field: Hashable {
        case username
        case password
    }
    
    @State private var username = ""
    @State private var password = ""
    @available(iOS 15.0, *)
    @FocusState private var focusedField: Field?
    
    var body: some View {
        Form {
            TextField("Username", text: $username)
                .focused($focusedField, equals: .username)
            
            TextField("Password", text: $password)
                .focused($focusedField, equals: .password)
            
            Button("Sign in") {
                if username.isEmpty {
                    focusedField = .username
                } else if password.isEmpty {
                    focusedField = .password
                } else {
                    handleLogin(username, password)
                }
                
            }
        }
    }
    
    func handleLogin(_ username: String, _ password: String) -> Void {
        
    }
}

struct LoginForm_Previews: PreviewProvider {
    static var previews: some View {
        LoginForm()
    }
}
