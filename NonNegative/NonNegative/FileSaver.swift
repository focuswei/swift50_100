//
//  FileSaver.swift
//  NonNegative
//
//  Created by guozw2 on 2023/5/24.
//

import Foundation


extension FileManager {
    /// 模拟器每次运行会改变
    static var savePath: URL = {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }()
    
    static var imageDicPathComponent: String {
        return "ImgDic.json"
    }
    
    func getFilePath(by fileName: String) -> URL {
        let url = FileManager.savePath.appending(component: fileName)
        return url
    }
}
