//
//  NonNegativeApp.swift
//  NonNegative
//
//  Created by guozw2 on 2023/5/23.
//

import SwiftUI

@main
struct NonNegativeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
