//
//  ImagePicker.swift
//  NonNegative
//
//  Created by guozw2 on 2023/5/23.
//

import UIKit
import SwiftUI
import PhotosUI

// 只能用struct
struct ImagePicker: UIViewControllerRepresentable {

    @Binding var image: UIImage?
    @Binding var images: [UIImage]?
    
    class Coordinator: NSObject, PHPickerViewControllerDelegate {
        var parent: ImagePicker
        
        init(parent: ImagePicker) {
            self.parent = parent
        }
        
        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            picker.dismiss(animated: true)
            
            let itemProviders = results.map { $0.itemProvider }
            for (_, itemProvider) in itemProviders.enumerated() where itemProvider.canLoadObject(ofClass: UIImage.self) {
                itemProvider.loadObject(ofClass: UIImage.self) { reading, error in
                    if error == nil,
                       let image = reading as? UIImage {
                        self.parent.images?.append(image)
                        self.parent.image = image
                    }
                }
            }
        }
    }
    
    func makeUIViewController(context: Context) -> some UIViewController {
        var config = PHPickerConfiguration()
        config.filter = PHPickerFilter.images
        config.selectionLimit = 2
        let picker = PHPickerViewController(configuration: config)
        picker.delegate = context.coordinator
        return picker
    }
    
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(parent: self)
    }
}


struct CameraPicker: UIViewControllerRepresentable {
    
    @Binding var image: UIImage?
    
    
    class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        var parent: CameraPicker
        
        init(parent: CameraPicker) {
            self.parent = parent
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            
            var imageInfo: [String: Any] = [:]
            var image: UIImage?
            if picker.allowsEditing {
                image = info[.editedImage] as? UIImage
            }
            else {
                image = info[.originalImage] as? UIImage
            }
            let imageURL = (info[.imageURL] as? NSURL)?.absoluteString
            imageInfo["filePath"] = imageURL
            if let asset = info[.phAsset] as? PHAsset {
                if let filename = (asset.value(forKey: "filename")) as? String {
                    imageInfo["filename"] = filename
                }
            }
            
            picker.dismiss(animated: true) {
                if image != nil {
                    self.parent.image = image
                }
            }
        }
    }
    
    func makeUIViewController(context: Context) -> some UIViewController {
        
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = .camera
            picker.allowsEditing = false
            picker.showsCameraControls = true
        } else {
            picker.sourceType = .photoLibrary
        }
        return picker
        
    }
    
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(parent: self)
        
    }
}
