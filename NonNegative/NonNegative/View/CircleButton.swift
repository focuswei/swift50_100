//
//  CircleButton.swift
//  NonNegative
//
//  Created by guozw2 on 2023/5/26.
//

import SwiftUI

struct CircleButton: View {
    var imageSystemName: String
    var backgroundColor = Color.blue.opacity(0.7)
    var action: () -> Void
    
    var body: some View {
        Button {
            action()
        } label: {
            Image(systemName: imageSystemName)
                .padding()
                .background(backgroundColor)
                .foregroundColor(.white)
                .font(.subheadline)
                .clipShape(Circle())
                .padding(.trailing)
                .shadow(color: .white, radius: 10, x: 1, y: 1)
        }
    }
}

struct CircleButton_Previews: PreviewProvider {
    static var previews: some View {
        CircleButton(imageSystemName: "magnifyingglass", action: { })
    }
}
