//
//  ListView.swift
//  NonNegative
//
//  Created by guozw2 on 2023/5/25.
//

import SwiftUI

struct ListView: View {
    @State private var image: Image?
    @State private var imgName: String = ""
    @State private var showImagePickerSimple = false
    @State private var showAlert = false
    @State private var showListView = false
    @State private var inputImage: UIImage?
    
    @EnvironmentObject var locationFetcher: LocationFetcher
    @StateObject var vm = ListViewModel()
    var body: some View {
        VStack {
            GeometryReader { g in
                image?
                    .resizable()
                    .scaledToFit()
                    .frame(maxWidth: g.size.width, maxHeight: g.size.height)
            }
            
            
            
            TextField("ImageName:", text: $imgName)
                    .background(.clear)
                    .padding(.leading, 50)
            
            // MARK: Button
            VStack {
                
                
                Spacer()
                HStack {
                    VStack{
                        CircleButton(imageSystemName: "list.bullet") {
                            showListView = true
                        }
                        
                        CircleButton(imageSystemName: "plus") {
                            showImagePickerSimple = true
                        }
                    }
                }
            }
            .padding()
            
            
            
            
        }.sheet(isPresented: $showImagePickerSimple) {
            ImagePicker(image: $inputImage, images: .constant(nil))
        }
        .alert("输入图片名字", isPresented: $showAlert) {
            TextField("image name", text: $imgName)
            Button("确定") {
                saveImage()
            }
        }
        .sheet(isPresented: $showListView) {
            ImageList()
        }
        .onChange(of: inputImage) { newValue in
            if let n = newValue {
                showAlert = true
                loadImage(uiimage: n)
            }/// onChange 修饰符应该在最后，因为别的 Binding还没绑定，可能会少一次通知。
        }
    }
    
    func loadImage(uiimage: UIImage) {
        image = Image(uiImage: uiimage)
    }
    
    func deleteImage() {
        image = nil
    }
    
    func saveImage() {
        guard inputImage != nil else { return }
        let pngData = inputImage?.pngData()
        let imgDic = vm.creatImageDic(imageName: imgName, latitude: locationFetcher.lastKnownLocation?.latitude, longitude: locationFetcher.lastKnownLocation?.longitude)
        // TODO: 不是好办法，最好是将图片重命名为UUID, 大文件用FileMananger, 关系型数据用Core Data，小数据用mapped缓存。
        var dataArray = [ImageDic]()
        do {
            let data = try Data(contentsOf: FileManager.savePath.appendingPathComponent(FileManager.imageDicPathComponent), options: .mappedIfSafe)
            let array = try JSONDecoder.init().decode([ImageDic].self, from: data)
            dataArray.append(contentsOf: array)
        } catch {
            
        }
        dataArray.append(imgDic)
        let encoder = JSONEncoder.init()
        do {
            if let url = URL(string: imgDic.imagePath) {
                try pngData?.write(to: url, options: [.completeFileProtection])
            }
        } catch {
            print("Image save error: \(error.localizedDescription)")
        }
        do {
            let saveData = try encoder.encode(dataArray)
            var savePath = FileManager.savePath
            savePath = savePath.appendingPathComponent(FileManager.imageDicPathComponent)
            try saveData.write(to: savePath, options: [.atomic, .completeFileProtection])
        } catch {
            print("Opps: \(error.localizedDescription)")
        }
        
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
