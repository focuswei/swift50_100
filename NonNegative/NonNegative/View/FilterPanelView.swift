//
//  FilterPanelView.swift
//  NonNegative
//
//  Created by guozw2 on 2023/5/26.
//

import SwiftUI

struct FilterPanelView: View {
    @Binding var filterKeyword: String
    @Binding var sortOrder: SortOrder
    
    var isFilterPanelShowed: Bool
    var geometry: GeometryProxy
    var filterPanelHeightRatio = 0.05
    var filterPanelAniamtionDuration = 0.5
    var filterSystemImage = "arrow.up.arrow.down.square"
    var transition: AnyTransition = .move(edge: .top).combined(with: .opacity)
    
    @State private var switcher = false
    @FocusState private var isKeywordFocused: Bool
    
    var body: some View {
        ZStack {
            Text("Trigger")
                .hidden()
                .onChange(of: isFilterPanelShowed) { newValue in
                    withAnimation(.easeInOut(duration: filterPanelAniamtionDuration)) {
                        switcher.toggle()
                        isKeywordFocused = true
                    }
                }
            
            if switcher {
                HStack {
                    TextField(text: $filterKeyword) {
                        Text("Keyword")
                    }
                    .textInputAutocapitalization(.never)
                    .textFieldStyle(.roundedBorder)
                    .focused($isKeywordFocused, equals: true)
                    
                    Button {
                        sortOrder = (sortOrder == .forward) ? .reverse: .forward
                    } label: {
                        Image(systemName: filterSystemImage)
                            .font(.title)
                            .foregroundColor(.white)
                    }
                }
                .padding(.horizontal, 5)
                .frame(maxWidth: geometry.size.width, maxHeight: switcher ? geometry.size.height * filterPanelHeightRatio : 0)
                .background(.blue.opacity(0.15))
                .transition(transition)
                .clipShape(RoundedRectangle(cornerRadius: 10))
            }
        }
    }
}

//struct FilterPanelView_Previews: PreviewProvider {
//    static var previews: some View {
//        FilterPanelView()
//    }
//}
