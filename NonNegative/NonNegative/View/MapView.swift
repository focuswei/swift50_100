//
//  MapView.swift
//  NonNegative
//
//  Created by guozw2 on 2023/5/29.
//

import SwiftUI
import MapKit

struct MapView: View {
    // 地图中心
    var latitude: Double
    var longitude: Double
    @State private var mapRegion =  MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 0, longitude: 0), span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
    
    var body: some View {
        Map(coordinateRegion: $mapRegion)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
        
    }
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
        _mapRegion = State<MKCoordinateRegion>.init(wrappedValue: MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2)))
    }
}

//struct MapView_Previews: PreviewProvider {
//    static var previews: some View {
//        MapView()
//    }
//}

