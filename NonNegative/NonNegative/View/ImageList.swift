//
//  ImageList.swift
//  NonNegative
//
//  Created by guozw2 on 2023/5/24.
//

import SwiftUI

struct ImageList: View {
    @Environment(\.dismiss) var dismiss
    @State private var imgDicArray: [ImageDic] = []
    @State private var uiimages = [String:UIImage]()
    
    var body: some View {
        VStack {
            Button(action: {
                dismiss()
            }, label: {
                Image(systemName: "chevron.backward")
            })
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.top, 8)
            .padding(.leading,10)
            
            
        }
        NavigationView {
            List(imgDicArray) { array in
                NavigationLink(destination: {
                    if let la = array.latitude, let lo = array.longitude {
                        MapView(latitude: la, longitude: lo)
                    }
                }, label: {
                    Image(uiImage: uiimages[array.name ?? ""] ?? UIImage())
                        .resizable()
                        .scaledToFit()
                })
                
                LabeledContent("IMG: ", value: array.name ?? "")
                    .foregroundColor(.purple)
            }
            .navigationTitle("Image List")
            .onAppear(perform: getImageDicArray)
            .task {
                getUIImage()
            }
        }
        
        
    }
    
    func getImageDicArray() -> Void {
        let decoder = JSONDecoder.init()
        
        do {
            let data = try Data(contentsOf: FileManager.savePath.appendingPathComponent(FileManager.imageDicPathComponent), options: .mappedIfSafe)
            let array = try decoder.decode([ImageDic].self, from: data)
            imgDicArray = array.sorted { a, b in
                a.name ?? "" > b.name ?? ""
            }
        } catch {
            // handle error
            print("\(error.localizedDescription)")
        }
        
    }
    
    func getUIImage() {
        self.uiimages.removeAll()
        for dic in  imgDicArray {
            let url = FileManager.default.getFilePath(by: dic.fileName)
            if let uiimage = UIImage(contentsOfFile: url.absoluteString) {
                if let name = dic.name {
                    self.uiimages[name] = uiimage
                }
    //            return uiimage
            } else {
                var data: Data
                do {
                    data = try Data(contentsOf: url)
                    if let name = dic.name {
                        self.uiimages[name] = UIImage(data: data)
                    }
                } catch {
                    print("Cannot get image: \(error.localizedDescription)")
    //                return nil
                }
    //            return UIImage(data: data)
            }
        }
        
    }
}

struct ImageList_Previews: PreviewProvider {
    static var previews: some View {
        ImageList()
    }
}
