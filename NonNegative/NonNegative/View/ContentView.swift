//
//  ContentView.swift
//  NonNegative
//
//  Created by guozw2 on 2023/5/23.
//

import SwiftUI

struct ContentView: View {
    @StateObject var locationFetcher = LocationFetcher()
    @State var isCoreLocationEnable: Bool = false
    
    
    var body: some View {
        ZStack {
            Image("george-Dagerotip")
                .resizable()
                .scaledToFill()
                .ignoresSafeArea()
                .frame(maxWidth: .infinity, maxHeight: .infinity)
            
            VStack {

                if isCoreLocationEnable {
                    GeometryReader { g in
                        ListView().environmentObject(locationFetcher)
                    }
                } else {
                    VStack(alignment: .center, content: { Text("Please enable Core Location.").font(.title).foregroundColor(.white).multilineTextAlignment(.center)})
                }
                
                
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .task {
                locationFetcher.start()
            }
            .onChange(of: locationFetcher.authorizationStatus) { _ in
                isCoreLocationEnable = locationFetcher.isAuthorized
            }
        }
        
    }
    
    
    
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

/// 用一个struct包装 Value，仅限属性properties，不能之外的单独常量或变量
@propertyWrapper
struct NonNegative<Value: BinaryInteger> {
    var value: Value
    
    init(wrappedValue: Value) {
        if wrappedValue > 0 {
            value = 0
        } else {
            value = wrappedValue
        }
    }
    
    var wrappedValue: Value {
        get { value }
        set {
            if newValue > 0 {
                value = 0
            } else {
                value = newValue
            }
        }
    }
}
