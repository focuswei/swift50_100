//
//  ImageDic.swift
//  NonNegative
//
//  Created by guozw2 on 2023/5/23.
//

import Foundation

struct ImageDic: Identifiable,Codable, Hashable {
    var id: UUID = UUID()
    var name: String?
    var imagePath: String
    var fileName: String
    var latitude: Double?
    var longitude: Double?
    var date: Date
    
    init(name: String, longitude: Double?, latitude: Double?) {
        self.name = name
        self.longitude = longitude
        self.latitude = latitude
        self.fileName = "\(id.uuidString).png"
        self.date = Date()
        self.imagePath = FileManager.savePath.appending(component: self.fileName).absoluteString
    }

}

extension ImageDic: Comparable {
    static func < (lhs: Self, rhs: Self) -> Bool {
        return lhs.date.compare(rhs.date) == .orderedDescending
    }
}
