//
//  ListViewModel.swift
//  NonNegative
//
//  Created by guozw2 on 2023/5/25.
//

import UIKit

enum ActionType {
    case create, rename, changeFace
}

@MainActor class ListViewModel: ObservableObject {
    
    let jsonFileName = "ImgDic.json"
    
    var imgDics = [ImageDic]()

    private(set) var modifiedObj: ImageDic?
    
    func getList() -> [ImageDic] {
        let decoder = JSONDecoder.init()
        do {
            let data = try Data(contentsOf: FileManager.savePath.appendingPathComponent(FileManager.imageDicPathComponent), options: .mappedIfSafe)
            let array = try decoder.decode([ImageDic].self, from: data)
            let a = array.sorted()
            imgDics = a
            return a
        } catch {
            return []
        }
        
    }
    
    func filteredList(keyword: String, sortOrder: SortOrder) -> [ImageDic] {
        // 不区分大小写
        let sortKey = keyword.trimmingCharacters(in: .whitespaces).lowercased()
        
        let sortedArray = self.imgDics.filter { imgDic in
            return imgDic.name?.lowercased().contains(sortKey) == true
        }
        
        
        return sortedArray.sorted { lhs, rhs in
            if sortOrder == .forward {
                return lhs < rhs
            } else {
                return lhs > rhs
            }
        }
    }
    
    func creatImageDic(imageName: String, latitude: Double?, longitude: Double?) -> ImageDic {
        let imgDic = ImageDic(name: imageName, longitude: longitude, latitude: latitude)
        return imgDic
    }
}
