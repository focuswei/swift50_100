//
//  FlipEffect.swift
//  ButtonStyle
//
//  Created by guozw2 on 2023/12/26.
//

import SwiftUI

struct FlipEffect: GeometryEffect {
    
    var animatableData: Double {
        get { angle }
        set { angle = newValue }
    }
    
    @Binding var flipped: Bool
    var angle: Double
    let axis: (x: CGFloat, y: CGFloat)
    
    func effectValue(size: CGSize) -> ProjectionTransform {
        DispatchQueue.main.async {
            self.flipped = self.angle >= 90 && self.angle < 270
        }
        
        let a = CGFloat(Angle(degrees: angle).radians)
        
        var transform3d = CATransform3DIdentity
        transform3d.m34 = -1/max(size.width, size.height)
        
        transform3d = CATransform3DRotate(transform3d, a, axis.x, axis.y, 0)
        transform3d = CATransform3DTranslate(transform3d, -size.width/2.0, -size.height/2.0, 0)
        
        let affineTransform = ProjectionTransform(CGAffineTransform(translationX: size.width/2.0, y: size.height/2.0))
        
        return ProjectionTransform(transform3d).concatenating(affineTransform)
    }
}

struct RotatingCard: View {
    @State private var flipped = false
    @State private var animate3d = false
    @State private var rotate = false
    @State private var imgIndex = 0
    
    let images: [String] = []
    
    var body: some View {
        let binding = Binding<Bool> {
            self.flipped
        } set: { value, t in
            self.updateBinding(value)
        }

        Image(flipped ? "back" : images[imgIndex]).resizable()
            .frame(width: 265, height: 400)
            .modifier(FlipEffect(flipped: binding, angle: animate3d ? 360:0, axis: (x: 1, y: 5)))
            .onAppear {
                withAnimation(Animation.linear(duration: 4.0).repeatForever(autoreverses: false)) {
                    self.animate3d = true
                }
                
                withAnimation(Animation.linear(duration: 8.0).repeatForever(autoreverses: false)) {
                    self.rotate = true
                }
            }
    }
    
    func updateBinding(_ value: Bool) {
        if flipped != value && !flipped {
            self.imgIndex = self.imgIndex+1 < self.images.count ? self.imgIndex+1 : 0
        }
        flipped = value
    }
}

//#Preview {
//    
//}
