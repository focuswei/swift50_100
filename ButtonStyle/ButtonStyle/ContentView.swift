//
//  ContentView.swift
//  ButtonStyle
//
//  Created by guozw2 on 2023/12/25.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Button("Tap me") {
                print("button pressed")
                
            }
            .buttonStyle(MyButtonStyle(color: .red))
            .buttonStyle(MyPrimitiveButtonStyle(color: .blue))
            
        }
        .padding()
    }
}

struct MyPrimitiveButtonStyle: PrimitiveButtonStyle {
    var color: Color

    func makeBody(configuration: PrimitiveButtonStyle.Configuration) -> some View {
        MyButton(configuration: configuration, color: color)
    }
    
    struct MyButton: View {
        @GestureState private var pressed = false

        let configuration: PrimitiveButtonStyle.Configuration
        let color: Color

        var body: some View {
            let longPress = LongPressGesture(minimumDuration: 1.0, maximumDistance: 0.0)
                .updating($pressed) { value, state, _ in state = value }
                .onEnded { _ in
                   self.configuration.trigger()
                 }

            return configuration.label
                .foregroundColor(.white)
                .padding(15)
                .background(RoundedRectangle(cornerRadius: 5).fill(color))
                .compositingGroup()
                .shadow(color: .black, radius: 3)
                .opacity(pressed ? 0.5 : 1.0)
                .scaleEffect(pressed ? 0.8 : 1.0)
                .gesture(longPress)
        }
    }
}

struct MyButtonStyle: ButtonStyle {
    var color: Color = .green
    
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .foregroundColor(.white)
            .padding(15)
            .background(RoundedRectangle(cornerRadius: 5).fill(color))
            .compositingGroup()
            .shadow(color: .black, radius: 3, x: 0.0, y: 0.0)
            .opacity(configuration.isPressed ? 0.5:1.0)
            .scaleEffect(configuration.isPressed ? 0.8:1.0)
    }
}

#Preview {
    ContentView()
}

struct Vector2D {
    var x = 0.0, y = 0.0
    
    static let zero = Vector2D(x: 0, y: 0)
}

extension Vector2D: CustomStringConvertible {
    var description: String {
        return "(x: \(x), y: \(y))"
    }
}

extension Vector2D {
    // Vector addition
    static func + (left: Vector2D, right: Vector2D) -> Vector2D {
        return Vector2D(x: left.x + right.x, y: left.y + right.y)
    }
    
    // Vector subtraction
    static func - (left: Vector2D, right: Vector2D) -> Vector2D {
        return left + (-right)
    }
    
    // Vector addition assignment
    static func += (left: inout Vector2D, right: Vector2D) {
        left = left + right
    }
    
    // Vector subtraction assignment
    static func -= (left: inout Vector2D, right: Vector2D) {
        left = left - right
    }
    
    // Vector negation
    static prefix func - (vector: Vector2D) -> Vector2D {
        return Vector2D(x: -vector.x, y: -vector.y)
    }
}

extension Vector2D: Equatable {
    static func == (left: Vector2D, right: Vector2D) -> Bool {
        return (left.x == right.x) && (left.y == right.y)
    }
}
