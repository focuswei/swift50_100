//
//  ButtonStyleApp.swift
//  ButtonStyle
//
//  Created by guozw2 on 2023/12/25.
//

import SwiftUI

@main
struct ButtonStyleApp: App {
    var body: some Scene {
        WindowGroup {
//            ContentView()
//            OutOfControlView()
            WidthView()
        }
    }
}
