//
//  CustomToggleView.swift
//  ButtonStyle
//
//  Created by guozw2 on 2023/12/25.
//

import SwiftUI

struct CustomToggleView: View {
    @State private var flag = false
    var body: some View {
        VStack {
            Toggle("Toggle Style", isOn: $flag)
//                .toggleStyle(MyToggleStyle())
                
        }
    }
}

struct MyToggleStyle: ToggleStyle {
    let width: CGFloat = 50
    func makeBody(configuration: Configuration) -> some View {
        HStack {
                    configuration.label

                    ZStack(alignment: configuration.isOn ? .trailing : .leading) {
                        RoundedRectangle(cornerRadius: 4)
                            .frame(width: width, height: width / 2)
                            .foregroundColor(configuration.isOn ? .green : .red)
                        
                        RoundedRectangle(cornerRadius: 4)
                            .frame(width: (width / 2) - 4, height: width / 2 - 6)
                            .padding(4)
                            .foregroundColor(.white)
                            .onTapGesture {
                                withAnimation {
                                    configuration.$isOn.wrappedValue.toggle()
                                }
                        }
                    }
                }
    }
    
    
}
#Preview {
    CustomToggleView()
}
