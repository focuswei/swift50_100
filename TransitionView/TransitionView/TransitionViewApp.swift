//
//  TransitionViewApp.swift
//  TransitionView
//
//  Created by guozw2 on 2023/12/20.
//

import SwiftUI

@main
struct TransitionViewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
