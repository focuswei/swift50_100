//
//  CustomTransition.swift
//  TransitionView
//
//  Created by guozw2 on 2023/12/20.
//

import SwiftUI

struct CustomTransition: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

extension AnyTransition {
    //  计算属性
    static var myOpacity: AnyTransition {
        get {
            AnyTransition.modifier(active: MyOpacityModifier.init(opacity: 0), identity: MyOpacityModifier.init(opacity: 1))
        }
    }
    
    
    func myOpacity() -> AnyTransition {
        return AnyTransition.modifier(active: MyOpacityModifier.init(opacity: 0), identity: MyOpacityModifier.init(opacity: 1))
    }
    
    static var opacityOn: AnyTransition {
        asymmetric(insertion: .myOpacity, removal: .identity)
    }
}


struct MyOpacityModifier: ViewModifier {
    
    let opacity: Double
    
    func body(content: Content) -> some View {
        content
            .opacity(opacity)
    }
}

#Preview {
    CustomTransition()
}
