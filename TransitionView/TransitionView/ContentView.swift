//
//  ContentView.swift
//  TransitionView
//
//  Created by guozw2 on 2023/12/20.
//

import SwiftUI

struct ContentView: View {
    @State private var show = false
        
        var body: some View {
            
            VStack {
                Spacer()
                
                if show {
                    LabelView()
                        .animation(.easeInOut(duration: 1.0))
                        .transition(.opacity)
//                    LabelView()
//                        .transition(AnyTransition.opacity.animation(.easeInOut(duration: 1.0)))
                    
                    if #available(iOS 15, *) {
                        LabelView()
                            .animation(.easeInOut, value: 1.0)
                            .transition(.opacity)
                    }
                    
                    // 非对称过渡
                    LabelView()
                        .transition(.asymmetric(insertion: AnyTransition.opacity.animation(.easeInOut(duration: 1.0)), removal: AnyTransition.move(edge: .bottom).combined(with: .opacity)))
                }
                
                Spacer()
                
                Button("Animate") {
                    // 显式动画
                    withAnimation(.easeInOut(duration: 1.0)) {
                        self.show.toggle()
                    }
                }.padding(20)
            }
        }
}

struct LabelView: View {
    var body: some View {
        Text("Hi there!")
            .padding(10)
            .font(.title)
            .foregroundColor(.white)
            .background(RoundedRectangle(cornerRadius: 8).fill(Color.green).shadow(color: .gray, radius: 3))
    }
}
#Preview {
    ContentView()
}
